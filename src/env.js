(function (window) {
  window.__env = window.__env || {};

  // API url
  
  window.__env.APIURL = 'http://localhost:8081';
  window.__env.APIURLPDF = 'http://localhost:8081/pdfs';
  
  /*
  window.__env.APIURL = 'http://distribuidoradonaji.ddns.net:8081';
  window.__env.APIURLPDF = 'http://distribuidoradonaji.ddns.net:8081/pdfs';    
  */
  
  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = false;
}(this));