import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PedidosComponent} from './pedidos.component';
import {NuevoPedidoComponent} from './nuevo-pedido/nuevo-pedido.component';
import {DetallePedidoComponent} from './detalle-pedido/detalle-pedido.component';

const routes: Routes = [
  {
    path: '', component: PedidosComponent, data: {
      title: 'Pedidos',
      breadcrumb: 'Pedidos',
    },
  },
  {
    path: 'nuevo', component: NuevoPedidoComponent, data: {
      title: 'Nuevo pedido',
      breadcrumb: 'Nuevo pedido',
    },
  },
  {
    path: ':id', component: DetallePedidoComponent, data: {
      title: 'Detalle pedido',
      breadcrumb: 'Detalle',
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PedidosRoutingModule {
}
