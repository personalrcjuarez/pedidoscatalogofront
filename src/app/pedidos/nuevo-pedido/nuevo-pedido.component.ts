import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PersonModel } from '../../models/personModel';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PersonasService } from '../../services/personas.service';
import { Observable, of, Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { map, startWith, tap } from 'rxjs/operators';
import { Impuesto, Moneda, Producto, Marca, NewProd, ArticleFull, ArticuloFront } from '../../models/articles';
import { ProductsService } from '../../services/products.service';
import { DetallesFront, PedidoFront,  StatusPedido } from '../../models/pedidos';
import { CatalogsService } from '../../services/catalogs.service';
import { MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PedidosService } from '../../services/pedidos.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { ModalProductosComponent } from '../modal-productos/modal-productos.component';
import { APIResponse } from '../../models/common';
import { Almacen } from '../../models/almacenes';

@Component({
  selector: 'app-nuevo-pedido',
  templateUrl: './nuevo-pedido.component.html',
  styleUrls: ['./nuevo-pedido.component.scss']
})

export class NuevoPedidoComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['marca', 'clave', 'nombre', 'color', 'talla', 'cantidad', 'precio', 'descuentoMarca', 'descuentoProducto', 'importe', 'actions'];
  dataSource: MatTableDataSource<DetallesFront>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  errorMessage = '';
  customers: PersonModel[] = [];
  filteredCustomers: Observable<PersonModel[]> = of([]);
  customerInput = new FormControl();
  readonly customerType = 3;

  products: Producto[] = [];
  filteredProducts: Observable<ArticuloFront[]> = of([]);
  productInput = new FormControl();

  subscriptions: Subscription[] = [];
  foundResults = false;

  pedido = new PedidoFront();
  marcas: Map<any, Marca> = new Map<any, Marca>();
  almacenes: Almacen[] = [];
  monedas: Moneda[] = [];
  monedaSelected: Moneda;
  mapImpuestos: Map<any, Impuesto> = new Map<any, Impuesto>();

  subtotal = 0;
  selectedCustomer: PersonModel;

  status = StatusPedido;
  loading = false;
  datosAll: APIResponse = null;

  constructor(
    private ps: PersonasService,
    private cs: CatalogsService,
    private p: ProductsService,
    private pedidosService: PedidosService,
    private snack: MatSnackBar,
    private router: Router,
    public auth: AuthService,
    private dialog: MatDialog,
  ) {
    this.dataSource = new MatTableDataSource<DetallesFront>(this.pedido.DetallesFront);
  }

  ngOnInit(): void {
    this.loading = true;

    this.ps.getPersons().subscribe(personDB => {
      this.customers = personDB.filter(d => this.customerType === d.tipo && !d.bloqueado);
      const myself = this.customers.filter(d => d.id === this.auth.user.userId);
      if (this.auth.isCustomer()) {
        //carga nuevamente a la persona en específico para determinar sus descuentos
        this.ps.getPerson(myself[0].id).subscribe(cliente => {
          if (cliente != null) {
            if (this.auth.isCustomer()) {
              this.selectedCustomer = cliente;
              this.pedido.ClienteId = this.selectedCustomer.id;
              this.pedido.cliente = this.selectedCustomer;
              this.customerInput.disable();
            }
            this.customerInput.setValue(this.selectedCustomer);
          }
        });
      }
      this.customerInput.setValue(this.selectedCustomer);
    }, (err) => {
      this.errorMessage = err.error.mensajeInformativo || '';
    });

    this.filteredCustomers = this.customerInput.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value && value.toLowerCase ? value.toLowerCase() : '';
          return this.customers.filter(option => option.nombres.toLowerCase().includes(filterValue));
        })
      );

    //this.productInput.disable();
    /*
        this.p.products.subscribe(prod => {
          if (prod.length === 0) {
            this.p.loadProducts().subscribe(() => {
              this.productInput.enable();
            });
          }
        });
        */
    //this.productInput.enable();

    //GET ALL MARCAS
    this.cs.getMarcasLoaded.subscribe(mrc => {
      if (mrc.length === 0) {
        this.cs.loadMarcas().subscribe(xx => {
        })
      }
    })

    this.marcas = this.cs.mapaMarcas;

    const productsSubscription = this.p.products.subscribe(d => {

      //set marca to product
      d.forEach(prod => {
        prod.marca = this.marcas.get(prod.marcaId);
      })

      this.products = d ? d.filter(v => !v.bloqueado) : [];
      this.loading = false;
    });
    this.subscriptions.push(productsSubscription);
    /*
        this.filteredProducts = this.productInput.valueChanges
          .pipe(
            startWith(''),
            map(value => {
              const filterValue = value && value.toLowerCase ? value.toLowerCase() : '';
              return this.products.filter(option =>
                option.marca.NombreMarca.toLowerCase().includes(filterValue) ||
                option.nombre.toLowerCase().includes(filterValue) ||
                option.clave.toLowerCase().includes(filterValue));
            })
          );
          */


    this.cs.loadAlmacenes().subscribe(d => {
      this.almacenes = d;
      this.pedido.AlmacenId = 0;
    });

    this.pedido.NombreStatus = StatusPedido[8];

    this.cs.loadMonedas().subscribe(d => {
      this.monedas = d;
      //d[0] Pesos Mexicanos
      //d[1] Quetzal Guatemalteco
      this.monedaSelected = d[1];
    });

    this.cs.loadImpuestos().subscribe(d => {
      if (d && d.length) {
        d.forEach(v => {
          this.mapImpuestos.set(v.id, v);
        });
      }
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


  displayClientFn(client: PersonModel): string {
    return client ? `${client.nombres} ${client.apellido1} ${client.apellido2}` : '';
  }

  displayProductFn(p: ArticuloFront): string {
    return p ? `${p.Clave} - ${p.NombreProducto}` : '';
  }

  addProductBefore(): void {
    let prductOri: Producto = this.productInput.value;

      this.p.get(prductOri.idArticulo).subscribe(prod => {

        prod.marca = this.marcas.get(prod.marcaId);
        this.updatePedido();

        this.dataSource.data = this.pedido.DetallesFront;

      });
  }
  
  addProductOptimized(): void {
    let prductOri: ArticuloFront = this.productInput.value;
      this.p.getByTallaColors(prductOri.Id, this.pedido.cliente.id).subscribe(prod => {
        this.openDialog(prod.object);

        this.updatePedido();

        this.dataSource.data = this.pedido.DetallesFront;

      });
  }
  changeQuantityProduct(p: DetallesFront, nuevaCantidad: number, e: KeyboardEvent): void {
    if (isNaN(nuevaCantidad)) {
      p.Cantidad = 1;
      this.updateProduct(p, 1);
      return;
    }
    p.Cantidad = nuevaCantidad;
    this.updateProduct(p, p.Cantidad);
    return;
  }

  updateProduct(p: any, nuevaCantidad: any): void {
    let porcentajeImpuesto = 0;
    p.DescuentoArticuloFront = calcularDescuentoArticulo(p.PrecioMoneda, nuevaCantidad, p.PorcDescArticulo, p.EnOferta);
    p.Impuesto = calcularImpuesto(p.PrecioMoneda, nuevaCantidad, porcentajeImpuesto);
    p.CantDescMarca = calcularDescuentoMarca(p.PrecioMoneda, p.Cantidad, p.PorcDescMarca);
    p.Importe = calcularImporte(p.PrecioMoneda, nuevaCantidad, p.Impuesto, p.CantDescMarca, p.DescuentoArticuloFront);
    p.ImporteBruto = p.PrecioMoneda * nuevaCantidad;
    this.updatePedido();
  }


  updateProductsOptimized(p: DetallesFront): void {
    p.DescuentoArticuloFront = calcularDescuentoArticulo(p.PrecioMoneda, p.Cantidad, p.PorcDescArticulo, p.EnOferta);
    p.DescuentoArticulo = calcularDescuentoArticulo(p.PrecioMoneda, 1, p.PorcDescArticulo, p.EnOferta);
    p.Impuesto = calcularImpuesto(p.PrecioMoneda, p.Cantidad, p.Impuesto);
    p.CantDescMarca = calcularDescuentoMarca(p.PrecioMoneda, p.Cantidad, p.PorcDescMarca);
    p.Importe = calcularImporte(p.PrecioMoneda, p.Cantidad, p.Impuesto, p.CantDescMarca, p.DescuentoArticuloFront);
    p.ImporteBruto = p.PrecioMoneda * p.Cantidad;
    this.updatePedido();
  }

  updatePedido(): void {  
    let subtotalTemp = 0;
    let impuestos = 0;
    let descuento = 0;

    let descuentosMarcas = this.pedido.cliente?.descuentoMarcas;
      this.pedido.DetallesFront.forEach(p => {
        p.IdCatalogoTallasColores = p.IdCatalogoTallasColores;
        p.Precio = p.PrecioMoneda;
        //determinar el porcentaje de la marca para posteriormente realizar los cálculos
        descuentosMarcas?.forEach(dtoMarc => {          
          if (p.IdMarca == dtoMarc.idMarca) {
            p.PorcDescMarca = dtoMarc.porcentajeDescuento;
            p.CantDescMarca = calcularDescuentoMarca(p.PrecioMoneda, p.Cantidad, dtoMarc.porcentajeDescuento);
          }
        });
        subtotalTemp += p.ImporteBruto;
        impuestos += (p.Impuesto * this.monedaSelected.tipoCambio);
        descuento += (p.CantDescMarca + p.DescuentoArticuloFront);
      });

    this.pedido.PorcDescAdic = Number(this.pedido.PorcDescAdic) || 0;
    this.pedido.TotalDescuento = descuento;
    this.pedido.TotalImporte = subtotalTemp;
    //this.pedido.TotalImpuesto = impuestos;
    this.subtotal = subtotalTemp + impuestos - descuento;
    this.pedido.DescuentoCliente = this.selectedCustomer ? this.subtotal * (this.selectedCustomer.descuento / 100) : 0;
    this.pedido.CantDescAdic = (this.subtotal - this.pedido.DescuentoCliente) * (this.pedido.PorcDescAdic / 100);
    this.pedido.TotalGeneral = this.subtotal - this.pedido.DescuentoCliente - this.pedido.CantDescAdic;
    this.pedido.Anticipo = this.pedido.TotalGeneral * .30;
    this.pedido.NombreStatus = StatusPedido[8];
  }

  selectClient(c: MatAutocompleteSelectedEvent): void {
    this.selectedCustomer = c.option.value;
    if (this.selectedCustomer) {
      //carga nuevamente a la persona en específico para determinar sus descuentos
      this.ps.getPerson(this.selectedCustomer.id).subscribe(cliente => {        
        if (cliente != null) {
          this.selectedCustomer.descuentoMarcas = cliente.descuentoMarcas;

          this.pedido.cliente = this.selectedCustomer;
          this.pedido.ClienteId = this.selectedCustomer.id;
          this.updatePedido();
        }
      });
    }
  }

  save(): void {
      //FUNCIÓN DESCONINUADA, YA NO ES NECESARIO VALIDAR COLORES Y TALLAS
      /*
      //Validar que todos los colores y las tallas estén seleccionados
      //Comprobar si ya hay elementos en el detalle del pedido, siendo así obligar a que seleccionen la talla y el color
      //del elemento anterior
      if (this.validarColoresyTallas() === false) {
        return;
      }
      */

    this.loading = true;
    this.pedido.TipoCambio = this.monedaSelected.tipoCambio;
    this.pedido.ClaveMoneda = this.monedaSelected.clave;
    this.pedido.AutorizaDesc = !this.pedido.PorcDescAdic;
    this.pedido.VendedorId = this.auth.user.userId;
    

    this.pedidosService.create(this.pedido).subscribe(d => {
      this.snack.open(`Pedido creado con exito`, '', {
        duration: 4000,
      });
      this.loading = false;
      this.router.navigate(['/pedidos']);
    }, error => {
      console.log(error);
      this.snack.open(`El pedido no se pudo crear: ${error.error.mensajeInformativo}`, '', {
        duration: 4000,
      });
      this.loading = false;
    });

  }

  removeProduct(r: DetallesFront): void {

    Swal.fire({
      title: "¿Está seguro de quitar éste producto?",
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.isConfirmed) {        
        const index = this.pedido.DetallesFront.findIndex(d => d.ArticuloId === r.ArticuloId && d.IdTalla == r.IdTalla && d.IdColor == r.IdColor);
        if (index >= 0) {
          this.pedido.DetallesFront.splice(index, 1);
          this.dataSource.data = this.pedido.DetallesFront;
        }
        this.updatePedido();
      }
    })
  }

  calcularTipoMoneda(): void {

    this.pedido.DetallesFront.forEach(pedido => {
      pedido.PrecioMoneda = pedido.Precio * this.monedaSelected.tipoCambio;
      this.updateProductsOptimized(pedido);
    });
  }

  searchCoincidences(input: any): void {
    if (input != "") {
      this.p.getCoincidencesProduct(input).pipe(
        map((productData: APIResponse) =>
          this.datosAll = productData,
        )
      ).subscribe(dato => {

        if (dato.object.length > 0) {
          this.foundResults = true;
          dato.object.forEach(element => {
            element.marca = this.marcas.get(element.marcaId);
          });

          this.filteredProducts = this.productInput.valueChanges
            .pipe(
              startWith(''),
              map(value => {
                return dato.object;
              })
            );
        } else {
          if (dato.codigo == 200) {
            this.snack.open(`No hay coincidencias para el artículo: [`+ input +"]", 'Producto inexistente',
            {
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
              duration: 5000,
            });
          }else{
            this.snack.open("["+ dato.mensajeError +"]", 'Error al consultar: ['+dato.codigo+']',
            {
              horizontalPosition: 'center',
              verticalPosition: 'bottom',
              duration: 10000,
            });
          }          
          this.foundResults = false;
        }

      }
      );
    }
  }

  openPanel(evt: any, trigger: MatAutocompleteTrigger, words: string): void {
    if (words != "") {
      this.searchCoincidences(words);
      evt.stopPropagation();
      trigger.openPanel();
    } else {
      this.foundResults = false;
    }
  }

  openDialog(prod: ArticleFull[]): void {

    const dialogRef = this.dialog.open(ModalProductosComponent, {
      maxWidth: '100vw',
      height: '85vh',
      maxHeight: '85vh',
      closeOnNavigation: true,
      data: prod,
    });

    dialogRef.afterClosed().subscribe((result: Set<ArticleFull>) => {
      if (result.size > 0) {

        //vacir el mat autocomplete
        //y limpiar el input
        this.productInput.setValue("");
        this.filteredProducts = null;

        result.forEach((valBefPedido: ArticleFull) => {

            if (!validateExistOrdersOptimized(valBefPedido, this.pedido.DetallesFront)) {
            let calculoTipoMoneda = valBefPedido.Precio * this.monedaSelected.tipoCambio;

            let porcentajeMarca = this.selectedCustomer?.descuentoMarcas;
            let descuentoMarca = 0;
            let porcDescMarca = 0;
            //determinar el porcentaje de la marca para posteriormente realizar los cálculos
            porcentajeMarca?.forEach(element => {
              if (valBefPedido.IdMarca == element.idMarca) {
                porcDescMarca = element.porcentajeDescuento;
                descuentoMarca = calcularDescuentoMarca(calculoTipoMoneda, 1, element.porcentajeDescuento);
              }
            });

            this.pedido.DetallesFront.push({
              Id: 0,
              ArticuloId: valBefPedido.IdArticulo,
              Cantidad: 1,
              PorcDescMarca: porcDescMarca,
              CantDescMarca: descuentoMarca,
              DescuentoArticulo: calcularDescuentoArticulo(calculoTipoMoneda, 1, valBefPedido.PorcentajeDescuentoArticulo, valBefPedido.EnOferta),
              DescuentoArticuloFront: calcularDescuentoArticulo(calculoTipoMoneda, 1, valBefPedido.PorcentajeDescuentoArticulo, valBefPedido.EnOferta),
              Importe: (calcularImporte(calculoTipoMoneda, 1,
                calcularImpuesto(calculoTipoMoneda, 1, 0),
                calcularDescuentoMarca(calculoTipoMoneda, 1, porcDescMarca),
                calcularDescuentoArticulo(calculoTipoMoneda, 1, valBefPedido.PorcentajeDescuentoArticulo, valBefPedido.EnOferta))
              ),
              Impuesto: calcularImpuesto(calculoTipoMoneda, 1, 0),
              Precio: valBefPedido.Precio,
              IdMarca: valBefPedido.IdMarca,
              //coloresProductos: valBefPedido.producto.coloresProductos,
              IdColor: valBefPedido.IdColor,
              NombreColor: valBefPedido.Color,
              //tallasProductos: valBefPedido.producto.tallasProductos,
              IdTalla: valBefPedido.IdTalla,
              NombreTalla: valBefPedido.NombreTalla,
              PrecioMoneda: calculoTipoMoneda,
              CostoUltimo : valBefPedido.Costo,
              NombreMarca : valBefPedido.NombreMarca,
              ClaveProducto: valBefPedido.Clave,
              NombreProducto: valBefPedido.Nombre,
              Subtotal: 0,
              ImporteBruto: calculoTipoMoneda,
              EnOferta: valBefPedido.EnOferta,
              PorcDescArticulo: valBefPedido.PorcentajeDescuentoArticulo,
              DescUnitMarca: descuentoMarca,
              IdCatalogoTallasColores: valBefPedido.IdCatalogoTallasColores,
              EstatusInventario: '',
            });
          }
        });
        //this.productInput.setValue('');
        this.updatePedido();
        this.dataSource.data = this.pedido.DetallesFront;
      }
    });
  }

  /*
  Métodos para la tabla
  */
 
  getTotalImport() {
    return this.pedido.DetallesFront.map(t => t.Importe).reduce((acc, value) => acc + value, 0);
  }

  getTotalCantidad() {
    return this.pedido.DetallesFront.map(t => t.Cantidad).reduce((acc, value) => acc + value, 0);
  }

  getTotalDescMarca() {
    return this.pedido.DetallesFront.map(t => t.CantDescMarca).reduce((acc, value) => acc + value, 0);
  }
  
  getTotalDescProd() {
    return this.pedido.DetallesFront.map(t => t.DescuentoArticuloFront).reduce((acc, value) => acc + value, 0);
  }
    
}

export function  validateExistOrdersOptimized(newProd: ArticleFull, detallesPedido: DetallesFront[]): boolean {
  let found: boolean = false;
  detallesPedido.forEach(ped => {
    if (newProd.IdArticulo == ped.ArticuloId
      && newProd.IdColor == ped.IdColor
      && newProd.IdTalla == ped.IdTalla) {
      found = true;
      return;
    }
  });
  return found;
}

export function calcularDescuentoMarca(precioUnitario: number, cantidad: number, porcentaje: number) {
  return (precioUnitario * (porcentaje / 100)) * cantidad;
}

export function calcularDescuentoArticulo(precioUnitario: number,
  cantidad: number,
  porcentaje: number, oferta: boolean): number {
  if (!oferta) {
    return 0;
  }
  return (precioUnitario * (porcentaje / 100)) * cantidad;
}

export function calcularImporte(precioUnitario: number, cantidad: number, impuestos: number, descuentoMarca: number, descuentoProducto: number): number {
  return (precioUnitario * cantidad) + impuestos - (descuentoMarca + descuentoProducto);
}

export function   calcularImpuesto(precioUnitario: number, cantidad: number, porcentajeImpuesto: number): number {
  const impuestoUnitario = precioUnitario * ((porcentajeImpuesto || 0) / 100);
  return impuestoUnitario * cantidad;
}