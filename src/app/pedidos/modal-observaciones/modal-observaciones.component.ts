import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-modal-observaciones',
  templateUrl: './modal-observaciones.component.html',
  styleUrls: ['./modal-observaciones.component.scss']
})
export class ModalObservacionesComponent implements OnInit {
  observaciones = '';

  constructor(public dialogRef: MatDialogRef<ModalObservacionesComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { title: string }) {
  }

  ngOnInit(): void {
  }

  accept(): void {
    this.dialogRef.close(this.observaciones);
  }

  close(): void {
    this.dialogRef.close(null);
  }
}
