import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { PedidosService } from '../services/pedidos.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { PedidoFront, StatusPedido } from '../models/pedidos';
import { Devolucion } from '../models/devolucion';
import { CatalogsService } from '../services/catalogs.service';
import { PersonModel } from '../models/personModel';
import { PersonasService } from '../services/personas.service';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Catalogs } from '../models/catalogs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { DataPaginator } from '../models/common';
import { Almacen } from '../models/almacenes';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})
export class PedidosComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['id', 'cliente', 'vendedor', 'almacen', 'moneda', 'total', 'createdAt', 'updatedAt', 'status', 'saldoActual', 'actions', 'devoluciones', 'estatusDevolucion'];
  dataSource: DataPaginator = null;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  errorMessage = '';

  pedidos: PedidoFront[] = [];
  displayedData = [];
  almacenes: Map<any, Almacen> = new Map<any, Almacen>();
  persons: Map<any, PersonModel> = new Map<any, PersonModel>();
  arrayPersonas: PersonModel[];
  catalogs: Catalogs;
  devoluciones: Devolucion[] = [];
  pedido: PedidoFront;
  sortedData: PedidoFront[];
  productos: PedidoFront[];

  postPerPage = 10;
  pageNumber = 1;
  campoOrden = "updated_at";
  direccionOrden = "desc";
  inputBusqueda : "";
  checkSelected: boolean = false;
  nameProduct: string = "";

   // MatPaginator Output
  pageEvent: PageEvent;

  loading = false;
  

  constructor(
    private snack: MatSnackBar,
    private pedidosService: PedidosService,
    private cs: CatalogsService,
    private ps: PersonasService,
    private router: Router,
    public auth: AuthService,    
  ) {

  }

  ngOnInit(): void {

    
    this.loading = true;

    this.ps.personas.subscribe(persons => {
      if (persons.length === 0) {
        this.ps.getPersons().subscribe(personsdb => {
          this.arrayPersonas = persons;
          this.arrayPersonas.forEach(v => {
            this.persons.set(v.id, v);
          });
        })
      } else {
        this.arrayPersonas = persons;
        this.arrayPersonas.forEach(v => {
          this.persons.set(v.id, v);
        });
      }
    });

    this.cs.almacensLoaded.subscribe(d => {
      if (d.length === 0) {
        this.cs.loadAlmacenes().subscribe(al => {
          al.forEach(v => {
            this.almacenes.set(v.id, v);
          });
        });
      } else {
        d.forEach(v => {
          this.almacenes.set(v.id, v);
        });
      }

    });

    this.searchPedidosCoincidences(this.postPerPage,this.pageNumber,this.campoOrden,this.direccionOrden,this.inputBusqueda, false);
  }

  ngAfterViewInit(): void {
    /*
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    */
  }

  applyFilter(): void {
    this.pageNumber = 0;
    if ( this.inputBusqueda != "" ) {
      this.searchPedidosCoincidences(this.postPerPage,this.pageNumber,this.campoOrden,this.direccionOrden,this.inputBusqueda,this.checkSelected);
    }
  }

  resetQuery(): void {
    this.inputBusqueda = "";
    this.searchPedidosCoincidences(this.postPerPage,this.pageNumber,this.campoOrden,this.direccionOrden,this.inputBusqueda,false);
  }

  showDetails(id: number): void {
    this.router.navigate(['/pedidos',id]);
  }

  devolver2(r: PedidoFront): void {
    //Si el pedido no está cerrado, no podrá hacer la devolución
    if (r.NombreStatus == StatusPedido[4] ){

      let fechaActual = new Date().getTime();
      let fechaPedido = new Date(r.CreatedAt).getTime();
      var diff = fechaActual - fechaPedido;
      let numDays = diff/(1000*60*60*24);

      
      if (!this.auth.isCustomer()){
        if (r.EstatusDevolucion.replace(/\s+/g, '') != "" ){
          this.router.navigate(['/devoluciones', r.Id]); 
        }else{
          this.snack.open(`La creación de la devolución solo es permitido por el afiliado`, '',
          {
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 10000,
          });
          this.mostrarErrorValidacion(`La creación de la devolución solo es permitido por el afiliado`);
        }
      }else {
        if (numDays > 25 ) {
          this.mostrarErrorValidacion(`Las devoluciones no se permiten después de 25 días de la creación del pedido`);
        }else{
            this.router.navigate(['/devoluciones', r.Id]); 
        }
      }
    }else{
      this.mostrarErrorValidacion(`Las devoluciones únicamente es permitido al cerrarse el pedido`,);
    }
  }

  mostrarErrorValidacion(texto: string){
    this.snack.open(texto, '',
    {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 1500,
    });
  }


  searchPedidosCoincidences(limit: number, page: number,active: string, direction :string, wordsSearched: string, byProduct: boolean) {
    this.pedidosService.getPedidosCoincidences(limit, page,active, direction, wordsSearched,byProduct).pipe(
      map((productData: DataPaginator) =>         
      this.dataSource = productData,
      )
    ).subscribe( dato => {
      this.sortedData = dato.items.slice();
      this.productos = dato.items;
      this.loading = false;
    }
    );
  }

  setAll(completed: boolean) {
    this.checkSelected = completed;
  }

  sortData(sort: Sort) {
    
    const data = this.productos.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.campoOrden = sort.active;
    this.direccionOrden = sort.direction;

    this.searchPedidosCoincidences(this.postPerPage,this.pageNumber,this.campoOrden,this.direccionOrden,this.inputBusqueda,this.checkSelected);
  }
  
  onPaginate(pageEvent: PageEvent) {
    
    this.postPerPage = +pageEvent.pageSize;
    this.pageNumber = +(pageEvent.pageIndex + 1) ;

    this.searchPedidosCoincidences(this.postPerPage,this.pageNumber,this.campoOrden,this.direccionOrden,this.inputBusqueda,this.checkSelected);
    }
}