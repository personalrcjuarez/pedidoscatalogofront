import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { PedidoFront, StatusDevolucion } from 'src/app/models/pedidos';
import { Devolucion, SendDevoluciones } from 'src/app/models/devolucion';
import { DevolucionesService } from 'src/app/services/devoluciones.service';
import { PedidosService } from 'src/app/services/pedidos.service';
import Swal from 'sweetalert2';
import { AuthService } from 'src/app/services/auth.service';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { CatalogValue } from 'src/app/models/catalogs';

@Component({
  selector: 'app-devoluciones',
  templateUrl: './devolucionesNew.component.html',
  styleUrls: ['./devoluciones.component.scss']
})
export class DevolucionesComponent implements OnInit {

  displayedColumns: string[] = ['marca', 'clave', 'nombre', 'color', 'talla', 'cantidadSolicitada', 'cantidadAutorizada', 'precio', 'descuentoMarca', 'descuentoProducto', 'importe'];
  loading = false;
  pedido: PedidoFront;
  dataSource: MatTableDataSource<Devolucion>;
  errorMessage = '';
  profileForm: FormGroup;
  devoluciones: Devolucion[] = [];
  idPedido: number;
  esNuevo: boolean;
  nombreBotonPrincipal: string;
  desactivarBotonPrincipal: boolean;
  desactivarBotonCancelacion: boolean;
  desactivarCantidadesSolicitadas: boolean = false;
  desactivarCantidadesAutorizadas: boolean = false;
  solDev = "Solicitar devolución";
  aprobarDev = "Autorizar devolución";
  rechazarDev = "Rechazar devolución";

  colores: Map<any, CatalogValue>;
  tallas: Map<any, CatalogValue>;

  constructor(
    private snack: MatSnackBar,
    private router: Router,
    private ped: PedidosService,
    private devs: DevolucionesService,
    private route: ActivatedRoute,
    private cs: CatalogsService,
    public auth: AuthService,) {
    //cargar colores y tallas
    /*
this.cs.availableCatalogs.subscribe(cat => {
this.colores = cat.colores.mapValues;
this.tallas = cat.tallas.mapValues;
});
*/
  }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      if (params.id) {
        this.idPedido = params.id;
        this.ped.get(params.id).subscribe(d => {
          this.pedido = d;

          //Si el pedido ya contiene una devolución, mostrará los artículos correspondientes de la devolución
          //En caso contrario, mostrará los detalles del pedido
          if (d.EstatusDevolucion == "" || d == null) {
            this.esNuevo = true;
            this.manipulacionBoton();
            //Muestra el detalle de los pedidos
            this.devoluciones = [];
            d.DetallesFront.forEach(det => {
              let dev: Devolucion = {
                id: det.Id,
                pedidoId: Number(params.id),
                articuloId: det.ArticuloId,
                claveArticulo: det.ClaveProducto,
                nombreArticulo: det.NombreProducto,
                cantidadDevuelta: 0,
                cantidadOriginal: 0,
                cantidadInicialPedido: det.Cantidad,
                precio: det.Precio,
                subtotal: 0,
                impuesto: det.Impuesto,
                idColor: det.IdColor,
                nombreColor: det.NombreColor,
                idTalla: det.IdTalla,
                nombreTalla: det.NombreTalla,
                nombreMarca: det.NombreMarca,
                descuentoMarca: 0.0,
                descUnitMarca: det.DescUnitMarca,
                descuentoArticulo: det.DescuentoArticulo,
                descuentoArticuloFront: det.DescuentoArticulo,
                importe: 0,
                comentarios: d.Comentarios
              }

              this.desactivarCantidadesAutorizadas = true;


              this.devoluciones.push(dev);
            });

            this.dataSource = new MatTableDataSource<Devolucion>([]);
            this.dataSource.data = this.devoluciones;
          } else {
            this.esNuevo = false;
            this.manipulacionBoton();
            //Muestra los artículos de la devolución
            this.devs.get(params.id).subscribe(devol => {
              this.devoluciones = [];
              devol.forEach(det => {
                let dev: Devolucion = {
                  id: det.id,
                  pedidoId: Number(params.id),
                  articuloId: det.articuloId,
                  claveArticulo: det.claveArticulo,
                  nombreArticulo: det.nombreArticulo,
                  cantidadDevuelta: det.cantidadDevuelta,
                  cantidadOriginal: det.cantidadOriginal,
                  cantidadInicialPedido: 0,
                  precio: det.precio,
                  subtotal: det.subtotal,
                  impuesto: det.impuesto,
                  idColor: det.idColor,
                  nombreColor: det.nombreColor,
                  idTalla: det.idTalla,
                  nombreTalla: det.nombreTalla,
                  nombreMarca: det.nombreMarca,
                  descuentoMarca: det.descuentoMarca,
                  descUnitMarca: det.descUnitMarca,
                  descuentoArticulo: det.descuentoArticulo,
                  descuentoArticuloFront: det.descuentoArticulo * det.cantidadDevuelta,
                  importe: det.importe,
                  comentarios: det.comentarios
                }
                //Comparar el estatus del documento...
                switch (this.pedido.EstatusDevolucion) {                  
                  //Si la devolución es solicida
                  case StatusDevolucion[1]:                                     
                    this.desactivarCantidadesSolicitadas = true;
                    this.desactivarCantidadesAutorizadas = true;
                    if (dev.cantidadDevuelta>0 ){                     
                      dev.importe = (dev.cantidadDevuelta * dev.precio) - dev.descuentoArticuloFront - dev.descuentoMarca;
                    }else{
                      dev.importe = 0;
                    }
                    if (this.auth.isFilial()) {
                      dev.cantidadDevuelta = dev.cantidadDevuelta;
                    }
                    break;
                  //Si el estatus de la devolución es Solicitado
                  case StatusDevolucion[0]:
                    if (this.auth.isCustomer()) {
                      dev.cantidadDevuelta = 0;
                      this.desactivarCantidadesSolicitadas = true;
                      this.desactivarCantidadesAutorizadas = true;
                    }

                    if (this.auth.isFilial()) {
                      dev.cantidadDevuelta = dev.cantidadOriginal;
                      this.desactivarCantidadesSolicitadas = true;
                    }
                    break;

                  default:
                    this.desactivarCantidadesSolicitadas = true;
                    this.desactivarCantidadesAutorizadas = true;
                    break;
                }
                this.devoluciones.push(dev);
              });
              this.dataSource = new MatTableDataSource<Devolucion>([]);
              this.dataSource.data = this.devoluciones;
            }, err2 => {
              this.loading = false;
              this.errorMessage = `No se pudo cargar los productos de la devolución: ${err2.error.mensajeInformativo}`;
            })
          }

        }, err => {
          this.loading = false;
          this.errorMessage = `No se pudo cargar el pedido: ${err.error.mensajeInformativo}`;
        });

      }
    });
  }

  //type: Solicitado || Autorizado
  changeQuantity(p: Devolucion, nuevaCantidad: number, e: KeyboardEvent, type: string): void {
    if (!isNaN(nuevaCantidad)) {
      this.updateDevolucion(p, nuevaCantidad, type);
    }

    if (this.getTotalCost() > 0) {
      this.desactivarBotonPrincipal = false;
    } else {
      this.desactivarBotonPrincipal = true;
    }
  }

  //type: Solicitado || Autorizado
  updateDevolucion(dev: Devolucion, cantidad: number, type: string) {
    this.devoluciones.forEach(devs => {
      if (devs.articuloId == dev.articuloId && devs.idTalla == dev.idTalla && devs.idColor == dev.idColor) {
        this.updDev(dev, cantidad, type);
      }
    });
  }

  //type: Solicitado || Autorizado
  updDev(devs: Devolucion, cantidad: number, type: string) {
    devs.descuentoArticuloFront = (cantidad * devs.descuentoArticulo);
    devs.descuentoMarca = cantidad * devs.descUnitMarca;
    devs.impuesto = (cantidad * devs.impuesto);
    devs.subtotal = cantidad * devs.precio;
    devs.importe = devs.subtotal + devs.impuesto - devs.descuentoMarca - devs.descuentoArticuloFront;
    switch (type) {
      case "Solicitado":
        devs.cantidadOriginal = cantidad;
        break;
      case "Autorizado":
        devs.cantidadDevuelta = cantidad;
        break;

    }
  }

  solicitarDevolucion() {

    if (this.getTotalCost() > 0) {
      let error = 0;
      let devsReal: Devolucion[] = [];
      this.devoluciones.forEach(elem => {
        if (elem.cantidadOriginal > 0) {
          elem.pedidoId = Number(elem.pedidoId);
          devsReal.push(elem);
        }

        if (elem.cantidadOriginal > elem.cantidadOriginal) {
          error++;
        }
      });

      if (error > 0) {
        this.snack.open(`En alguno de los productos está devolviendo más de lo permitido`, '',
          {
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 10000,
          });
      } else {
        Swal.fire({
          title: "¿Está seguro de solicitar la devolución?",
          text: "",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sí',
          cancelButtonText: 'No',
        }).then((xxxx) => {
          if (xxxx.isConfirmed) {

            let devs = {} as SendDevoluciones;
            devs.devolucions = devsReal;
            this.devs.create(devs).subscribe(d => {
              this.snack.open(`Devolución creado con éxito`, '', {
                duration: 4000,
              });
              this.loading = false;
              this.router.navigate(['/pedidos']);
            }, error => {
              console.log(error);
              this.snack.open(`La devolución no se pudo crear: ${error.error.mensajeInformativo}`, '', {
                duration: 4000,
              });
              this.loading = false;
            });

            this.loading = false;
          }
        })
      }
    }
  }

  manipulacionBoton() {

    if (this.auth.isCustomer()) {
      this.nombreBotonPrincipal = this.solDev;
      this.desactivarBotonCancelacion = true;
      //Si ya está autorizado, desactivar todos los botones        
      if (this.pedido.EstatusDevolucion === StatusDevolucion[1]) {
        this.desactivarBotonCancelacion = this.desactivarBotonPrincipal = true;
        //this.desactivarBotonPrincipal = true;
      }
      if (this.getTotalCantidad() == 0 || this.pedido.EstatusDevolucion === StatusDevolucion[1]) {
        this.desactivarBotonPrincipal = true;
      } else {
        this.desactivarBotonPrincipal = false;
      }
    }

    if (this.auth.isFilial()) {
      if (this.esNuevo) {
        this.nombreBotonPrincipal = this.solDev;
        this.desactivarBotonCancelacion = true;
      } else {
        this.nombreBotonPrincipal = this.aprobarDev;
        //Si ya está autorizado, desactivar todos los botones
        if (this.pedido.EstatusDevolucion === StatusDevolucion[1]) {
          this.desactivarBotonCancelacion = true;
          this.desactivarBotonPrincipal = true;
        }
      }
    }
  }

  accionRechazo() {
    Swal.fire({
      title: "¿Está seguro de rechazar la devolución?",
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No',
    }).then(xxxx => {
      if (xxxx.isConfirmed) {
        //let devs = {} as SendDevoluciones;
        //devs.devolucions = this.devoluciones;
        this.pedido.EstatusDevolucion = StatusDevolucion[2];
        this.ped.updateStatusDevolucion(this.pedido).subscribe(d => {
          this.snack.open(`Se ha actualizado el estatus de la devolución`, '', {
            duration: 4000,
          });
          this.router.navigate(['/pedidos']);
        }, err => {
          this.loading = false;
          this.snack.open(`No se pudo actualizar el estatus de la devolución: ${err.error.mensajeInformativo}`, '', {
            duration: 4000,
          });
        });
      }
    }
    );
  }

  accionBotonDevolucion() {
    switch (this.nombreBotonPrincipal) {
      case this.solDev:
        this.solicitarDevolucion();
        break;
      case this.aprobarDev:
        this.aprobarDevolucion(this.getTotalDto());
        break;

      default:
        console.log("No se encontraron acciones")
        break;
    }
  }

  aprobarDevolucion(saldo: number) {

    Swal.fire({
      title: "¿Está seguro de aprobar la devolución?",
      text: "Se le abonará " + Math.round((saldo + Number.EPSILON) * 100) / 100 + " al saldo del afiliado",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No',
    }).then(xxxx => {
      if (xxxx.isConfirmed) {
        let devs = {} as SendDevoluciones;
        devs.devolucions = this.devoluciones;
        this.devs.update(this.idPedido, devs).subscribe(d => {
          this.snack.open(`Se ha aprobado con éxito la devolución`, '', {
            duration: 4000,
          });
          this.router.navigate(['/pedidos']);
        }, err => {
          this.loading = false;
          this.snack.open(`No se pudo aprobar la devolución: ${err.error.mensajeInformativo}`, '', {
            duration: 4000,
          });
        });
      }
    }
    );
  }

  /** Gets the total cost of all transactions. */
  getTotalCost() { return this.devoluciones.map(t => t.importe).reduce((acc, value) => acc + value, 0); }

  getTotalCantidad() { return this.devoluciones.map(t => t.cantidadDevuelta).reduce((acc, value) => acc + value, 0); }

  getTotalCantidadSolicitada() { return this.devoluciones.map(t => t.cantidadOriginal).reduce((acc, value) => acc + value, 0); }

  getDescuentoMarca() { return this.devoluciones.map(t => t.descuentoMarca).reduce((acc, value) => acc + value, 0); }

  getDescuentoProducto() {  return this.devoluciones.map(t => t.descuentoArticuloFront).reduce((acc, value) => acc + value, 0);  }

  getCalcDtoCte(): number { return (this.getTotalCost() * this.pedido.PorcDescCliente) / 100; }

  getCalcDtoAdic(): number { return (this.getTotalCost() * this.pedido.PorcDescAdic) / 100; }

  getTotalDto(): number {  return this.getTotalCost() - this.getCalcDtoCte() - this.getCalcDtoAdic(); }

}
