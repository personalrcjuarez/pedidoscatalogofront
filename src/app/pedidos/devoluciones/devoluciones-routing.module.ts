import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { DevolucionesComponent } from './devoluciones.component';

const routes: Routes = [
  {
    path: ':id', component:DevolucionesComponent, data: {
      title: 'Detalle devolucion',
      breadcrumb: 'Devolucion',
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevolucionesRoutingModule {
}