import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DevolucionesRoutingModule } from './devoluciones-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DevolucionesComponent } from '.././devoluciones/devoluciones.component';
@NgModule({
  declarations: [ DevolucionesComponent],
  imports: [
    CommonModule,
    DevolucionesRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class DevolucionesModule { }