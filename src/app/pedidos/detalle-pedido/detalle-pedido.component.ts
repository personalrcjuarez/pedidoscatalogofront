import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { DetallesFront, PedidoFront, StatusDevolucion, StatusPedido } from '../../models/pedidos';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { PedidosService } from '../../services/pedidos.service';
import { AuthService } from '../../services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalObservacionesComponent } from '../modal-observaciones/modal-observaciones.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ModalPagosComponent } from './modal-pagos/modal-pagos.component';
import { ModalHistPagosComponent } from './modal-hist-pagos/modal-hist-pagos.component';
import { Pago } from 'src/app/models/pagos';
import { CatalogsService } from '../../services/catalogs.service';
import { Marca, ArticuloFront, ArticleFull } from '../../models/articles';
import { FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { ProductsService } from '../../services/products.service';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { PersonasService } from 'src/app/services/personas.service';
import { SelectionModel } from '@angular/cdk/collections';
import { calcularDescuentoArticulo, calcularDescuentoMarca, calcularImporte, calcularImpuesto, validateExistOrdersOptimized } from '../nuevo-pedido/nuevo-pedido.component';
import { ModalProductosComponent } from '../modal-productos/modal-productos.component';
import { APIResponse } from 'src/app/models/common';
import { StatusArticuloInventario } from 'src/app/models/almacenes';

@Component({
  selector: 'app-detalle-pedido',
  templateUrl: './detalle-pedido.component.html',
  styleUrls: ['./detalle-pedido.component.scss']
})

export class DetallePedidoComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['check', 'marca', 'clave', 'nombre', 'color', 'talla', 'cantidad', 'precio', 'descuentoMarca', 'descuentoProducto', 'importe', 'actions'];
  loading = false;
  name: string;
  pedido: PedidoFront;
  dataSource: MatTableDataSource<DetallesFront>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  editable: boolean = false;
  errorMessage = '';
  productInput = new FormControl();
  marcas: Map<any, Marca> = new Map<any, Marca>();
  filteredProducts: Observable<ArticuloFront[]> = of([]);
  datosAll: APIResponse = null;
  public selection = new SelectionModel<any>(true, []);
  subtotal = 0;
  StatusPedido = StatusPedido;
  pagos = new Pago;

  constructor(
    private cliServ: PersonasService,
    private route: ActivatedRoute,
    private ps: PedidosService,
    private p: ProductsService,
    private cs: CatalogsService,
    public auth: AuthService,
    private dialog: MatDialog,
    private snack: MatSnackBar,
    private router: Router,
  ) { this.dataSource = new MatTableDataSource<DetallesFront>([]); }

  ngOnInit(): void {
    this.loading = true;

    this.route.params.subscribe(params => {
      if (params.id) {
        this.loading = true;
        this.ps.getOptimized(params.id).subscribe(d => {

          this.pedido = d;
          this.manipulacionBotones();

          this.dataSource.data = this.pedido.DetallesFront;
          this.loading = false;

          this.pedido.DetallesFront.forEach(p => {
            p.PrecioMoneda = p.Precio;
            p.ImporteBruto = p.Precio * p.Cantidad;
            this.subtotal += p.ImporteBruto;
            p.DescuentoArticuloFront = p.DescuentoArticulo * p.Cantidad;
          });

          this.subtotal = this.subtotal + 0 - this.pedido.TotalDescuento;
          this.pedido.Anticipo = this.pedido.TotalGeneral * .30;

        }, err => {
          this.loading = false;
          this.errorMessage = `No se pudo cargar el pedido: ${err.error.mensajeInformativo}`;
        });
      }
    });

    //GET ALL MARCAS
    this.cs.getMarcasLoaded.subscribe(mrc => {
      if (mrc.length === 0) {
        this.cs.loadMarcas().subscribe(xx => {
        })
      }
    })

  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  aprobarDescuento(): void {
    this.dialog.open(ModalObservacionesComponent, {
      data: { title: 'Aprobar descuento' },
    }).afterClosed().subscribe(result => {
      if (result) {
        this.pedido.Comentarios = result;
        this.pedido.NombreStatus = StatusPedido[2];
        this.pedido.AutorizaDesc = true;
        this.updateStatusPedido('');
      }
    });
  }

  updateStatusPedido(accion: string): boolean {
    let exitoso = false;
    this.loading = true;

    switch (accion) {
      case "Surtido":
        this.ps.callSurtirPedido(this.pedido).subscribe(d => {

          if (d.codigo == 206)  {
            this.snack.open(`No se pudo actualizar el pedido: ${d.mensajeError}`, 'Articulos inexistentes',
            {
              horizontalPosition: 'center',
              verticalPosition: 'top',
              duration: 10000,
            });      
            exitoso = true;
            
            this.pedido.DetallesFront.forEach(detFO => {              
              const index = d.object.detallePedidos.findIndex(a => a.IdCatalogoTallasColores === detFO.IdCatalogoTallasColores);
              if (index >= 0) {
                detFO.EstatusInventario = d.object.detallePedidos[index].EstatusInventario;
                if (detFO.EstatusInventario == StatusArticuloInventario[0] || detFO.EstatusInventario == StatusArticuloInventario[1]){
                  exitoso = false;
                }
              }
            });           
          }
          this.loading = false;
          if (exitoso){
            this.manipulacionBotones(); 
          }                            
        }, err => {
          this.loading = false;
          this.snack.open(`No se pudo actualizar el pedido: ${err.error.mensajeInformativo}`, '',
            {
              horizontalPosition: 'center',
              verticalPosition: 'top',
              duration: 10000,
            });
        });
        break;    
      default:
        this.ps.updateStatusOptimized(this.pedido,accion).subscribe(d => {
          this.pedido = d;
          this.loading = false;
          this.manipulacionBotones();
          exitoso = true;
        }, err => {
          this.loading = false;
          this.snack.open(`No se pudo actualizar el pedido: ${err.error.mensajeInformativo}`, '',
            {
              horizontalPosition: 'center',
              verticalPosition: 'top',
              duration: 10000,
            });
        });
        break;
    }
    return exitoso;
  }

  editar(): void {
    //Si el estatus está en "Revisado"
    if (this.pedido.NombreStatus == StatusPedido[3]) {
        //Si es encargado
        if (this.auth.isFilial()){
          switch (this.pedido.EditarPedido) {
            //Estatus Solicitado
            case StatusDevolucion[0]:
              this.showMessage("Esperando la autorización del administrador", "Autorización solicitada");
              break;
            //Estatus Autorizado
            case StatusDevolucion[1]:
              this.editable = !this.editable;
              break;
            //Estatus Rechazado
            case StatusDevolucion[2]:
              this.showMessage("El administrador no autorizó la edición del pedido", "Edición rechazada");
              break;
            default:              
              this.mostrarOpcion("¿Solicitar autorización para editar el pedido?");
              break;
          }
        }
        //Si es admin
        if (this.auth.isAdmin()){
          switch (this.pedido.EditarPedido) {
            //Estatus Solicitado
            case StatusDevolucion[0]:
              this.autorizarEdicion();
              break;
            //Estatus Autorizado
            case StatusDevolucion[1]:
              this.editable =false;
              this.showMessage("Ha autorizado la edición del pedidio", "Edición autorizada");
              break;
            //Estatus Rechazado
            case StatusDevolucion[2]:
              this.editable = false;
              this.showMessage("Ha rechazado la edición del pedido", "Edición rechazada");
              break;
            default:
              this.editable = false;
              this.showMessage("Aun no se le ha solicitado la edición del pedido", "Solicitud pendiente");
              break;
          }
        }
    }else{
      this.editable = !this.editable;
    }
  }

  mostrarOpcion(titulo: string){
    Swal.fire({
      title: titulo,
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.isConfirmed) {      
        //Establecer el pedido como "Solicitado"
        this.pedido.EditarPedido = StatusDevolucion[0];  
        this.ps.updateStatusOrderEdit(this.pedido).subscribe(d => {
          this.pedido = d;
          this.loading = false;
          this.manipulacionBotones();
        }, err => {
          console.log(err);
          this.loading = false;
          this.showMessage(`No se pudo actualizar el pedido: ${err.error.mensajeInformativo}`, "Error");
        });
      }
    });    
  }

  autorizarEdicion() {
    Swal.fire({
      title: "¿Seguro de autorizar la edición del pedido?",
      text: "",
      icon: 'warning',
      showCancelButton: true,
      showDenyButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'Cancelar',
      denyButtonText: 'Denegar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.updateEstatusOrderEdit(1);
      }
      if (result.isDenied) {
        this.updateEstatusOrderEdit(2);
      }
    });
  }

  updateEstatusOrderEdit(type: number) {
    this.pedido.EditarPedido = StatusDevolucion[type];
    this.ps.updateStatusOrderEdit(this.pedido).subscribe(d => {
      this.pedido = d;
      this.loading = false;
      this.manipulacionBotones();
      if (type == 1) { this.showMessage(`Ha autorizado la edición del pedido`, "Edición autorizada"); }
      if (type == 2) { this.showMessage(`Ha denegado la edición del pedido`, "Edición denegada"); }
    }, err => {
      console.log(err);
      this.loading = false;
      this.showMessage(`No se pudo actualizar el pedido: ${err.error.mensajeInformativo}`, "Error");
    });
  }

  showMessage(text: string, title: string) {
    this.snack.open(text, title,
      {
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
        duration: 10000,
      });
  }

  addProduct(): void {
    let prductOri: ArticuloFront = this.productInput.value;

    this.p.getByTallaColors(prductOri.Id, this.pedido.ClienteId).subscribe(prod => {

      this.openDialog(prod.object);

      this.updatePedido();

      this.dataSource.data = this.pedido.DetallesFront;

    });
  }

  addProductOptimized(): void {
    let prductOri: ArticuloFront = this.productInput.value;
    this.p.getByTallaColors(prductOri.Id, this.pedido.ClienteId).subscribe(prod => {
      this.openDialog(prod.object);

      this.updatePedido();

      this.dataSource.data = this.pedido.DetallesFront;

    });
  }

  savePedido(): void {

    this.loading = true;

    this.ps.update(this.pedido).subscribe(d => {
      this.pedido = d;
      this.loading = false;

      this.desactivarBoton = false;
      this.editable = false;

      this.snack.open(`Pedido actualizado con exito`, '', {
        duration: 4000,
      });

      this.router.navigate(['/pedidos']);

    }, err => {
      console.log(err);
      this.loading = false;
      this.snack.open(`No se pudo actualizar el pedido: ${err.error.mensajeInformativo}`, '',
        {
          horizontalPosition: 'center',
          verticalPosition: 'top',
          duration: 10000,
        });
    });
  }

  accionBotonPedido(): void {
    switch (this.nombreAccionBotonPedido) {
      case this.autorizaPedido:
        this.accionStatusPedido('¿Está seguro de autorizar el pedido?', StatusPedido[9], "autorizar");
        break;
      //Estatus del pedido será "Revisado"
      case this.confirmaPedido:
        this.accionStatusPedido('¿Está seguro de haber revisado el pedido?', StatusPedido[3], "seguro");
        break;
      case this.solicitarPedido:
        //Estatus del pedido será "Enviado"
        this.accionStatusPedido('¿Está seguro de solicitar el pedido?', StatusPedido[9], "solicitar");
        break;
      case this.solicitarConfirmacion:
        //Estatus del pedido será "Por revisar"
        this.accionStatusPedido('¿Está seguro de solicitar revisión del pedido?', StatusPedido[7], "solicitar revisión");
        break;
      case this.surtirPedido:
        //Estatus del pedido será "Surtido"
        this.accionStatusPedido('¿Está seguro de surtir el pedido?', StatusPedido[11], "surtir");
        break;
      case this.recibirProductos:
        //Estatus del pedido será "Recibido"
        this.accionStatusPedido('¿Está seguro de confirmar el pedido?', StatusPedido[13], "confirmar de recibido");
        break;
      case this.cerrarPedido:
        //Estatus del pedido será "Cerrado"
        this.accionStatusPedido('¿Está seguro de cerrar el pedido?', StatusPedido[4], "cerrar");
        break;
      default:
        console.log("No hará ninguna acción: ",)
        break;
    }
  }

  accionStatusPedido(titulo: string, nombreStatus: string, vervoAccion: string): void {
    Swal.fire({
      title: titulo,
      text: "¡Una vez realizado la acción, no se podrá revertir!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, ' + vervoAccion
    }).then((result) => {
      if (result.isConfirmed) {
        //Establecer el estatus Enviado
        this.pedido.NombreStatus = nombreStatus;
        let exitoso = this.updateStatusPedido(nombreStatus);
        if (exitoso){
          this.router.navigate(['/pedidos']);
        }
      }
    })
  }
  /*
    Acciones para el botón multifunción
  */
  nombreAccionBotonPedido: string;
  //acciones del botón
  autorizaPedido = "Autorizar pedido";
  confirmaPedido = "Confirmar revisión";
  solicitarPedido = "Solicitar pedido";
  solicitarConfirmacion = "Solicitar revisión";
  porSurtir = "Pendiente de surtir";
  porRecibir = "Pendiente de recibir";
  surtirPedido = "Surtir pedido";
  recibirProductos = "Confirmar recibido";
  cerrarPedido = "Cerrar pedido";

  desactivarBoton = true;
  desactivarBotonEditar = true;
  desactivarBotonPago = true;
  desactivarBotonCancelacion = false;

  manipulacionBotones(): void {

    //Manipulacipon de las acciones del botón
    switch (this.pedido.NombreStatus) {

      //1.- Estatus "Abierto" => "Enviar pedido"
      case StatusPedido[8]:
        if (this.auth.isCustomer()) {
          this.desactivarBoton = false;
          this.desactivarBotonEditar = false;
          this.nombreAccionBotonPedido = this.solicitarPedido;
        }

        break;

      //Estatus "Enviado" => "Confirmar pedido"
      case StatusPedido[9]:
        if (this.auth.isCustomer()) {
          this.desactivarBotonEditar = true;
          this.desactivarBoton = true;
          this.nombreAccionBotonPedido = this.confirmaPedido;
        }
        if (this.auth.isFilial()) {
          this.desactivarBoton = false;
          this.desactivarBotonEditar = false;
          this.nombreAccionBotonPedido = this.solicitarConfirmacion;
        }
        break;

      //Estatus "Por revisar" => "Revisado"
      case StatusPedido[7]:
        if (this.auth.isCustomer()) {
          this.desactivarBotonEditar = true;
          this.desactivarBoton = false;
          this.nombreAccionBotonPedido = this.confirmaPedido;
        }
        if (this.auth.isFilial()) {
          this.desactivarBotonEditar = false;
          this.desactivarBoton = true;
          this.nombreAccionBotonPedido = this.confirmaPedido;
        }
        break;

      //Estatus "Revisado" => "Recibir productos"
      case StatusPedido[3]:
        if (this.pedido.SaldoActual != this.pedido.TotalGeneral) {
          this.desactivarBotonCancelacion = true;
          this.desactivarBotonEditar = true;
        } else {
          this.desactivarBotonEditar = false;
        }

        if (this.auth.isCustomer()) {
          this.desactivarBotonEditar = true;
          this.desactivarBoton = true;
          this.nombreAccionBotonPedido = this.recibirProductos;
        }
        
        if (this.auth.isAdmin() && this.pedido.EditarPedido == "") {
            this.desactivarBotonEditar = true;
        }

        this.accionBtonSurtir();
        break;

      //Estatus "Por surtir" => "Surtido"
      case StatusPedido[10]:
        this.desactivarBotonPago = false;
        this.desactivarBotonCancelacion = true;
        if (this.auth.isFilial()) {
          this.nombreAccionBotonPedido = this.surtirPedido;
          this.desactivarBoton = false;
        }
        break;

      /*
        Cambio de lógica, ahora de surtido, se habilitará el botón para inmediatamente cerrar el pedido
        Actualizacion 30/03/2022
      */
      //Estatus "Surtido" => "Por recibir" //descontinuado
      //Estatus "Surtido" => "Cerrado"
      case StatusPedido[11]:
        this.desactivarBotonCancelacion = true;
        /*
        if (this.auth.isCustomer()) {
          this.nombreAccionBotonPedido = this.recibirProductos;
          this.desactivarBoton = false;
        }
        if (this.auth.isFilial()) {
          this.nombreAccionBotonPedido = this.porRecibir;
          this.desactivarBoton = true;
        }
        */
        this.nombreAccionBotonPedido = this.cerrarPedido;
        this.desactivarBoton = false;
        break;

      //Estatus "Recibido" => "Cerrado"
      case StatusPedido[13]:
        this.desactivarBotonCancelacion = true;
        if (this.auth.isCustomer()) {
          this.nombreAccionBotonPedido = this.recibirProductos;
          this.desactivarBoton = true;
        }
        if (this.auth.isFilial()) {
          this.nombreAccionBotonPedido = this.cerrarPedido;
          this.desactivarBoton = false;
        }
        break;

      //Estatus "Cancelado" => "Cancelado"
      case StatusPedido[6]:
        this.desactivarBotonCancelacion = true;
        this.desactivarBoton = true;
        this.desactivarBotonPago = true;
        this.editable = false;
        break;

      //Estatus "Pendiente por Autorizar" => "Autorizar pedido"
      case StatusPedido[1]:
        this.nombreAccionBotonPedido = this.autorizaPedido;
        break;

      default:
        this.nombreAccionBotonPedido = "NINGUNA ACCIÓN";
        this.desactivarBotonPago = false;
        break;
    }
  }

  accionBtonSurtir(): void {
    if (this.auth.isFilial()) {
      let redondeo = Math.round(this.pedido.SaldoActual * 100) / 100;
      if (redondeo > 0) {
        this.desactivarBotonPago = false;
        this.desactivarBoton = true;
      } else {
        this.desactivarBoton = false;
      }
      this.nombreAccionBotonPedido = this.surtirPedido;
      //Si el estatus es Surtido, desactivar el botón "Surtir pedido"
      if (this.pedido.NombreStatus == StatusPedido[11]
        || this.pedido.NombreStatus == StatusPedido[4]) {
        this.desactivarBoton = true;
      }
    }
  }

  openDialog(prod: ArticleFull[]): void {
    const dialogRef = this.dialog.open(ModalProductosComponent, {
      maxWidth: '100vw',
      height: '85vh',
      maxHeight: '85vh',
      closeOnNavigation: true,
      data: prod,
    });

    dialogRef.afterClosed().subscribe((result: Set<ArticleFull>) => {
      if (result.size > 0) {
        //vacir el mat autocomplete
        //y limpiar el input
        this.productInput.setValue("");
        this.filteredProducts = null;

        result.forEach((valBefPedido: ArticleFull) => {

          /* if (!this.validateExistOrders(valBefPedido)) { */
          if (!validateExistOrdersOptimized(valBefPedido, this.pedido.DetallesFront)) {

            let calculoTipoMoneda = valBefPedido.Precio * this.pedido.TipoCambio;

            //let porcentajeMarca = this.selectedCustomer?.descuentoMarcas;
            //let descuentoMarca = 0;
            //let porcDescMarca = 0;
            //determinar el porcentaje de la marca para posteriormente realizar los cálculos
            /*
            porcentajeMarca?.forEach(element => {
              if (valBefPedido.producto.marcaId == element.idMarca) {
                porcDescMarca = element.porcentajeDescuento;
                descuentoMarca = calcularDescuentoMarca(calculoTipoMoneda, 1, element.porcentajeDescuento);
              }
            });
            */

            this.pedido.DetallesFront.push({
              Id: 0,
              ArticuloId: valBefPedido.IdArticulo,
              NombreMarca: valBefPedido.NombreMarca,
              PorcDescMarca: valBefPedido.PorcentajeDescuentoMarca,
              ClaveProducto: valBefPedido.Clave,
              NombreProducto: valBefPedido.Nombre,
              IdColor: valBefPedido.IdColor,
              NombreColor: valBefPedido.Color,
              IdTalla: valBefPedido.IdTalla,
              NombreTalla: valBefPedido.NombreTalla,
              Cantidad: 1,
              Precio: calculoTipoMoneda,
              Subtotal: 0,
              Impuesto: calcularImpuesto(calculoTipoMoneda, 1, null),
              CantDescMarca: calcularDescuentoMarca(calculoTipoMoneda, 1, valBefPedido.PorcentajeDescuentoMarca),
              DescuentoArticulo: calcularDescuentoArticulo(calculoTipoMoneda, 1, valBefPedido.PorcentajeDescuentoArticulo, valBefPedido.EnOferta),
              DescuentoArticuloFront: calcularDescuentoArticulo(calculoTipoMoneda, 1, valBefPedido.PorcentajeDescuentoArticulo, valBefPedido.EnOferta),
              Importe: (calcularImporte(calculoTipoMoneda, 1,
                calcularImpuesto(calculoTipoMoneda, 1, null),
                calcularDescuentoMarca(calculoTipoMoneda, 1, valBefPedido.PorcentajeDescuentoMarca),
                calcularDescuentoArticulo(calculoTipoMoneda, 1, valBefPedido.PorcentajeDescuentoArticulo, valBefPedido.EnOferta))
              ),
              PrecioMoneda: calculoTipoMoneda,
              CostoUltimo: valBefPedido.Costo,
              IdMarca: valBefPedido.IdMarca,
              ImporteBruto: calculoTipoMoneda,

              EnOferta: valBefPedido.EnOferta,
              PorcDescArticulo: valBefPedido.PorcentajeDescuentoArticulo,
              DescUnitMarca: 0,
              IdCatalogoTallasColores: valBefPedido.IdCatalogoTallasColores,
              EstatusInventario: '',
            });
          }
        });
        //this.productInput.setValue('');
        this.updatePedido();
        this.dataSource.data = this.pedido.DetallesFront;
      }
    });
  }

  searchCoincidences(input: any): void {
    if (input != "") {
      this.p.getCoincidencesProduct(input).pipe(
        map((productData: APIResponse) =>
          this.datosAll = productData,
        )
      ).subscribe(dato => {
        if (dato.object.length > 0) {
          this.filteredProducts = this.productInput.valueChanges
            .pipe(
              startWith(''),
              map(value => {
                return dato.object;
              })
            );
        } else {
          if (dato.codigo == 200) {
            this.snack.open(`No hay coincidencias para el artículo: [` + input + "]", 'Producto inexistente',
              {
                horizontalPosition: 'center',
                verticalPosition: 'bottom',
                duration: 5000,
              });
          } else {
            this.snack.open("[" + dato.mensajeError + "]", 'Error al consultar: [' + dato.codigo + ']',
              {
                horizontalPosition: 'center',
                verticalPosition: 'bottom',
                duration: 10000,
              });
          }
        }
      }
      );
    }
  }

  updatePedido(): void {
    let subtotalTemp = 0;
    let impuestos = 0;
    let descuento = 0;
    //let descuentosMarcas = this.selectedCustomer.descuentoMarcas;
    let descuentosMarcas = this.pedido.cliente?.descuentoMarcas;

    this.pedido.DetallesFront.forEach(p => {
      p.Precio = p.PrecioMoneda;
      //determinar el porcentaje de la marca para posteriormente realizar los cálculos
      descuentosMarcas?.forEach(element => {
        if (p.IdMarca === element.idMarca) {
          p.PorcDescMarca = element.porcentajeDescuento;
          //p.descuentoMarca = calcularDescuentoMarca(p.precioMoneda, p.cantidad, element.porcentajeDescuento);
          p.CantDescMarca = calcularDescuentoMarca(p.PrecioMoneda, p.Cantidad, element.porcentajeDescuento);
        }
      });
      subtotalTemp += p.ImporteBruto;
      impuestos += (p.Impuesto * this.pedido.TipoCambio);
      descuento += (p.CantDescMarca + p.DescuentoArticuloFront);
    });

    this.pedido.PorcDescAdic = Number(this.pedido.PorcDescAdic) || 0;
    this.pedido.TotalDescuento = descuento;
    this.pedido.TotalImporte = subtotalTemp;
    //this.pedido.TotalImpuesto = impuestos;
    this.subtotal = subtotalTemp + impuestos - descuento;
    this.pedido.DescuentoCliente = this.subtotal * (this.pedido.PorcDescCliente / 100);
    this.pedido.CantDescAdic = (this.subtotal - this.pedido.DescuentoCliente) * (this.pedido.PorcDescAdic / 100);
    this.pedido.TotalGeneral = this.subtotal - this.pedido.DescuentoCliente - this.pedido.CantDescAdic;
    this.pedido.SaldoActual = this.pedido.TotalGeneral;
    this.pedido.Anticipo = this.pedido.TotalGeneral * .30;

    //Si el estatus del pedido es "Enviado" y el usuario es un encargado
    //actualizará el estatus del pedido a "Por Confirmar"
    if (this.pedido.NombreStatus === StatusPedido[9]) {
      this.pedido.NombreStatus = StatusPedido[7]
    }

  }

  displayProductFn(p: ArticuloFront): string {
    return p ? `${p.Clave} - ${p.NombreProducto}` : '';
  }

  openPanel(evt: any, trigger: MatAutocompleteTrigger, words: string): void {
    if (words != "") {
      this.searchCoincidences(words);
      evt.stopPropagation();
      trigger.openPanel();
    }
  }

  public checkboxLabel(row?: any): string {
    return (!row)
      ? `${this.isAllSelected() ? 'select' : 'deselect'} all`
      : `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  public isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /*
  changeQuantityProduct(p: ProductoPedido, nuevaCantidad: number, e: KeyboardEvent): void {
    if (isNaN(nuevaCantidad)) {
      p.cantidad = 1;
      this.updateProduct(p, 1);
      return;
    }
    p.cantidad = nuevaCantidad;
    this.updateProduct(p, p.cantidad);
    return;
  }
  */
  changeQuantityProductOptimized(p: any, nuevaCantidad: number, e: KeyboardEvent): void {

    if (isNaN(nuevaCantidad)) {
      p.Cantidad = 1;
      this.updateProductOptimized(p, 1);
      return;
    }
    p.Cantidad = nuevaCantidad;
    this.updateProductOptimized(p, p.Cantidad);
    return;
  }

  updateProductOptimized(p: DetallesFront, nuevaCantidad: any): void {
    p.DescuentoArticulo = calcularDescuentoArticulo(p.PrecioMoneda, 1, p.PorcDescArticulo, p.EnOferta);
    p.DescuentoArticuloFront = calcularDescuentoArticulo(p.PrecioMoneda, nuevaCantidad, p.PorcDescArticulo, p.EnOferta);
    //p.impuesto = calcularImpuesto(p.precioMoneda, nuevaCantidad, this.mapImpuestos.get(p.articulo.impuestoId));
    p.CantDescMarca = calcularDescuentoMarca(p.PrecioMoneda, p.Cantidad, p.PorcDescMarca);
    p.Importe = calcularImporte(p.PrecioMoneda, nuevaCantidad, p.Impuesto, p.CantDescMarca, p.DescuentoArticuloFront);
    p.ImporteBruto = p.PrecioMoneda * nuevaCantidad;
    this.updatePedido();
  }

  removeProduct(r: DetallesFront): void {

    Swal.fire({
      title: "¿Está seguro de quitar éste producto?",
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.isConfirmed) {
        const index = this.pedido.DetallesFront.findIndex(d => d.ArticuloId === r.ArticuloId);
        if (index >= 0) {
          this.pedido.DetallesFront.splice(index, 1);
          this.dataSource.data = this.pedido.DetallesFront;
        }
        this.updatePedido();
      }
    });
  }

  historialPagos(): void {
    this.dialog.open(ModalHistPagosComponent, {
      width: '700px',
      data: { pedido: this.pedido }
    }).afterClosed().subscribe(result => {
    });
  }

  cancelar(): void {
    //Si el estatus del pedido es "Abierto"
    //Solo mostrar el mensaje de cancelación, en caso contrario, mostrar la advertencia de que se le cobrará un 5% de penalización
    let texto = "¡Una vez realizado la acción, no se podrá revertir!";
    if (this.pedido.NombreStatus != StatusPedido[8]) {
      texto = "Si lo cancela se le cobrará el 5% de penalización";
    }

    Swal.fire({
      title: "¿Está seguro de cancelar éste pedido?",
      text: texto,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        //Establecer el estatus Cancelado
        this.pedido.NombreStatus = StatusPedido[6];
        this.updateStatusPedido('');
      }
    })
  }

  confirmarAnticipo(): void {
    let texto = "";
    let saldoPedidoMayor = true;
    if (this.pedido.SaldoFavor > 0 &&
      this.pedido.TotalGeneral == this.pedido.SaldoActual) {
      if (this.pedido.TotalGeneral > this.pedido.SaldoFavor) {
        texto = "Usted tiene un saldo a su favor por: " + this.pedido.SaldoFavor + " ¿Desea abonarlo a éste pedido?";
        saldoPedidoMayor = true;
      } else {
        texto = "Usted tiene un saldo a su favor por: " + this.pedido.SaldoFavor + " ¿Desea liquidar éste pedido con parte de su saldo?";
        saldoPedidoMayor = false;
      }
      this.solicitarConfirmacionAbono(texto, saldoPedidoMayor);
    } else {
      this.mostrarDialogoPagos();
    }
  }

  solicitarConfirmacionAbono(texto: string, saldoPedidoMayor: boolean) {
    Swal.fire({
      title: texto,
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.isConfirmed) {
        //mandar a abonar el pago 

        let pago: Pago = new Pago();

        pago.idUsuario = this.auth.user.userId;
        pago.saldoAnterior = this.pedido.SaldoActual;
        if (saldoPedidoMayor) {
          pago.pago = this.pedido.SaldoFavor;
        } else {
          pago.pago = this.pedido.TotalGeneral;
        }

        pago.saldoActual = this.pedido.SaldoActual - pago.pago;
        this.pedido.SaldoActual = pago.saldoActual;
        pago.referencia = "Pago generado por el saldo a favor del cliente";
        pago.PedidoId = this.pedido.Id;

        this.createPagos(pago, true);

      } else {
        this.mostrarDialogoPagos();
      }
    });
  }

  createPagos(pago: Pago, actualizaSaldoCliente: Boolean): void {
    this.loading = true;
    this.ps.createPagos(pago).subscribe(d => {
      this.pagos = d;
      this.loading = false;

      if (actualizaSaldoCliente) {
        this.pedido.SaldoFavor -= pago.pago;
        this.cliServ.updateClientBalance(this.pedido.ClienteId, this.pedido.SaldoFavor).subscribe(sub => {
          this.snack.open(`Pago realizado con éxito. Saldo del cliente actualizado con éxito`, '', {
            duration: 4000,
          });
        }, err => {
          this.snack.open(`No se pudo actualizar el saldo del cliente, consultelo con su administrador: ${err.error.mensajeInformativo}`, '', {
            duration: 4000,
          });
        })
      } else {
        if (this.pedido.SaldoActual == 0) {
          //Establecer el estatus "Por surtir"
          this.pedido.NombreStatus = StatusPedido[10];
          this.updateStatusPedido('');
          this.desactivarBoton = false;
          this.desactivarBotonPago = true;

        }
        this.snack.open(`Pago realizado con éxito`, '', {
          duration: 4000,
        });
      }

    }, err => {
      console.log(err);
      this.loading = false;
      this.snack.open(`No se pudo actualizar el pedido: ${err.error.mensajeInformativo}`, '',
        {
          horizontalPosition: 'center',
          verticalPosition: 'top',
          duration: 10000,
        });
    });
  }

  mostrarDialogoPagos() {
    console.log("Mostrando el dialogo de pagos... ")
    this.dialog.open(ModalPagosComponent, {
      width: '500px',
      data: { title: 'Gestión de pagos', saldo: this.pedido.SaldoActual }
    }).afterClosed().subscribe(result => {
      if (result) {
        let pago: Pago = new Pago();

        pago.idUsuario = this.auth.user.userId;
        pago.saldoAnterior = this.pedido.SaldoActual;
        pago.pago = result.pago;
        pago.saldoActual = this.pedido.SaldoActual - result.pago;
        this.pedido.SaldoActual = pago.saldoActual;
        pago.referencia = result.referencia;
        pago.PedidoId = this.pedido.Id;

        this.createPagos(pago, false);
        this.desactivarBotonCancelacion = true;
        /*
        if (this.loading == false) {
          this.pedido.SaldoActual = this.pedido.SaldoActual - result.pago;
        }
        */

        //Esta sección de código se pasará al crear el pago en la fujnción "createPagos"
        /*
        if (this.pedido.SaldoActual == 0) {
          //Establecer el estatus "Por surtir"
          this.pedido.NombreStatus = StatusPedido[10];
          this.updateStatusPedido('');
          this.desactivarBoton = false;
          this.desactivarBotonPago = true;

        }
        */
      }
    });
  }

  rechazarDescuento(): void {
    this.dialog.open(ModalObservacionesComponent, {
      data: { title: 'Rechazar descuento' },
    }).afterClosed().subscribe(result => {
      if (result) {
        this.pedido.Comentarios = result;
        //this.pedido.StatusID = StatusPedido['Descuento rechazado'];
        this.pedido.NombreStatus = StatusPedido[5];
        this.pedido.AutorizaDesc = false;
        this.updateStatusPedido('');
      }
    });
  }

  /*
  Métodos para la tabla
  */
 
  getTotalImport() {
    return this.pedido.DetallesFront.map(t => t.Importe).reduce((acc, value) => acc + value, 0);
  }

  getTotalCantidad() {
    return this.pedido.DetallesFront.map(t => t.Cantidad).reduce((acc, value) => acc + value, 0);
  }

  getTotalDescMarca() {
    return this.pedido.DetallesFront.map(t => t.CantDescMarca).reduce((acc, value) => acc + value, 0);
  }
  
  getTotalDescProd() {
    return this.pedido.DetallesFront.map(t => t.DescuentoArticuloFront).reduce((acc, value) => acc + value, 0);
  }

}