import { Component,Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-pagos',
  templateUrl: './modal-pagos.component.html',
  styleUrls: ['./modal-pagos.component.scss']
})
export class ModalPagosComponent implements OnInit {

  observaciones = '';
  pago = 0;
  referencia = '';

  constructor(public dialogRef: MatDialogRef<ModalPagosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { title: string, saldo: number, pago: number, referencia: string }) { }

  ngOnInit(): void {
  }

  accept(): void {
    this.dialogRef.close(this.data);
  }

  close(): void {
    this.dialogRef.close(null);
  }

}
