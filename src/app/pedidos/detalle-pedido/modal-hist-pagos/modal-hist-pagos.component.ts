import { Component,Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { PedidoFront } from 'src/app/models/pedidos';
import { PagosService } from '../../../services/pagos.service';
import { Pago } from '../../../models/pagos';

@Component({
  selector: 'app-modal-hist-pagos',
  templateUrl: './modal-hist-pagos.component.html',
  styleUrls: ['./modal-hist-pagos.component.scss']
})
export class ModalHistPagosComponent implements OnInit {
  pagos: Pago[] = [];

  displayedColumns =
  ['numero', 'responsable', 'saldo_anterior','pago','saldo_actual', 'fecha_creacion', 'referencia',  'star'];

  constructor(
    public dialogRef: MatDialogRef<ModalHistPagosComponent>,
    private ps: PagosService,
    @Inject(MAT_DIALOG_DATA) public data: { pedido: PedidoFront}) {

      this.ps.get(this.data.pedido.Id).subscribe(d => {
        this.pagos = d;
      });

     }

  ngOnInit(): void {
  
  }

}
