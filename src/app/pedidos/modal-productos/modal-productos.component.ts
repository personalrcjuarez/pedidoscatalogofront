import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ArticleFull, NewProd, Producto } from 'src/app/models/articles';

@Component({
  selector: 'app-modal-productos',
  templateUrl: './modal-productos.component.html',
  styleUrls: ['./modal-productos.component.scss']
})
export class ModalProductosComponent implements OnInit {

  dataSource = new MatTableDataSource<ArticleFull>([]);
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  productos: Producto[];
  pageNumber = 1;
  campoOrden = "nombre";
  direccionOrden = "asc";
  loading = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['id', 'color', 'talla'];
  clickedRows = new Set(null);

  constructor(
    public dialogRef: MatDialogRef<ModalProductosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ArticleFull[] 
    ) 
    {
    this.dataSource = new MatTableDataSource<ArticleFull>(data);
    this.clickedRows = new Set<NewProd>();
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    
  }

  accept(): void {
    this.dialogRef.close(this.clickedRows);
  }

  close(): void {
    this.dialogRef.close(this.data);
  }
  pushRows(row: any) {
  
    if (this.clickedRows.has(row)) {
      this.clickedRows.delete(row);
    } else {
      this.clickedRows.add(row)
    }
  }

}