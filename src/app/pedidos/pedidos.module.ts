import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PedidosRoutingModule } from './pedidos-routing.module';
import { PedidosComponent } from './pedidos.component';
import {SharedModule} from '../shared/shared.module';
import { NuevoPedidoComponent } from './nuevo-pedido/nuevo-pedido.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DetallePedidoComponent } from './detalle-pedido/detalle-pedido.component';
import { ModalObservacionesComponent } from './modal-observaciones/modal-observaciones.component';
import { ModalPagosComponent } from './detalle-pedido/modal-pagos/modal-pagos.component';
import { ModalHistPagosComponent } from './detalle-pedido/modal-hist-pagos/modal-hist-pagos.component';
import { ModalProductosComponent } from './modal-productos/modal-productos.component';
import {MatGridListModule} from '@angular/material/grid-list';
@NgModule({
  declarations: [PedidosComponent, NuevoPedidoComponent, DetallePedidoComponent, ModalObservacionesComponent, ModalPagosComponent, ModalHistPagosComponent, ModalProductosComponent],
  imports: [
    CommonModule,
    PedidosRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatGridListModule
  ]
})
export class PedidosModule { }
