import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from './shared/shared.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AppLayoutComponent } from './app-layout.component';
import {FormsModule} from '@angular/forms';
import {AuthInterceptorService} from './services/auth-interceptor.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {MatGridListModule} from '@angular/material/grid-list';
import { EnvServiceProvider } from '../env.service.provider';
import { ReporteVentasComponent } from './home/reporte-ventas/reporte-ventas.component';
import { ReporteDevolucionesComponent } from './home/reporte-devoluciones/reporte-devoluciones.component';
import { ReportePorsurtirComponent } from './home/reporte-porsurtir/reporte-porsurtir.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    AppLayoutComponent,
    ReporteVentasComponent,
    ReporteDevolucionesComponent,
    ReportePorsurtirComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    FormsModule,
    MatGridListModule,
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptorService,
    multi: true,    
  },EnvServiceProvider],
  bootstrap: [AppComponent],
  exports: [HomeComponent]
})
export class AppModule {
}
