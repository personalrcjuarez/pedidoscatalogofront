import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { MovimientosService } from 'src/app/services/movimientos.service';
import { DetalleMovimiento, MovimientoAlmacen } from '../almacenes.component';

@Component({
  selector: 'app-detalle-movimiento',
  templateUrl: './detalle-movimiento.component.html',
  styleUrls: ['./detalle-movimiento.component.scss']
})
export class DetalleMovimientoComponent implements OnInit {

  loading = false;
  movimiento: MovimientoAlmacen;
  dataSource: MatTableDataSource<DetalleMovimiento>;
  errorMessage = '';
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['Id', 'ClaveProducto', 'NombreProducto', 'NombreColor', 'NombreTalla', 'Cantidad'];

  constructor(
    private route: ActivatedRoute,
    private _mov_serv: MovimientosService,
  ) { 
    this.dataSource = new MatTableDataSource<DetalleMovimiento>([]);
  }

  ngOnInit(): void {
    this.loading = true;

    this.route.params.subscribe(params => {
      if (params.id){
          this._mov_serv.getMov(params.id).subscribe(miMov => {            
          this.movimiento = miMov;
          console.log("Mi movimiento: ", this.movimiento);
          this.dataSource.data = this.movimiento.DetalleMovimiento;
          this.loading = false;
        }, err => {
          this.loading = false;
          this.errorMessage = `No se pudo cargar el pedido: ${err.error.mensajeInformativo}`;
        });
      }
    })
    this.loading = false;
    
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getTotalCantidad() {
    return this.movimiento?.DetalleMovimiento.map(t => t.Cantidad).reduce((acc, value) => acc + value, 0);
  }


}
