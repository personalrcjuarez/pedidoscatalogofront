import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Almacen } from 'src/app/models/almacenes';
import { ArticleFull, ArticuloFront, Marca } from 'src/app/models/articles';
import { APIResponse } from 'src/app/models/common';
import { DetallesFront } from 'src/app/models/pedidos';
import { ModalProductosComponent } from 'src/app/pedidos/modal-productos/modal-productos.component';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { MovimientosService } from 'src/app/services/movimientos.service';
import { ProductsService } from 'src/app/services/products.service';
import Swal from 'sweetalert2';
import { MovimientoAlmacen, DetalleMovimiento } from '../almacenes.component';

@Component({
  selector: 'app-crear-movimiento',
  templateUrl: './crear-movimiento.component.html',
  styleUrls: ['./crear-movimiento.component.scss']
})
export class CrearMovimientoComponent implements OnInit {

  options: FormGroup;
  floatLabelControl = new FormControl('auto');
  movimientoAlmacen = new MovimientoAlmacen();
  displayedColumns: string[] = ['clave', 'nombre', 'color', 'talla', 'cantidad', 'actions'];
  dataSource: MatTableDataSource<DetalleMovimiento>;
  productInput = new FormControl();
  filteredProducts: Observable<ArticuloFront[]> = of([]);
  errorMessage = '';
  datosAll: APIResponse = null;
  foundResults = false;
  marcas: Map<any, Marca> = new Map<any, Marca>();
  almacenes: Almacen[] = [];

  tipo_movimiento: string;
  operacion: string;
  almacen_origen: number;
  almacen_destino: number;
  comentarios: string

  loading = false;

  constructor(fb: FormBuilder,
    private p: ProductsService,
    private dialog: MatDialog,
    private snack: MatSnackBar,
    private router: Router,
    private cs: CatalogsService,
    private movimientoService: MovimientosService
  ) {
    this.options = fb.group({
      floatLabel: this.floatLabelControl,
    });
    this.dataSource = new MatTableDataSource<DetalleMovimiento>(this.movimientoAlmacen.DetalleMovimiento);
  }

  ngOnInit(): void {
    this.cs.almacens.subscribe(d => {
      this.almacenes = d;
    });
    this.tipo_movimiento = "Ajuste";
  }

  realizarMovimiento() {

    if (!this.validaciones()) {
      this.movimientoAlmacen.AlmacenDestinoId = this.almacen_destino;
      this.movimientoAlmacen.AlmacenOrigenId = this.almacen_origen;
      this.movimientoAlmacen.Comentarios = this.comentarios;
      this.movimientoAlmacen.TipoMovimiento = this.tipo_movimiento;
      this.movimientoAlmacen.TipoOperacion = this.operacion;
      this.llamarServicioCreacion();
    }
  }
  
  llamarServicioCreacion(){
    this.movimientoService.create(this.movimientoAlmacen).subscribe(d => {
      this.snack.open(`Movimiento creado con exito`, '', {
        duration: 4000,
      });
      this.loading = false;
      this.router.navigate(['/movimientos']);
    }, error => {
      console.log(error);
      this.snack.open(`El movimiento no se pudo crear: ${error.error.mensajeError}`, '', {
        duration: 4000,
      });
      this.loading = false;
    });
  }

  mensajeErrores(mensaje: string, titulo: string) {
    this.snack.open(mensaje, titulo,
      {
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
        duration: 5000,
      });
  }

  validaciones(): boolean {
    let error: boolean;
    switch (this.tipo_movimiento) {
      case undefined:
        this.mensajeErrores("Debe seleccionar el tipo de movimiento", "Error de validación");
        error = true;
        break;
      case "Ajuste":
        if (this.almacen_origen == undefined) {
          this.mensajeErrores("Debe seleccionar el almacén de donde se hará la operación", "Error de validación");
          error = true;
        }
        if (this.operacion == undefined) {
          this.mensajeErrores("Debe seleccionar el tipo de operación a realizar", "Error de validación");
          error = true;
        }
        break;
      case "Traspaso":
        if (this.almacen_origen == undefined) {
          this.mensajeErrores("Debe seleccionar el almacén de donde se hará la operación", "Error de validación");
          error = true;
        }
        if (this.almacen_destino == undefined) {
          this.mensajeErrores("Debe seleccionar el almacén de destino a donde se hará la operación", "Error de validación");
          error = true;
        }
        break;
    }

    if (this.movimientoAlmacen.DetalleMovimiento.length == 0) {
      this.mensajeErrores("Aun no tiene seleccionado los productos", "Error de validación");
      error = true;
    }

    return error;
  }

  /*
    Métodos para la tabla
    */


  getTotalCantidad() {
    return this.movimientoAlmacen.DetalleMovimiento.map(t => t.Cantidad).reduce((acc, value) => acc + value, 0);
  }

  removeProduct(r: DetalleMovimiento): void {

    Swal.fire({
      title: "¿Está seguro de quitar éste producto?",
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.isConfirmed) {
        const index = this.movimientoAlmacen.DetalleMovimiento.findIndex(d => d.ArticuloId === r.ArticuloId && d.TallaId == r.TallaId && d.ColorId == r.ColorId);
        if (index >= 0) {
          this.movimientoAlmacen.DetalleMovimiento.splice(index, 1);
          this.dataSource.data = this.movimientoAlmacen.DetalleMovimiento;
        }
      }
    })
  }

  addProductOptimized(): void {
    let prductOri: ArticuloFront = this.productInput.value;
    this.p.getByTallaColors(prductOri.Id, 0).subscribe(prod => {
      this.openDialog(prod.object);
      this.dataSource.data = this.movimientoAlmacen.DetalleMovimiento;
    });
  }

  openDialog(prod: ArticleFull[]): void {

    const dialogRef = this.dialog.open(ModalProductosComponent, {
      maxWidth: '100vw',
      height: '85vh',
      maxHeight: '85vh',
      closeOnNavigation: true,
      data: prod,
    });

    dialogRef.afterClosed().subscribe((result: Set<ArticleFull>) => {
      if (result.size > 0) {

        //vacir el mat autocomplete
        //y limpiar el input
        this.productInput.setValue("");
        this.filteredProducts = null;

        result.forEach((valBefPedido: ArticleFull) => {

          //if (!validateExistOrdersOptimized(valBefPedido, this.pedido.ProductosMovimientos)) {


          this.movimientoAlmacen.DetalleMovimiento.push({
            Id: 0,
            ArticuloId: valBefPedido.IdArticulo,
            ClaveProducto: valBefPedido.Clave,
            NombreProducto: valBefPedido.Nombre,
            ColorId: valBefPedido.IdColor,
            NombreColor: valBefPedido.Color,
            TallaId: valBefPedido.IdTalla,
            NombreTalla: valBefPedido.NombreTalla,
            Cantidad: 1,
            Precio: valBefPedido.Precio,
            IdMarca: valBefPedido.IdMarca,
            NombreMarca: valBefPedido.NombreMarca,
            IdCatalogoTallasColores: valBefPedido.IdCatalogoTallasColores
          });
          //}
        });
        this.dataSource.data = this.movimientoAlmacen.DetalleMovimiento;
      }
    });
  }

  searchCoincidences(input: any): void {
    if (input != "") {
      this.p.getCoincidencesProduct(input).pipe(
        map((productData: APIResponse) =>
          this.datosAll = productData,
        )
      ).subscribe(dato => {

        if (dato.object.length > 0) {
          this.foundResults = true;
          dato.object.forEach(element => {
            element.marca = this.marcas.get(element.marcaId);
          });

          this.filteredProducts = this.productInput.valueChanges
            .pipe(
              startWith(''),
              map(value => {
                return dato.object;
              })
            );
        } else {
          if (dato.codigo == 200) {
            this.snack.open(`No hay coincidencias para el artículo: [` + input + "]", 'Producto inexistente',
              {
                horizontalPosition: 'center',
                verticalPosition: 'bottom',
                duration: 5000,
              });
          } else {
            this.snack.open("[" + dato.mensajeError + "]", 'Error al consultar: [' + dato.codigo + ']',
              {
                horizontalPosition: 'center',
                verticalPosition: 'bottom',
                duration: 10000,
              });
          }
          this.foundResults = false;
        }

      }
      );
    }
  }

  displayProductFn(p: ArticuloFront): string {
    return p ? `${p.Clave} - ${p.NombreProducto}` : '';
  }

  openPanel(evt: any, trigger: MatAutocompleteTrigger, words: string): void {
    if (words != "") {
      this.searchCoincidences(words);
      evt.stopPropagation();
      trigger.openPanel();
    } else {
      this.foundResults = false;
    }
  }

}


