import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { APIResponse, DataPaginator } from 'src/app/models/common';
import { AuthService } from 'src/app/services/auth.service';
import { MovimientosService } from 'src/app/services/movimientos.service';
import { MovimientoAlmacen } from '../almacenes.component';

@Component({
  selector: 'app-listado-movimientos',
  templateUrl: './listado-movimientos.component.html',
  styleUrls: ['./listado-movimientos.component.scss']
})
export class ListadoMovimientosComponent implements OnInit {

  //VARIABLES
  displayedColumns: string[] = ['ID', 'CreatedAt', 'TipoMovimiento', 'TipoOperacion', 'AlmacenOrigen', 'AlmacenDestino', 'Usuario', 'Comentarios', 'Acciones'];
  dataSource: APIResponse = null;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  movimientos: MovimientoAlmacen[] = [];
  displayedData = [];
  sortedData: MovimientoAlmacen[];
  postPerPage = 10;
  pageNumber = 1;
  campoOrden = "updated_at";
  direccionOrden = "desc";
  pageEvent: PageEvent;
  loading = false;
  inputBusqueda: "";
  errorMessage = '';

  constructor(
    private movimientosService: MovimientosService,
    private router: Router,
    public auth: AuthService,
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.GetMovs(this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden, this.inputBusqueda);
  }

  GetMovs(limit: number, page: number, active: string, direction: string, wordsSearched: string) {
    this.movimientosService.getMovs(limit, page, active, direction, wordsSearched,false).pipe(
      map((productData: APIResponse) =>
        this.dataSource = productData,
      )
    ).subscribe(dato => {
      this.sortedData = dato.DataMeta.items.slice();
      this.movimientos = dato.DataMeta.items;
      this.loading = false;
    }
    );
  }

  applyFilter(): void {
    this.pageNumber = 0;
    if (this.inputBusqueda != "") {
      this.GetMovs(this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden, this.inputBusqueda);
    }
  }

  resetQuery(): void {
    this.inputBusqueda = "";
    this.GetMovs(this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden, this.inputBusqueda);
  }

  sortData(sort: Sort) {

    const data = this.movimientos.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.campoOrden = sort.active;
    this.direccionOrden = sort.direction;

    this.GetMovs(this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden, this.inputBusqueda);
  }

  onPaginate(pageEvent: PageEvent) {

    this.postPerPage = +pageEvent.pageSize;
    this.pageNumber = +(pageEvent.pageIndex + 1);
    this.GetMovs(this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden, this.inputBusqueda);
  }

  showDetails(id: number): void {
    this.router.navigate(['/almacenes', id]);
  }

}
