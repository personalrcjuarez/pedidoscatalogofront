import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort, Sort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Almacen } from 'src/app/models/almacenes';
import { APIResponse, DataPaginator } from 'src/app/models/common';
import { AuthService } from 'src/app/services/auth.service';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { MovimientosService } from 'src/app/services/movimientos.service';
import { Inventario } from '../almacenes.component';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.scss']
})
export class InventarioComponent implements OnInit {

  displayedColumns: string[] = ['ID', 'CreatedAt', 'NombreAlmacen', 'Clave', 'Producto', 'NombreColor', 'NombreTalla', 'Existencia'];
  dataSource: APIResponse = null;
  @ViewChild(MatSort) sort: MatSort;
  inventario: Inventario[] = [];
  displayedData = [];
  sortedData: Inventario[];
  postPerPage = 5;
  pageNumber = 1;
  campoOrden = "CreatedAt";
  direccionOrden = "desc";
  pageEvent: PageEvent;
  loading = false;
  errorMessage = '';
  inputBusqueda: "";
  almacenes: Almacen[] = [];
  id_almacen: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private movimientosService: MovimientosService,
    public auth: AuthService,    
    private cs: CatalogsService,
    private snack: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.cs.almacens.subscribe(d => {
      this.almacenes = d;
    });

    //this.loading = true;
    //this.GetIventory(this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden, this.inputBusqueda, this.id_almacen);
  }

  GetIventory(limit: number, page: number, active: string, direction: string, wordsSearched: string, idAlmacen: number) {
    this.movimientosService.getInventory(limit, page, active, direction, wordsSearched, idAlmacen ).pipe(
      map((productData: APIResponse) =>
        this.dataSource = productData,
      )
    ).subscribe(dato => {
      console.log("Mi dato: ", dato);
      this.sortedData = dato.DataMeta.items.slice();
      this.inventario = dato.DataMeta.items;
      this.loading = false;
    }
    );
  }

  ngAfterViewInit(): void {
    /*
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    */    
  }

  applyFilter(): void {
    
    if (this.id_almacen == 0 || this.id_almacen == undefined) {
      this.snack.open('Seleccion el almacén', 'Error de almacén',
        {
          horizontalPosition: 'center',
          verticalPosition: 'bottom',
          duration: 5000,
        });
    }else{
      this.loading = true;
      this.pageNumber = 0;
      this.GetIventory(this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden, this.inputBusqueda, this.id_almacen);      
      this.loading = false;
    }
    
  }
  
  resetQuery(): void {
    this.inputBusqueda = "";
    //this.GetIventory(this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden, this.inputBusqueda, this.id_almacen);
    this.dataSource = null;
  }

  sortData(sort: Sort) {

    const data = this.inventario.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.campoOrden = sort.active;
    this.direccionOrden = sort.direction;

    this.GetIventory(this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden, this.inputBusqueda, this.id_almacen);
  }  

  onPaginate(pageEvent: PageEvent) {

    this.postPerPage = +pageEvent.pageSize;
    this.pageNumber = +(pageEvent.pageIndex + 1);
    this.GetIventory(this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden, this.inputBusqueda, this.id_almacen);
  }

}
