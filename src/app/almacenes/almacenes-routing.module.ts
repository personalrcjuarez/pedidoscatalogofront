import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AlmacenesComponent } from './almacenes.component';
import { DetalleMovimientoComponent } from './detalle-movimiento/detalle-movimiento.component';

const routes: Routes = [
  {
    path: '', component: AlmacenesComponent, data: {
      title: 'Almacenes',
      breadcrumb: 'Movimientos',
    },
  },
  {
    path: 'traspaso', component: AlmacenesComponent, data: {
      title: 'Traspaso',
      breadcrumb: 'Realizar traspaso',
    },
  },
  {
    path: ":id", component: DetalleMovimientoComponent, data : {
        title: 'Detalle movimiento',
        breadcrumb : 'Detalle movimiento',
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlmacenesRoutingModule {
}
