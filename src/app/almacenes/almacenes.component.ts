import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-almacenes',
  templateUrl: './almacenes.component.html',
  styleUrls: ['./almacenes.component.scss']
})
export class AlmacenesComponent implements OnInit {

  
  constructor(public auth: AuthService) {

  }

  ngOnInit(): void {
      
  }
}

export class MovimientoAlmacen {
  Id: number;
  CreatedAt: Date
  TipoMovimiento: string
  AlmacenOrigenId: number
  AlmacenDestinoId: number
  TipoOperacion: string
  Comentarios: string
  DetalleMovimiento: DetalleMovimiento[] = [];  

  //Datos no necesarios para el back
  AlmacenOrigen:  string
	AlmacenDestino: string
	Usuario:        string
}

export class DetalleMovimiento {
  Id: number;
  ArticuloId: number;
  ClaveProducto: string;
  NombreProducto: string;
  ColorId: number;
  NombreColor: string;
  TallaId: number;
  NombreTalla: string;
  Cantidad: number;
  Precio: number;
  IdMarca: number;
  NombreMarca: string;

  IdCatalogoTallasColores: number;
}

export class Inventario {
  Id: number;
  CreatedAt: Date;
  Clave: string;
  Producto: string;
  NombreColor: string;
  NombreTalla: string;
  Existencia: string;
}