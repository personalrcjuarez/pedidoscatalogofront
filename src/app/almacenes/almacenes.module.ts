
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import { AlmacenesComponent } from './almacenes.component';
import {SharedModule} from '../shared/shared.module';
import { AlmacenesRoutingModule } from './almacenes-routing.module';
import { CrearMovimientoComponent } from './crear-movimiento/crear-movimiento.component';
import { ListadoMovimientosComponent } from './listado-movimientos/listado-movimientos.component';
import { DetalleMovimientoComponent } from './detalle-movimiento/detalle-movimiento.component';
import { InventarioComponent } from './inventario/inventario.component';

@NgModule({
  declarations: [AlmacenesComponent, CrearMovimientoComponent, ListadoMovimientosComponent, DetalleMovimientoComponent, InventarioComponent],
  imports: [
    CommonModule,
    AlmacenesRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatGridListModule
  ]
})
export class AlmacenesModule { }
