import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  password = '';
  usuario = '';
  loading = false;

  constructor(private router: Router, private auth: AuthService, private snack: MatSnackBar) {
    if (auth.isLogged()) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
  }

  login(): void {
    if (!this.usuario || !this.usuario.length || !this.password || !this.password.length) {
      return;
    }
    this.loading = true;
    this.auth.login(this.usuario, this.password).subscribe(d => {
      this.loading = false;
      this.router.navigate([`/`]);
    }, err => {
      this.loading = false;
      this.snack.open(`No se pudo iniciar sesión, ${err.error?.mensajeInformativo || ''}`, '', {
        duration: 5000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
      });
    });
  }

}
