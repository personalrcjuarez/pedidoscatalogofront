import {Component, OnInit} from '@angular/core';
import {of, Observable} from 'rxjs';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {map, shareReplay} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  title = 'Pedidos por catalogo';
  showText = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private router: Router, private route: ActivatedRoute,
              private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              public auth: AuthService,
  ) {
    this.matIconRegistry.addSvgIcon(
      'logo',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icon.svg')
    );
  }

  ngOnInit(): void {
    //Mostrar con texto
    this.breakpointObserver.observe([
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge
    ]).subscribe(result => {
      if (result.matches){
        this.isHandset$ = of(!result.matches);
        this.showText = result.matches;
      }
    });

    //Mostrar solo iconos
    this.breakpointObserver.observe([
      Breakpoints.Small,
      Breakpoints.Medium
    ]).subscribe(result => {
      if (result.matches){
        this.isHandset$ = of(!result.matches);
        this.showText = !result.matches;
      }
    });

    //No mostrar nada
   this.breakpointObserver.observe([
      Breakpoints.XSmall
    ]).subscribe(result => {
      if (result.matches){
        this.isHandset$ = of(result.matches);
        this.showText = !result.matches;    
      }
    });

  }

  cargaMasivaProductos(): void {
    this.router.navigate(['/import_productos']);
  }

  logout(): void {
    this.auth.logout().then(() => {
      this.router.navigate(['/login']);
    });
  }

  /*
  log(e: any): void {
    console.log(e);
  }
  */
}
