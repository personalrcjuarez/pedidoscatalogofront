import {Component, Inject, OnInit} from '@angular/core';
import {Catalog, CatalogValue} from '../../models/catalogs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

import {CatalogsService} from '../../services/catalogs.service';
import {Observable} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-catalog-form',
  templateUrl: './catalog-form.component.html',
})
export class CatalogFormComponent implements OnInit {

  v: CatalogValue;
  title = '';
  allowAbreviatura = true;
  saving = false;

  constructor(private cs: CatalogsService,
              private snack: MatSnackBar,
              public dialogRef: MatDialogRef<CatalogFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { catalog: Catalog, value: CatalogValue, useExternalCatalog }) {
    this.title = this.data.value.value ? `Editar ${data.catalog.displayName}` : `Crear ${data.catalog.displayName}`;
    this.v = this.data.value;
    this.allowAbreviatura = data.catalog.name === 'unidades';
  }

  ngOnInit(): void {
  }

  save(): void {
    this.saving = true;
    let req: Observable<CatalogValue>;
    if (this.v.value) {
      req = this.cs.updateCatalogValue(this.v, this.data.catalog.name, this.data.useExternalCatalog ? this.data.catalog : null);
    } else {
      req = this.cs.createCatalogValue(this.v, this.data.catalog.name, this.data.useExternalCatalog ? this.data.catalog : null);
    }
    req.subscribe(d => {
      this.dialogRef.close(d);
      this.saving = false;
    }, err => {
      this.saving = false;
      this.snack.open('No se pudo guardar la información', 'X', {
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      this.dialogRef.close();
    });
  }

}
