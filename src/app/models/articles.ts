export class Producto {
  idArticulo?: number;
  clave?: string;
  codigoBarras?: string;
  claveAlterna?: string;
  nombre?: string;
  descripcion?: string;
  //talla?: string;
  //colorId?: number;
  //color?: Color;
  
  //coloresProductos: ColorProducto[];
  //tallasProductos: TallaProducto[];
  TallasColoresProducto:  CatalogoTallasColores[];

  unidadId?: number;
  unidad?: Unidad;
  lineaId?: number;
  linea?: Linea;
  marcaId?: number;
  marca?: Marca;
  //modelo?: string;
  porcentajeDescuento?: number;
  enOferta?: boolean;
  precioBajoCosto?: boolean;
  imagen?: string;
  bloqueado?: boolean;
  precio1?: number;
  //precio2?: number;
  //precio3?: number;
  costoReal?: number;
  costoUltimo?: number;
  costoAdicional?: number;
  //existencia?: number;
  observaciones?: string;
  impuestoId: number;
  kit: boolean;
  kits: KitComponent[];

  createAt?: Date;
  updateAt?: Date;

  static FromJson(json: any): Producto {
    const p = new Producto();
    p.idArticulo = Number(json.idArticulo);
    p.clave = json.clave;
    p.codigoBarras = json.codigoBarras;
    p.claveAlterna = json.claveAlterna;
    p.nombre = json.nombre;
    p.descripcion = json.descripcion;
    //p.talla = json.talla;
    
    //p.coloresProductos = json.coloresProductos;
    //p.tallasProductos = json.tallasProductos;

    //p.colorId = Number(json.colorId);
    //p.color = json.color;
    p.unidadId = Number(json.unidadId);
    p.unidad = json.unidad;
    p.lineaId = Number(json.lineaId);
    p.linea = json.linea;
    p.marcaId = Number(json.marcaId);
    p.marca = json.marca;
    //p.modelo = json.modelo;
    p.porcentajeDescuento = Number(json.porcentajeDescuento);
    p.enOferta = json.enOferta;
    p.precioBajoCosto = json.precioBajoCosto;
    p.imagen = json.imagen;
    p.bloqueado = json.bloqueado;
    p.precio1 = Number(json.precio1);
    //p.precio2 = Number(json.precio2);
    //p.precio3 = Number(json.precio3);
    p.costoReal = Number(json.costoReal);
    p.costoUltimo = Number(json.costoUltimo);
    p.costoAdicional = Number(json.costoAdicional);
    //p.existencia = Number(json.existencia);
    p.observaciones = json.observaciones;
    p.impuestoId = json.impuestoId;
    p.kit = json.kit;
    p.kits = json.kits;
    p.createAt = json.createAt;
    p.updateAt = json.updateAt;
    return p;
  }
}

export interface CatalogoTallasColores {
  IdArticulo: number;
  IdTalla: number;
  IdColor: number;

  ClaveProducto: string;
  NombreColor: string;
  NombreTalla: string;
}

export interface NewProd {
  producto: Producto,
  idColor: number,
  idTalla: number,
  valorColor: string,
  valorTalla: string,
}

export interface ArticleFull {
  IdCatalogoTallasColores: number;
  ID: number,
  IdArticulo: number,
	PorcentajeDescuentoMarca: number,
	Mes: string,
	IdPersona: number,
	IdColor: number,
	IdTalla: number,
	Color: string,
	NombreTalla: string,
	Bloqueado: boolean,
  Precio: number,
  NombreMarca: string,
  Clave: string,
  Nombre: string,
  EnOferta: boolean,
  Costo: number,
  IdMarca: number,
  PorcentajeDescuentoArticulo: number,
}

export class ArticuloFront {
	Id: number;
	Clave: string;
	CodigoBarras: string;
	NombreProducto: string;
	NombreMarca: string;
	Bloqueado: boolean;
	Existencia: number;
}

export class KitComponent {
  articuloId: number;
  cantidad: number;
  componenteId: number;
  articulo?: Producto;
}

export interface Color {
  idColor?: number;
  color?: string;
  selected?: boolean;
}

export interface Talla {
  idTalla?: number;
  nombreTalla?: string;
  selected?: boolean;
}

export interface Linea {
  idLinea?: number;
  nombreLinea?: string;
}

export class Marca {
  IDMarca?: number;
  NombreMarca?: string;
  Archivos?: Archivos[];

  static FromJson(json: any): Marca {
    const m = new Marca();
    m.IDMarca = json.IDMarca;
    m.NombreMarca = json.NombreMarca;
    m.Archivos = json.Archivos;
    return m;
  }
}

export interface Archivos {
  Archivo: File;
  NombrePdf: string;
  TamanioPdf: number;
}

export interface Unidad {
  idUnidad?: number;
  nombreUnidad?: string;
  abreviatura?: string;
}

/*
export interface ColorProducto {
  idArticulo?: number;
  idColor?: number;
  color?: Color;
  selected?: boolean;
}

export interface TallaProducto {
  idArticulo?: number;
  idTalla?: number;
  talla?: Talla;
  selected?: boolean;
}
*/




export interface Impuesto {
  abreviatura: string;
  id: number;
  nombre: string;
  porcentaje: number;
}

export interface Moneda {
  clave: string;
  id: number;
  nombre: string;
  signo: string;
  tipoCambio: number;
}
