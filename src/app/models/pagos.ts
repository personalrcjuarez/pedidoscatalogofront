import { PedidoFront } from './pedidos';
import { PersonModel } from './personModel';

export class Pago {
    CreatedAt: Date;
    UpdateAt: Date;
    ID: number;
    pago: number;
    referencia: string;
    saldoActual: number;
    saldoAnterior: number;    
    idUsuario: number;
    usuario: PersonModel;
    PedidoId: number;
    Pedido: PedidoFront;

    static FromJson(json: any): Pago {
        const p = new Pago();

        p.saldoAnterior = json.saldoAnterior;
        p.pago = json.pago;
        p.saldoActual = json.saldoActual;
        p.referencia = json.referencia;
        p.PedidoId = json.PedidoId;
        return p;
      }
}