export interface APIResponse {
  codigo: number;
  mensajeError: string;
  mensajeInformativo: string;
  object: any;
  totalRegistros: number;
  DataMeta: DataPaginator;
}

export interface DataPaginator {
  items: any[],
  totalItems: number,
  itemsPerPage: number,
}