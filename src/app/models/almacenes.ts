export class Almacen {
    comentarios: string;
    direccion: string;
    id: number;
    nombreAlmacen: string;
  
    static FromJson(json: any): Almacen {
      const p = new Almacen();
      p.direccion = json.direccion;
      p.nombreAlmacen = json.nombreAlmacen;
      p.comentarios = json.comentarios;
      p.id = json.id;
      return p;
    }
  }

  
export enum StatusArticuloInventario {
    "inexistente",//0
    "insuficiente",//1
}
