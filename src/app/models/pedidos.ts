import { PersonModel } from './personModel';

export enum StatusPedido {
    ' ',//0
    'Pendiente por autorizar',//1
    'Autorizado',//2
    'Revisado',//3
    'Cerrado',//4
    'Descuento rechazado',//5
    'Cancelado',//6
    'Por revisar',//7
    'Abierto',//8
    'Enviado',//9
    'Por surtir',//10
    'Surtido',//11
    'Por recibir',//12
    'Recibido'//13
}

export enum StatusDevolucion {
    'Solicitado',//0
    'Autorizado',//1
    'Rechazado',//2
}

export interface QueryReport {
    SearchProduct: boolean,
    ProductName: string,
    Filters: filters[],
}

export interface filters {
    campo: string,//created_at, nombre_status, clave_moneda, tipo_cambio, cliente_id
    operador: string,//=, >, <, <>
    valor: any,//any
}

export class PedidoFront {
    Id: number;
    NombreCliente: string;
    NombreVendedor: string;
    NombreAlmacen: string;
    ClaveMoneda: string;
    TotalGeneral: number;
    CreatedAt: Date;
    UpdatedAt: Date;
    NombreStatus: string;
    SaldoActual: number;
    EstatusDevolucion: string;
    ClienteId: number;
    VendedorId: number;
    AlmacenId: number;
    DetallesFront: DetallesFront[] = [];
    Comentarios: string;
    AutorizaDesc: Boolean;
    TipoCambio: number;
    DescuentoCliente: number;
    PorcDescAdic: number;
    CantDescAdic: number;
    Anticipo: number;
    SaldoFavor: number;
    TotalImporte: number;
    TotalDescuento: number;
    cliente: PersonModel;
    PorcDescCliente: number;
    MontoDevolucion: number;
    EditarPedido: string;
}

export class DetallesFront {
    Id: number;
    IdCatalogoTallasColores: number;
    ArticuloId: number;
    NombreMarca: string;
    PorcDescMarca: number;
    PorcDescArticulo: number;
    ClaveProducto: string;
    NombreProducto: string;
    IdColor: number;
    NombreColor: string;
    IdTalla: number;
    NombreTalla: string;
    Cantidad: number;
    Precio: number;
    Subtotal: number;
    Impuesto: number;
    CantDescMarca: number;
    DescuentoArticulo: number;
    DescuentoArticuloFront: number;
    Importe: number;
    PrecioMoneda: number;
    CostoUltimo: Number;
    IdMarca: number;
    ImporteBruto: number;
    EnOferta: boolean;
    DescUnitMarca: number;
    EstatusInventario: string;//Inexistente, Insuficiente
}