export interface CatalogValue {
  value: string | number;
  text: string;
  abreviatura?: string;
  selected?: boolean;
}

export interface Catalog {
  name: string;
  displayName: string;
  values: CatalogValue[];
  mapValues?: Map<any, CatalogValue>;
  systemCatalog?: boolean;

  fromCatalog(c: CatalogValue): any;

  toCatalog(a: any): CatalogValue;
}


export interface Catalogs {
  statusPedido: Catalog;
  //marcas: Catalog;
  lineas: Catalog;
  colores: Catalog;
  tallas: Catalog;
  unidades: Catalog;
}

