import { PersonModel } from "./personModel";


export class SendDevoluciones {
    devolucions: Devolucion[];
}

export class Devolucion {
    id: number;
    pedidoId: number;
    articuloId: number;
    claveArticulo: string;
    nombreArticulo: string;
    cantidadDevuelta: number;
    cantidadOriginal: number;
    cantidadInicialPedido: number;
    precio: number;
    subtotal: number;
    impuesto: number
    idColor: number;
    nombreColor: string;
    idTalla: number;
    nombreTalla: string;
    nombreMarca: string;
    descuentoMarca: number;
    descUnitMarca: number;
    descuentoArticulo: number;
    descuentoArticuloFront: number;
    importe: number;
    comentarios: string;
}

export class ReporteDevoluciones {
    ID: number;
    UpdatedAt: Date;
    NombreStatus: string;
    ClaveMoneda: string;
    ClienteId: number;
    cliente: PersonModel;
    vendedor: string;
    EstatusDevolucion: string;
    MontoDevolucion: number;
    NombreArticulo: string;
    ClaveArticulo: string;
    CantidadDevuelta: number;
    NombreColor: string;
    NombreTalla: string;
    NombreMarca: string;
    Importe: number;
    Precio: number;
}

export class ReporteProductosSolicitados {
    NombreAlmacen: string;
    Clave: string;
    Nombre: string;
    Color: string;
    NombreTalla: string;
    NombreUnidad: string;
    NombreMarca: string;
    TotalCantidad: number;
}