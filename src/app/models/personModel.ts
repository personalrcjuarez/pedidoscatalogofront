
export class PersonModel {
  id = 0;
  agrup = 0;
  apellido1: string = null;
  apellido2: string = null;
  bloqueado = false;
  calle: string = null;
  clasif: number = null;
  codigoPostal: string = null;
  colonia: string = null;
  comentarios: string = null;
  descuento = 0;
  diasCredito = 0;
  email: string = null;
  estado: string = null;
  limiteCredito = 0;
  listaPrecios = 0;
  municipio: string = null;
  nombres: string = null;
  numExt: string = null;
  numInt: string = null;
  pais: string = null;
  password: string = null;
  poblacion: string = null;
  saldo = 0;
  saldoFavor = 0;
  telefono: string = null;
  tipo: number = null;
  usuario: string = null;
  createAt: Date;
  updateAt: Date;
  descuentoMarcas: descuentoMarca[];

  constructor() {
    this.bloqueado = false;
    this.saldo = 0;
    this.descuento = 0;
    this.diasCredito = 0;
    this.limiteCredito = 0;
  }

}

export class descuentoMarca {
  ID: number;
  idMarca: number;
  porcentajeDescuento: number;
  mes: string;
  nombreMarca: string;
  idPersona: number;
}