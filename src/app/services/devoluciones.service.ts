import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Devolucion, SendDevoluciones} from '../models/devolucion';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {APIResponse} from '../models/common';
//import {APIURL_X} from '../consts';
import { EnvService } from '../../env.service';

@Injectable({
    providedIn: 'root'
  })
export class DevolucionesService {
  private baseURL = this.env.APIURL;
    private d: Devolucion[] = [];
    private observableDevoluciones: BehaviorSubject<Devolucion[]> = new BehaviorSubject<Devolucion[]>([]);

    constructor(private http:HttpClient, private env: EnvService) {}

    create(d: SendDevoluciones): Observable<SendDevoluciones> {
        const rd: SendDevoluciones = JSON.parse(JSON.stringify(d));
        return this.http.post(`${this.baseURL}/devoluciones`, rd).pipe(
            map((d: APIResponse) => {
                const np = d.object;
                this.d.push(np);
                this.observableDevoluciones.next(this.d);
                return np;
            })
        );
    }

    get(id: number): Observable<Devolucion[]> {
        return this.http.get(`${this.baseURL}/devoluciones/${id}`).pipe(
          map((d: APIResponse) => {
            return d.object;
          }),
        );
      }

      update(id: number, devolucion: SendDevoluciones): Observable<SendDevoluciones> {
        return this.http.put(`${this.baseURL}/devoluciones/${id}`, devolucion).pipe(
          map((d: APIResponse) => {
            return d?.object || devolucion;
          }),
        );
      }
    
}