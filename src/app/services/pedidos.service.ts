import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { filters, PedidoFront, QueryReport} from '../models/pedidos';
import {Pago} from '../models/pagos';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {APIResponse, DataPaginator} from '../models/common';
import { ReporteDevoluciones } from '../models/devolucion';
import { EnvService } from '../../env.service';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {
  private baseURL = this.env.APIURL;

  // pedidos
  private p: PedidoFront[] = [];
  private observablePedidos: BehaviorSubject<PedidoFront[]> = new BehaviorSubject<PedidoFront[]>([]);

  constructor(private http: HttpClient, private env: EnvService) {
  }

  create(p: PedidoFront): Observable<PedidoFront> {
    const rp: PedidoFront = JSON.parse(JSON.stringify(p));
   
    return this.http.post(`${this.baseURL}/pedidos`, rp).pipe(
      map((d: APIResponse) => {
        const np = d.object;
        this.p.push(np);
        this.observablePedidos.next(this.p);
        return np;
      })
    );
  }

  load(): Observable<PedidoFront[]> {
    return this.http.get(`${this.baseURL}/pedidos`).pipe(
      map((d: APIResponse) => {
        const p = d.object;
        this.p = p;
        this.observablePedidos.next(this.p);
        return p;
      }),
    );
  }

  get(id: any): Observable<PedidoFront> {
    return this.http.get(`${this.baseURL}/pedidos/${id}`).pipe(
      map((d: APIResponse) => {
        return d.object;
      }),
    );
  }

  getOptimized(id: any): Observable<PedidoFront> {
    return this.http.get(`${this.baseURL}/pedidos/${id}`).pipe(
      map((d: APIResponse) => {
        return d.object;        
      }),
    );
  }

  getPedidosCoincidences(limit: number, page : number, active : string, direction: string, wordsSearched: string, byProduct: boolean): Observable<DataPaginator>{

    let params = new HttpParams()
    .append('limit', limit.toString())
    .append('page', page.toString())
    .append('active', active.toString())
    .append('direction', direction.toString())
    .append('wordsSearched', wordsSearched)
    .append('byProduct', byProduct.toString())

      return this.http.get(`${this.baseURL}/pedidosSearch`, {params}).pipe(
        map((productData: DataPaginator) => productData),
        catchError(err => throwError(err))
      )
  }

  update(pedido: PedidoFront): Observable<PedidoFront> {
    return this.http.put(`${this.baseURL}/pedidos/${pedido.Id}`, pedido).pipe(
      map((d: APIResponse) => {
        return d?.object || pedido;
      }),
    );
  }

  updateStatusOptimized(pedido: PedidoFront, accion: string): Observable<PedidoFront> {   
    let myUrl = "" ;

    switch (accion) {
      case "Surtido":
        myUrl = `${this.baseURL}/pedidos/status/surtir/${pedido.Id}`
        break;    
      default:
        myUrl = `${this.baseURL}/pedidos/status/${pedido.Id}`
        break;
    }

    return this.http.put(myUrl, pedido).pipe(
      map((d: APIResponse) => {
        return d?.object;
      }),
    );
  }

  callSurtirPedido(pedido: PedidoFront): Observable<APIResponse> {   
    let myUrl = `${this.baseURL}/pedidos/status/surtir/${pedido.Id}`;

    return this.http.put(myUrl, pedido).pipe(
      map((d: APIResponse) => {
        return d;
      }),
    );
  }

  updateStatusOrderEdit(pedido: PedidoFront): Observable<PedidoFront> {    
    return this.http.put(`${this.baseURL}/pedidos/statusEdit/${pedido.Id}`, pedido).pipe(
      map((d: APIResponse) => {
        return d?.object || pedido;
      }),
    );
  }

  updateStatusDevolucion(pedido: PedidoFront): Observable<PedidoFront> {
    return this.http.put(`${this.baseURL}/pedidos/statusDevs/${pedido.Id}`, pedido).pipe(
      map((d: APIResponse) => {
        return d?.object || pedido;
      }),
    );
  }

  getPedidosReporte(filtros: QueryReport): Observable<PedidoFront[]> {
    return this.http.post(`${this.baseURL}/pedidos/reportes`, filtros).pipe(
      map((d: APIResponse) => {
        return d?.object;
      }),
    );
  }

  getDevolucionesReporte(filtros: QueryReport): Observable<ReporteDevoluciones[]> {
    return this.http.post(`${this.baseURL}/devoluciones/reportes`, filtros).pipe(
      map((d: APIResponse) => {
        return d?.object;
      }),
    );
  }

  getProductosSolicitados(filtros: filters[], limit: number, page : number, active : string, direction: string): Observable<APIResponse> {

    return this.http.post(`${this.baseURL}/pedidos/reportes/solicitados?limit=${limit}&page=${page}&active=${active}&direction=${direction}`, filtros).pipe(
      map((d: APIResponse) => {
        return d;
      }),
    );
  }  

  createPagos(pago: Pago): Observable<Pago> {
    return this.http.post(`${this.baseURL}/pagos`, pago).pipe(
      map((d: APIResponse) => {
        return d?.object || pago;
      }),
    );
  }

  get pedidos(): Observable<PedidoFront[]> {
    return this.observablePedidos.asObservable();
  }
}
