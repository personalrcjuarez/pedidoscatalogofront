import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { APIResponse } from '../models/common';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { Pago } from '../models/pagos';
//import { APIURL_X } from '../consts';
import { EnvService } from '../../env.service';

@Injectable({
    providedIn: 'root'
})
export class PagosService {

    private baseURL = this.env.APIURL;
    //private pgs: Pago[] = [];
    //private pagos: BehaviorSubject<Pago[]> = new BehaviorSubject<Pago[]>([]);

    constructor(private http: HttpClient, private env: EnvService) { }

    /*
    loadPagos(idPedido: Number): Observable<Pago[]> {
        return this.http.get(`${this.baseURL}/pagos/${idPedido}`)
            .pipe(map((d: APIResponse) => {
                this.pgs = d.object ? d.object : [];
                this.pagos.next(this.pgs);
                return this.pgs;
            }));
    }
    */
    get(idPedido: any): Observable<Pago[]> {
        return this.http.get(`${this.baseURL}/pagos/${idPedido}`)
          .pipe(map((d: APIResponse) => {
            return d.object;
          }));
      }
}