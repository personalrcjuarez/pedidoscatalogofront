import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Producto} from '../models/articles';
import {catchError, map} from 'rxjs/operators';
import {APIResponse, DataPaginator} from '../models/common';
//import {APIURL_x} from '../consts';
import { EnvService } from '../../env.service';

export interface ProductData {
  items: Producto[],
  meta: {
    totalItems: number,
    itemsPerPage: number,
  }
}

@Injectable({
  providedIn: 'root'
})

export class ProductsService {
  private ps: Producto[] = [];
  private productos: BehaviorSubject<Producto[]> = new BehaviorSubject<Producto[]>([]);
  private baseURL = this.env.APIURL;

  /*
    // monedas
    private artServ: ArticuloFront[] = [];
    private articulos: BehaviorSubject<ArticuloFront[]> = new BehaviorSubject<ArticuloFront[]>([]);
  */

  constructor(private http: HttpClient, private env: EnvService) {
  }

  get products(): Observable<Producto[]> {
    return this.productos.asObservable();
  }

  /*
  loadProducts(): Observable<Producto[]> {
    return this.http.get(`${this.baseURL}/articulos`)
      .pipe(map((d: APIResponse) => {
        this.ps = d.object ? d.object.map(a => Producto.FromJson(a)) : [];
        this.productos.next(this.ps);
        return this.ps;
      }));
  }
  */

  getProductsPaginator(limit: number, page : number, active : string, direction: string, stringSearched = ""): Observable<APIResponse>{
    let params = new HttpParams()
    .append('limit', limit.toString())
    .append('page', page.toString())
    .append('active', active.toString())
    .append('direction', direction.toString())
    .append('stringSearched', stringSearched);

    return this.http.get(`${this.baseURL}/articulosPaginator`, {params}).pipe(
      map((apiResponse: APIResponse) => apiResponse),
      catchError(err => throwError(err))
    )
  }

  getCoincidencesProduct(stringSearched = ""): Observable<APIResponse>{
    let params = new HttpParams().append('stringSearched', stringSearched.toString());

    return this.http.get(`${this.baseURL}/articulosPaginatorOptimized`, {params}).pipe(
      map((productData: APIResponse) => productData),
      catchError(err => throwError(err))
    )
  }

  get(id: any): Observable<Producto> {
    return this.http.get(`${this.baseURL}/articulos/${id}`)
      .pipe(map((d: APIResponse) => {
        return d.object;
      }));
  }

  getByTallaColors(idProduct: number, idPerson: number): Observable<APIResponse>{
      return this.http.get(`${this.baseURL}/articulos/${idProduct}/${idPerson}`).pipe(
        map((productData: APIResponse) => productData),
        catchError(err => throwError(err))
      )
    }
  
  createProduct(p: Producto): Observable<Producto> {
    return this.http.post(`${this.baseURL}/articulos`, p)
      .pipe(map((d: APIResponse) => {
        const np = Producto.FromJson(d.object);
        this.ps.push(np);
        this.productos.next(this.ps);
        return np;
      }));
  }

  importarProductos(prods: any): Observable<APIResponse> {
    return this.http.post(`${this.baseURL}/articulos/importar`, prods)
      .pipe(map((d: APIResponse) => {
        return d;
      }));
  }

  updateProduct(p: Producto, id: number): Observable<Producto> {
    return this.http.put(`${this.baseURL}/articulos/${id}`, p)
      .pipe(map((d: APIResponse) => {
        const np = Producto.FromJson(d.object);
        const idx = this.ps.findIndex(a => a.idArticulo === id);
        if (idx >= 0) {
          this.ps[idx] = np;
        } else {
          this.ps.push(np);
        }
        this.productos.next(this.ps);
        return np;
      }));
  }
}
