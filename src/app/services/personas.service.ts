import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CatalogValue} from '../models/catalogs';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {PersonModel} from '../models/personModel';
import {APIResponse} from '../models/common';
//import {APIURL_X} from '../consts';
import { EnvService } from '../../env.service';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  private tiposPersona: CatalogValue[] = [];
  private clasificacion: CatalogValue[] = [];
  private agrupacion: CatalogValue[] = [];

  private ps: PersonModel[] = [];
  private persons: BehaviorSubject<PersonModel[]> = new BehaviorSubject<PersonModel[]>([]);
  private baseURL = this.env.APIURL;

  constructor(private http: HttpClient, private env: EnvService) {
  }

  getTiposPersona(): Observable<CatalogValue[]> {
    if (this.tiposPersona.length) {
      return of(this.tiposPersona);
    }
    return this.http.get(`${this.baseURL}/tipo-usuarios`).pipe(
      map((v: APIResponse) => {
        let tipos: CatalogValue[] = [];
        if (v.object && v.object.length) {
          tipos = v.object.map((x: any) => {
            return {
              text: x.tipoUsuario || x.TipoUsuario,
              value: x.idTipoUsuario || x.IDTipoUsuario,
            };
          });
        }
        this.tiposPersona = tipos;
        return tipos;
      })
    );
  }

  getClasificacion(): Observable<CatalogValue[]> {
    if (this.clasificacion.length) {
      return of(this.clasificacion);
    }
    return this.http.get(`${this.baseURL}/clasificacion_personas`).pipe(
      map((v: APIResponse) => {
        let clasificacion = [];
        if (v.object && v.object.length) {
          clasificacion = v.object.map((x: any) => {
            return {text: x.nombre_clasificacion, value: x.id_clasif};
          });
        }
        this.clasificacion = clasificacion;
        return clasificacion;
      })
    );
  }

  getAgrupacion(): Observable<CatalogValue[]> {
    if (this.agrupacion.length) {
      return of(this.agrupacion);
    }
    return this.http.get(`${this.baseURL}/agrupacion_personas`).pipe(
      map((v: APIResponse) => {
        let agrupacion = [];
        if (v.object && v.object.length) {
          agrupacion = v.object.map((x: any) => {
            return {text: x.agrupacion, value: x.id_agrupacion};
          });
        }
        this.agrupacion = agrupacion;
        return agrupacion;
      })
    );
  }

  createPersona(p: PersonModel): Observable<PersonModel> {    
    return this.http.post(`${this.baseURL}/personas`, p).pipe(
      map((d: APIResponse) => {
          this.ps.push(d.object);
          this.persons.next(this.ps);
          return d.object;
        }
      ),
    );
  }

  getPersons(): Observable<PersonModel[]> {
    return this.http.get(`${this.baseURL}/personas`).pipe(
      map((data: any) => {
          this.ps = data.object || [];
          this.persons.next(this.ps);
          return this.ps;
        },
      ),
    );
  }

  getPerson(id: any): Observable<PersonModel> {
    return this.http.get(`${this.baseURL}/personas/${id}`)
      .pipe(map((d: APIResponse) => {
        return d.object;
      }));
  }

  updatePerson(p: PersonModel): Observable<PersonModel> {
    return this.http.put(`${this.baseURL}/personas/${p.id}`, p).pipe(
      map((d: APIResponse) => {
        const idx = this.ps.findIndex(x => x.id === d.object.id);
        this.ps[idx] = d.object;
        this.persons.next(this.ps);
        return d.object;
      }));
  }

  updateClientBalance(idClient: number, balance: number): Observable<PersonModel> {
    return this.http.put(`${this.baseURL}/personas/balance/${idClient}`, balance).pipe(
      map((d: APIResponse) => {
        const idx = this.ps.findIndex(x => x.id === d.object.id);
        this.ps[idx] = d.object;
        this.persons.next(this.ps);
        return d.object;
      }));
  }  

  get personas(): Observable<PersonModel[]> {
    return this.persons.asObservable();
  }
}
