import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { APIResponse, DataPaginator } from '../models/common';
import { EnvService } from '../../env.service';
import { MovimientoAlmacen } from '../almacenes/almacenes.component';

@Injectable({
  providedIn: 'root'
})
export class MovimientosService {

  private p: MovimientoAlmacen[] = [];
  private observableMovimientos: BehaviorSubject<MovimientoAlmacen[]> = new BehaviorSubject<MovimientoAlmacen[]>([]);

  private baseURL = this.env.APIURL;

  constructor(private http: HttpClient, private env: EnvService) {
  }

  create(p: MovimientoAlmacen): Observable<MovimientoAlmacen> {
    const rp: MovimientoAlmacen = JSON.parse(JSON.stringify(p));

    return this.http.post(`${this.baseURL}/movimientos`, rp).pipe(
      map((d: APIResponse) => {
        const np = d.object;
        this.p.push(np);
        this.observableMovimientos.next(this.p);
        return np;
      })
    );
  }

  getMovs(limit: number, page: number, active: string, direction: string, wordsSearched: string, byProduct: boolean): Observable<APIResponse> {

    let params = new HttpParams()
      .append('limit', limit.toString())
      .append('page', page.toString())
      .append('active', active.toString())
      .append('direction', direction.toString())
      .append('wordsSearched', wordsSearched)
      .append('byProduct', byProduct.toString())

    return this.http.get(`${this.baseURL}/movimientos`, { params }).pipe(
      map((productData: APIResponse) => productData),
      catchError(err => throwError(err))
    )
  }

  getMov(id: any): Observable<MovimientoAlmacen> {
    return this.http.get(`${this.baseURL}/movimientos/${id}`).pipe(
      map((d: APIResponse) => {
        return d.object;
      }),
    );
  }

  getInventory(limit: number, page: number, active: string, direction: string, wordsSearched: string, idAlm: number): Observable<APIResponse> {

    let params = new HttpParams()
      .append('limit', limit.toString())
      .append('page', page.toString())
      .append('active', active.toString())
      .append('direction', direction.toString())
      .append('wordsSearched', wordsSearched);

    return this.http.get(`${this.baseURL}/movimientos/inventario/`+idAlm, { params }).pipe(
      map((productData: APIResponse) => productData),
      catchError(err => throwError(err))
    )
  }
}