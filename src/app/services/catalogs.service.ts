import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { APIResponse } from '../models/common';
import { Catalog, Catalogs, CatalogValue } from '../models/catalogs';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { Impuesto, Marca, Moneda } from '../models/articles';
//import { APIURL_x } from '../consts';
import { EnvService } from '../../env.service';
import { Almacen } from '../models/almacenes';

@Injectable({
  providedIn: 'root'
})
export class CatalogsService {

  constructor(private http: HttpClient, private env: EnvService) {
    //this.loadCatalogs();
  }

  private baseURL = this.env.APIURL;

  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;

  // marcas
  private mrs: Marca[] = [];
  private marcs: BehaviorSubject<Marca[]> = new BehaviorSubject<Marca[]>([]);
  mapaMarcas: Map<any, Marca> = new Map<any, Marca>();

  // almacenes
  private as: Almacen[] = [];
  private almacenes: BehaviorSubject<Almacen[]> = new BehaviorSubject<Almacen[]>([]);

  // impuestos
  private is: Impuesto[] = [];
  private imps: BehaviorSubject<Impuesto[]> = new BehaviorSubject<Impuesto[]>([]);

  // monedas
  private ms: Moneda[] = [];
  private mons: BehaviorSubject<Moneda[]> = new BehaviorSubject<Moneda[]>([]);

  private cs: Catalogs = {
    statusPedido: {
      name: 'status-pedidos',
      displayName: 'status-pedidos',
      values: [],
      fromCatalog(c: CatalogValue): any {
        return { id: c.value, nombreStatus: c.text };
      },
      toCatalog(a: any): CatalogValue {
        return { text: a.nombreStatus, value: a.id };
      },
      systemCatalog: true,
    },
    /*
    marcas: {
      name: 'marcas',
      displayName: 'marcas',
      values: [],
      fromCatalog(c: CatalogValue): any {
        return {
          idMarca: c.value,
          nombreMarca: c.text,
        };
      },
      toCatalog(a: any): CatalogValue {
        return {
          text: a.nombreMarca,
          value: a.idMarca,
        };
      },
    },
    */
    lineas: {
      name: 'lineas',
      displayName: 'lineas',
      values: [],
      fromCatalog(c: CatalogValue): any {
        return {
          idLinea: c.value,
          nombreLinea: c.text,
        };
      },
      toCatalog(a: any): CatalogValue {
        return {
          text: a.nombreLinea,
          value: a.idLinea,
        };
      }
    },
    colores: {
      name: 'colores',
      displayName: 'colores',
      values: [],
      fromCatalog(c: CatalogValue): any {
        return {
          idColor: c.value,
          color: c.text,
        };
      },
      toCatalog(a: any): CatalogValue {
        return {
          text: a.color,
          value: a.idColor,
        };
      }
    },
    tallas: {
      name: 'tallas',
      displayName: 'tallas',
      values: [],
      fromCatalog(c: CatalogValue): any {
        return {
          idTalla: c.value,
          nombreTalla: c.text,
        };
      },
      toCatalog(a: any): CatalogValue {
        return {
          text: a.nombreTalla,
          value: a.idTalla,
        };
      },
    },
    unidades: {
      name: 'unidades',
      displayName: 'unidades',
      values: [],
      fromCatalog(c: CatalogValue): any {
        return {
          idUnidad: c.value,
          nombreUnidad: c.text,
          abreviatura: c.abreviatura,
        };
      },
      toCatalog(a: any): CatalogValue {
        return {
          text: a.nombreUnidad,
          value: a.idUnidad,
          abreviatura: a.abreviatura,
        };
      }
    },
  };

  private catalogs: BehaviorSubject<Catalogs> = new BehaviorSubject<Catalogs>(null);

  loadCatalogs(): void {
    merge(
      /* this.loadCatalog(this.cs.marcas), */
      this.loadCatalog(this.cs.colores),
      this.loadCatalog(this.cs.tallas),
      this.loadCatalog(this.cs.lineas),
      this.loadCatalog(this.cs.unidades),
      //this.loadCatalog(this.cs.statusPedido),
    )
      .subscribe((d) => {
      });
  }

  loadCatalog(c: Catalog): Observable<CatalogValue[]> {
    return this.http.get(`${this.baseURL}/${c.name}`).pipe(
      map((d: APIResponse | Array<any>) => {
        let values = [];
        if (d instanceof Array) {
          values = d.map(c.toCatalog);
        } else {
          values = d.object.map(c.toCatalog);
        }
        c.values = values;
        c.mapValues = new Map<any, CatalogValue>();
        values.forEach((v: CatalogValue) => {
          c.mapValues.set(v.value, v);
        });
        this.catalogs.next(this.cs);
        return values;
      })
    );
  }

  get availableCatalogs(): Observable<Catalogs> {
    return this.catalogs.asObservable();
  }

  get almacensLoaded(): Observable<Almacen[]> {
    return this.almacens;
  }

  createCatalogValue(v: CatalogValue, catalogName: string, catalog?: Catalog): Observable<CatalogValue> {
    v.value = null;
    const externalCatalog = !catalog;
    if (!catalog) {
      catalog = this.cs[catalogName];
    }
    return this.http.post(`${this.baseURL}/${catalog.name}`, catalog.fromCatalog(v))
      .pipe(map((d: APIResponse) => {
        const nv: CatalogValue = catalog.toCatalog(d.object);
        catalog.values.push(nv);
        catalog.mapValues.set(nv.value, nv);
        this.catalogs.next(this.cs);
        return d.object;
      }));
  }

  updateCatalogValue(v: CatalogValue, catalogName: string, catalog?: Catalog): Observable<CatalogValue> {
    const externalCatalog = !catalog;
    if (!catalog) {
      catalog = this.cs[catalogName];
    }
    return this.http.put(`${this.baseURL}/${catalog.name}/${v.value}`, catalog.fromCatalog(v))
      .pipe(map((d: APIResponse) => {
        const idx = catalog.values.findIndex(aux => aux.value === v.value);
        const nv = catalog.toCatalog(d.object);
        catalog.values[idx] = nv;
        catalog.mapValues.set(nv.value, nv);
        if (!externalCatalog) {
          this.catalogs.next(this.cs);
        }
        return d.object;
      }));
  }

  deleteCatalogValue(v: CatalogValue, catalogName: string, catalog?: Catalog): Observable<void> {
    const externalCatalog = !catalog;
    if (!catalog) {
      catalog = this.cs[catalogName];
    }
    return this.http.delete(`${this.baseURL}/${catalog.name}/${v.value}`)
      .pipe(map((d: APIResponse) => {
        const idx = catalog.values.findIndex(aux => aux.value === v.value);
        catalog.values.splice(idx, 1);
        if (!externalCatalog) {
          this.catalogs.next(this.cs);
        }
        catalog.mapValues.delete(v.value);
        return;
      }));
  }

  //Marcas

  loadMarcas(): Observable<Marca[]> {
    return this.http.get(`${this.baseURL}/marcas`)
      .pipe(map((d: APIResponse) => {
        this.mrs = d.object ? d.object : [];
        this.marcs.next(this.mrs);

        this.mrs.forEach(t => {
          this.mapaMarcas.set(t.IDMarca, t);
        });

        return this.mrs;
      }));
  }

  get getMarcasLoaded(): Observable<Marca[]> {
    return this.marcs.asObservable();
  }

  createMarca(p: Marca): Observable<Marca> {

    const formData: FormData = new FormData();

    for (let index = 0; index < p.Archivos.length; index++) {
      const element = p.Archivos[index];
      formData.append("files", element.Archivo);
    }

    formData.set("nombre", p.NombreMarca);

    return this.http.post(`${this.baseURL}/marcas`, formData)
      .pipe(map((d: APIResponse) => {
        const np = d.object;
        this.mrs.push(np);
        this.marcs.next(this.mrs);
        return np;
      }));
  }

  updateMarca(p: Marca, id: number): Observable<Marca> {
    const formData: FormData = new FormData();

    for (let index = 0; index < p.Archivos.length; index++) {
      const element = p.Archivos[index];
      formData.append("files", element.Archivo);
    }

    formData.set("nombre", p.NombreMarca);
    formData.set("idMarca", String(id));

    return this.http.put(`${this.baseURL}/marcas/${id}`, formData)
      .pipe(map((d: APIResponse) => {
        const np = d.object;
        const idx = this.mrs.findIndex(a => a.IDMarca === id);
        if (idx >= 0) {
          this.is[idx] = np;
        } else {
          this.mrs.push(np);
        }
        this.marcs.next(this.mrs);
        return np;
      }));
  }

  deleteMarca(p: Marca, id: number): Observable<void> {
    return this.http.delete(`${this.baseURL}/marcas/${id}`)
      .pipe(map((d: APIResponse) => {
        const idx = this.as.findIndex(a => a.id === id);
        if (idx >= 0) {
          this.mrs.splice(idx, 1);
        }
        this.marcs.next(this.mrs);
        return;
      }));
  }

  deleteFile(id: string, nameBrand: string): Observable<void> {
    return this.http.delete(`${this.baseURL}/file/${id}/${nameBrand}`)
      .pipe(map((d: APIResponse) => {
        return;
      }));
  }
  //Almacenes

  loadAlmacenes(): Observable<Almacen[]> {
    return this.http.get(`${this.baseURL}/almacenes`)
      .pipe(map((d: APIResponse) => {
        this.as = d.object ? d.object.map(a => Almacen.FromJson(a)) : [];
        this.almacenes.next(this.as);
        return this.as;
      }));
  }

  createAlmacen(p: Almacen): Observable<Almacen> {
    return this.http.post(`${this.baseURL}/almacenes`, p)
      .pipe(map((d: APIResponse) => {
        const np = Almacen.FromJson(d.object);
        this.as.push(np);
        this.almacenes.next(this.as);
        return np;
      }));
  }

  updateAlmacen(p: Almacen, id: number): Observable<Almacen> {
    return this.http.put(`${this.baseURL}/almacenes/${id}`, p)
      .pipe(map((d: APIResponse) => {
        const np = Almacen.FromJson(d.object);
        const idx = this.as.findIndex(a => a.id === id);
        if (idx >= 0) {
          this.as[idx] = np;
        } else {
          this.as.push(np);
        }
        this.almacenes.next(this.as);
        return np;
      }));
  }

  deleteAlmacen(p: Almacen, id: number): Observable<void> {
    return this.http.delete(`${this.baseURL}/almacenes/${id}`)
      .pipe(map((d: APIResponse) => {
        const idx = this.as.findIndex(a => a.id === id);
        if (idx >= 0) {
          this.as.splice(idx, 1);
        }
        this.almacenes.next(this.as);
        return;
      }));
  }

  get almacens(): Observable<Almacen[]> {
    return this.almacenes.asObservable();
  }

  // Impuestos
  loadImpuestos(): Observable<Impuesto[]> {
    return this.http.get(`${this.baseURL}/impuestos`)
      .pipe(map((d: APIResponse) => {
        this.is = d.object ? d.object : [];
        this.imps.next(this.is);
        return this.is;
      }));
  }

  createImpuesto(p: Impuesto): Observable<Impuesto> {
    return this.http.post(`${this.baseURL}/impuestos`, p)
      .pipe(map((d: APIResponse) => {
        const np = d.object;
        this.is.push(np);
        this.imps.next(this.is);
        return np;
      }));
  }

  updateImpuesto(p: Impuesto, id: number): Observable<Impuesto> {
    return this.http.put(`${this.baseURL}/impuestos/${id}`, p)
      .pipe(map((d: APIResponse) => {
        const np = d.object;
        const idx = this.is.findIndex(a => a.id === id);
        if (idx >= 0) {
          this.is[idx] = np;
        } else {
          this.is.push(np);
        }
        this.imps.next(this.is);
        return np;
      }));
  }

  deleteImpuesto(p: Impuesto, id: number): Observable<void> {
    return this.http.delete(`${this.baseURL}/impuestos/${id}`)
      .pipe(map((d: APIResponse) => {
        const idx = this.is.findIndex(a => a.id === id);
        if (idx >= 0) {
          this.is.splice(idx, 1);
        }
        this.imps.next(this.is);
        return;
      }));
  }

  get impuestos(): Observable<Impuesto[]> {
    return this.imps.asObservable();
  }

  get marcas(): Observable<Marca[]> {
    return this.marcs.asObservable();
  }

  // Moneds
  loadMonedas(): Observable<Moneda[]> {
    return this.http.get(`${this.baseURL}/monedas`)
      .pipe(map((d: APIResponse) => {
        this.ms = d.object ? d.object : [];
        this.mons.next(this.ms);
        return this.ms;
      }));
  }

  updateMoneda(p: Moneda, id: number): Observable<Moneda> {
    return this.http.put(`${this.baseURL}/monedas/${id}`, p)
      .pipe(map((d: APIResponse) => {
        const np = d.object;
        const idx = this.ms.findIndex(a => a.id === id);
        if (idx >= 0) {
          this.ms[idx] = np;
        } else {
          this.ms.push(np);
        }
        this.mons.next(this.ms);
        return np;
      }));
  }

  get monedas(): Observable<Moneda[]> {
    return this.mons.asObservable();
  }
}
