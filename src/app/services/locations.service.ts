import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {map} from 'rxjs/operators';

import {CatalogValue} from '../models/catalogs';

interface ResponseModel {
  error: boolean;
  code_error: number;
  error_message?: string;
  response: any;
}


export interface LocationByCP {
  municipios: CatalogValue[];
  colonias: CatalogValue[];
  estados: CatalogValue[];
  paises: CatalogValue[];
}

// service to load geo information using this API only works with Mexico https://api-sepomex.hckdrk.mx/documentacion/docs
@Injectable({
  providedIn: 'root'
})
export class LocationsService {
  private baseURL = 'https://api-sepomex.hckdrk.mx/query';
  private estados: CatalogValue[] = [];
  private locations: Map<string, LocationByCP> = new Map<string, LocationByCP>();

//  {metodo}/{busqueda}?={variable}
  constructor(private http: HttpClient) {
  }

  getEstados(): Observable<CatalogValue[]> {
    if (this.estados && this.estados.length) {
      return of(this.estados);
    }
    return this.http.get(`${this.baseURL}/get_estados`)
      .pipe(
        map((value: ResponseModel) => {
          if (value.error) {
            return [];
          }
          if (!value.response || !value.response.estado) {
            return [];
          }
          this.estados = value.response.estado.map((v: string) => {
            return {
              text: v,
              value: v,
            };
          });
          return this.estados;
        })
      );
  }


  getInfoByCP(cp: string): Observable<LocationByCP> {
    if (this.locations.has(cp)) {
      return of(this.locations.get(cp));
    }
    return this.http.get(`${this.baseURL}/info_cp/${cp}`)
      .pipe(
        map((value: ResponseModel[] | ResponseModel) => {
          if (!(value instanceof Array)) {
            throwError(value.error_message);
            return null;
          }
          if (!value || !value.length) {
            return null;
          }
          if (!value[0].response || value[0].error) {
            return null;
          }
          const colonias: Set<string> = new Set();
          const estados: Set<string> = new Set();
          const municipios: Set<string> = new Set();
          const paises: Set<string> = new Set();

          value.forEach(v => {
            if (v.response) {
              colonias.add(v.response.asentamiento);
              estados.add(v.response.estado);
              municipios.add(v.response.municipio);
              paises.add(v.response.pais);
            }
          });
          this.locations.set(cp, {
              colonias: Array.from(colonias.values()).map(v => ({text: v, value: v})),
              estados: Array.from(estados.values()).map(v => ({text: v, value: v})),
              municipios: Array.from(municipios.values()).map(v => ({text: v, value: v})),
              paises: Array.from(paises.values()).map(v => ({text: v, value: v})),
            }
          );
          return this.locations.get(cp);
        })
      );
  }
}
