import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
//import {APIURL_X} from '../consts';
import { EnvService } from '../../env.service';
import {map} from 'rxjs/operators';
import {APIResponse} from '../models/common';
import jwtDecode from 'jwt-decode';

export interface AuthToken {
  admin: boolean;
  bloqueado: boolean;
  exp: number;
  name: string;
  tipo: string;
  token: string;
  userId: number;
  userTypeId: number;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: AuthToken;

  constructor(private http: HttpClient, private env: EnvService){    
    const token = localStorage.getItem('token');
    if (token && token.length) {
      const decoded: AuthToken = jwtDecode(token);
      this.user = decoded;
    }
  }

  login(username: string, password: string): Observable<AuthToken> {
    return this.http.post(`${this.env.APIURL}/login`, {username, password}).pipe(
      map((d: APIResponse) => {
        const token = d.object.token;
        localStorage.setItem('token', token);
        const decoded: AuthToken = jwtDecode(token);
        this.user = decoded;
        return decoded;
      }));
  }

  isLogged(): boolean {
    if (!this.user) {
      return false;
    }
    const d = Math.round(new Date().getTime() / 1000);
    return d < this.user.exp;
  }

  logout(): Promise<any> {
    localStorage.removeItem('token');
    this.user = null;
    return Promise.resolve();
  }

  isFilial(): boolean {
    if (!this.user) {
      return false;
    }
    return this.user.userTypeId === 2;
  }

  isAdmin(): boolean {
    if (!this.user) {
      return false;
    }
    return this.user.admin;
  }

  isCustomer(): boolean {
    if (!this.user) {
      return false;
    }
    return this.user.userTypeId === 3;
  }
}
