import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { descuentoMarca, PersonModel } from '../../../models/personModel';
import { LocationByCP, LocationsService } from '../../../services/locations.service';
import { Observable, Subscription } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { PersonasService } from '../../../services/personas.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CatalogValue } from '../../../models/catalogs';
import { AuthService } from '../../../services/auth.service';
import { Marca } from 'src/app/models/articles';
import { CatalogsService } from 'src/app/services/catalogs.service';

@Component({
  selector: 'app-persona-form',
  templateUrl: './persona-form.component.html',
})
export class PersonaFormComponent implements OnInit, OnDestroy {
  favoriteSeason: string;
  formPerson: FormGroup;
  person: PersonModel = new PersonModel();
  locations: LocationByCP = {
    colonias: [],
    estados: [],
    municipios: [],
    paises: [],
  };
  saving = false;
  editable: boolean;
  isNew: boolean;
  porcentajeDescuento: number;

  tiposPersona: CatalogValue[] = [];
  clasificaciones: CatalogValue[] = [];
  agrupaciones: CatalogValue[] = [];
  subscriptions: Subscription[] = [];

  descuentoMarcas: descuentoMarca[] = [];
  //descuentosGral: descuentoMarca[] = [];
  personas: PersonModel[] = [];
  marcas: Marca[] = [];
  mapPersonas: Map<number, PersonModel> = new Map<number, PersonModel>();
  //meses : string[] = ['Enero', 'Febrero','Marzo','Abril']

  constructor(
    public ls: LocationsService,
    public ps: PersonasService,
    public cs: CatalogsService,
    private snack: MatSnackBar,
    public dialogRef: MatDialogRef<PersonaFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { person: PersonModel, marcas: Marca[] },
    public auth: AuthService,
  ) {

    this.person = this.data?.person ? { ...this.data.person } : new PersonModel();
    this.marcas = { ...this.data.marcas };

    this.isNew = !data.person;
    this.editable = !this.person.id;

    if (this.isNew) {
      //Recorrer todas las marcas  y hacer el match con los descuentos ya existentes de los clientes
      for (var element1 in this.marcas) {
        let descs = {} as descuentoMarca;
        descs.idMarca = this.marcas[element1].IDMarca;
        descs.nombreMarca = this.marcas[element1].NombreMarca;
        descs.mes = "No asignado";
        descs.porcentajeDescuento = 0.00;
        this.descuentoMarcas.push(descs);
      }

      this.formPerson = new FormGroup({
        nombres: new FormControl(this.person.nombres, [Validators.required]),
        apellido1: new FormControl(this.person.apellido1, [Validators.required]),
        apellido2: new FormControl(this.person.apellido2),
        tipo: new FormControl(this.person.tipo, [Validators.required]),
        usuario: new FormControl(this.person.usuario, [Validators.required]),
        password: new FormControl(this.person.password, [Validators.required]),
        telefono: new FormControl(this.person.telefono, [Validators.required]),
        email: new FormControl(this.person.email, [Validators.required, Validators.email]),
        calle: new FormControl(this.person.calle, [Validators.required]),
        numExt: new FormControl(this.person.numExt, [Validators.required]),
        numInt: new FormControl(this.person.numInt),
        colonia: new FormControl(this.person.colonia, [Validators.required]),
        poblacion: new FormControl(this.person.poblacion, [Validators.required]),
        municipio: new FormControl(this.person.municipio, [Validators.required]),
        estado: new FormControl(this.person.estado, [Validators.required]),
        pais: new FormControl(this.person.pais, [Validators.required]),
        //codigoPostal: new FormControl(this.person.codigoPostal, Validators.required, [this.postalCodeValidator()]),
        codigoPostal: new FormControl(this.person.codigoPostal),
        descuento: new FormControl(this.person.descuento, [Validators.required, Validators.min(0), Validators.max(100)]),
        limiteCredito: new FormControl(this.person.limiteCredito, [Validators.required]),
        diasCredito: new FormControl(this.person.diasCredito, [Validators.required, Validators.min(0)]),
        saldo: new FormControl(this.person.saldo, [Validators.required, Validators.min(0)]),
        saldoFavor: new FormControl(this.person.saldoFavor, [Validators.required, Validators.min(0)]),
        clasif: new FormControl(this.person.clasif, [Validators.required]),
        agrup: new FormControl(this.person.agrup, [Validators.required]),
        listaPrecios: new FormControl(this.person.listaPrecios, [Validators.required]),
        comentarios: new FormControl(this.person.comentarios),
        Schedule: new FormControl(),
      });

      if (this.auth.isAdmin() && auth.user.userId !== this.person.id) {
        this.formPerson.addControl('bloqueado', new FormControl(this.person.bloqueado));
      }
      this.toggleDisabledInputs(false);
    } else {
      this.ps.getPerson(this.person.id).subscribe(newP => {

        //Recorrer todas las marcas  y hacer el match con los descuentos ya existentes de los clientes
        for (var element1 in this.marcas) {

          let descs = {} as descuentoMarca;
          var encontrado = false;
          if (newP == null) {
            encontrado = true;
            descs.idMarca = this.marcas[element1].IDMarca;
            descs.nombreMarca = this.marcas[element1].NombreMarca;
            descs.mes = "No asignado";
            descs.porcentajeDescuento = 0.00;
            this.descuentoMarcas.push(descs);

          } else {
            for (let index = 0; index < newP?.descuentoMarcas.length; index++) {
              const element2 = newP.descuentoMarcas[index];
              if (this.marcas[element1].IDMarca == element2.idMarca) {
                encontrado = true;
                descs.ID = element2.ID;
                descs.idMarca = this.marcas[element1].IDMarca;
                descs.nombreMarca = this.marcas[element1].NombreMarca;
                descs.mes = element2.mes;
                descs.porcentajeDescuento = element2.porcentajeDescuento;
                descs.idPersona = element2.idPersona;
                this.descuentoMarcas.push(descs);
              }
            }
            if (encontrado == false) {
              descs.ID = 0;
              descs.idMarca = this.marcas[element1].IDMarca;
              descs.nombreMarca = this.marcas[element1].NombreMarca;
              descs.idPersona = newP.id;
              descs.mes = "No asignado";
              descs.porcentajeDescuento = 0.00;
              this.descuentoMarcas.push(descs);
            }
          }
        }

        this.formPerson = new FormGroup({
          nombres: new FormControl(this.person.nombres, [Validators.required]),
          apellido1: new FormControl(this.person.apellido1, [Validators.required]),
          apellido2: new FormControl(this.person.apellido2),
          tipo: new FormControl(this.person.tipo, [Validators.required]),
          usuario: new FormControl(this.person.usuario, [Validators.required]),
          password: new FormControl(this.person.password, [Validators.required]),
          telefono: new FormControl(this.person.telefono, [Validators.required]),
          email: new FormControl(this.person.email, [Validators.required, Validators.email]),
          calle: new FormControl(this.person.calle, [Validators.required]),
          numExt: new FormControl(this.person.numExt, [Validators.required]),
          numInt: new FormControl(this.person.numInt),
          colonia: new FormControl(this.person.colonia, [Validators.required]),
          poblacion: new FormControl(this.person.poblacion, [Validators.required]),
          municipio: new FormControl(this.person.municipio, [Validators.required]),
          estado: new FormControl(this.person.estado, [Validators.required]),
          pais: new FormControl(this.person.pais, [Validators.required]),
          //codigoPostal: new FormControl(this.person.codigoPostal, Validators.required, [this.postalCodeValidator()]),
          codigoPostal: new FormControl(this.person.codigoPostal),
          descuento: new FormControl(this.person.descuento, [Validators.required, Validators.min(0), Validators.max(100)]),
          limiteCredito: new FormControl(this.person.limiteCredito, [Validators.required]),
          diasCredito: new FormControl(this.person.diasCredito, [Validators.required, Validators.min(0)]),
          saldo: new FormControl(this.person.saldo, [Validators.required, Validators.min(0)]),
          saldoFavor: new FormControl(this.person.saldoFavor, [Validators.required, Validators.min(0)]),
          clasif: new FormControl(this.person.clasif, [Validators.required]),
          agrup: new FormControl(this.person.agrup, [Validators.required]),
          listaPrecios: new FormControl(this.person.listaPrecios, [Validators.required]),
          comentarios: new FormControl(this.person.comentarios),
          Schedule: new FormControl(),
        });

        if (!this.isNew) {
          this.formPerson.disable();
        }

        if (this.auth.isAdmin() && auth.user.userId !== this.person.id) {
          this.formPerson.addControl('bloqueado', new FormControl(this.person.bloqueado));
        }

        const inputSubs = this.formPerson.get('codigoPostal').statusChanges.subscribe(d => {
          this.toggleDisabledInputs(d === 'VALID');
        });
        this.subscriptions.push(inputSubs);
      })
    }



    this.ps.getTiposPersona().subscribe(d => {
      this.tiposPersona = d;
      //this.formPerson.get('listaPrecios').setValue(1);
      //this.formPerson.get('listaPrecios').disable();
      if (this.auth.isFilial() && this.isNew) {
        // 3 es el tipo de persona para el cliente
        const tipoCliente = this.tiposPersona.filter(v => v.value === 3);
        this.formPerson.get('tipo').setValue(tipoCliente && tipoCliente.length ? tipoCliente[0].value : null);
        this.formPerson.get('tipo').disable();
      }
    });
    this.ps.getClasificacion().subscribe(d => {
      this.clasificaciones = d;
      if (this.isNew) {
        this.formPerson.get('clasif').setValue(this.clasificaciones[0]?.value);
      }
    });
    this.ps.getAgrupacion().subscribe(d => {
      this.agrupaciones = d;
      if (this.isNew) {
        this.formPerson.get('agrup').setValue(this.agrupaciones[0]?.value);
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => {
      if (!s.closed) {
        s.unsubscribe();
      }
    });
  }


  valueInput(elemnto, otro): void {
  }

  ngOnInit(): void {

  }

  savePerson(): void {
    const p: PersonModel = this.formPerson.getRawValue();
    this.saving = true;
    let sf: Observable<PersonModel>;
    p.descuentoMarcas = this.descuentoMarcas;
    if (this.isNew) {
      sf = this.ps.createPersona(p);
    } else {
      p.id = this.person.id;
      sf = this.ps.updatePerson(p);
    }

    sf.subscribe(d => {
      this.saving = false;
      this.snack.open('Información guardada correctamente', '', {
        horizontalPosition: 'right',
        verticalPosition: 'top',
        duration: 2000,
      });
      this.dialogRef.close(d);
    },
      error => {
        this.saving = false;
        this.snack.open(`No se pudo guardar datos de la persona: ${error.error.mensajeInformativo}`, 'X', {
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      });
  }

  postalCodeValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this.loadLocations(control.value).pipe(
        map(res => {
          // if res is true, cp exists, return true
          return res ? null : { invalidCP: true };
          // NB: Return null if there is no error
        }),
        catchError(err => {
          if (this.formPerson) {
            this.formPerson.get('pais').setValue(null);
            this.formPerson.get('estado').setValue(null);
            this.formPerson.get('municipio').setValue(null);
          }
          return [{ invalidCP: true }];
        })
      );
    };
  }

  loadLocations(cp: string): Observable<LocationByCP> {
    return this.ls.getInfoByCP(cp).pipe(map(d => {
      if (d) {
        this.locations = d;
        this.formPerson.get('pais').setValue(d.paises[0].value);
        this.formPerson.get('estado').setValue(d.estados[0].value);
        this.formPerson.get('municipio').setValue(d.municipios[0].value);
      } else {
        this.locations = {
          colonias: [],
          estados: [],
          municipios: [],
          paises: [],
        };
      }
      return d;
    }
    ));
  }

  toggleEdit(): void {
    this.editable = !this.editable;
    if (this.editable) {
      this.formPerson.enable();
    } else {
      // tslint:disable-next-line:forin
      for (const k in this.person) {
        this.formPerson.get(k)?.setValue(this.person[k]);
      }
      this.formPerson.disable();
    }
  }

  toggleDisabledInputs(activate: boolean): void {
    /*
    if (activate) {
      //this.formPerson.get('pais').enable();
      this.formPerson.get('estado').enable();
      this.formPerson.get('municipio').enable();
      //this.formPerson.get('poblacion').enable();
      this.formPerson.get('colonia').enable();
    } else {
      //this.formPerson.get('pais').disable();
      this.formPerson.get('estado').disable();
      this.formPerson.get('municipio').disable();
      //this.formPerson.get('poblacion').disable();
      this.formPerson.get('colonia').disable();
    }
    */
  }

  showDivCP: boolean;
  showDivNCP: boolean;
  radioButtonChanged($event) {
    if ($event.value == true) {
      this.showDivNCP = true;
      this.showDivCP = false;
    } else {
      this.showDivCP = true;
      this.showDivNCP = false;
    }
  }
}
