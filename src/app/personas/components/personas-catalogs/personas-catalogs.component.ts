import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {PersonasService} from '../../../services/personas.service';
import {forkJoin} from 'rxjs';
import {Catalog, CatalogValue} from '../../../models/catalogs';
import {CatalogFormComponent} from '../../../shared/catalog-form/catalog-form.component';
import {CatalogsService} from '../../../services/catalogs.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-personas-catalogs',
  templateUrl: './personas-catalogs.component.html',
  styleUrls: ['./personas-catalogs.component.scss']
})
export class PersonasCatalogsComponent implements OnInit {
  loading = false;

  ClasificacionCatalog: Catalog = {
    toCatalog(x: any): CatalogValue {
      return {text: x.nombre_clasificacion, value: x.id_clasif};
    },
    fromCatalog(c: CatalogValue): any {
      return {nombre_clasificacion: c.text, id_clasif: c.value};
    },
    systemCatalog: true,
    name: 'clasificacion_personas',
    displayName: 'Clasificación',
    values: [],
    mapValues: new Map<any, CatalogValue>(),
  };
  AgrupacionCatalog: Catalog = {
    toCatalog(x: any): CatalogValue {
      return {text: x.agrupacion, value: x.id_agrupacion};
    },
    fromCatalog(c: CatalogValue): any {
      return {agrupacion: c.text, id_agrupacion: c.value};
    },
    systemCatalog: true,
    name: 'agrupacion_personas',
    displayName: 'Agrupacion',
    values: [],
    mapValues: new Map<any, CatalogValue>(),
  };

  constructor(private dialog: MatDialog,
              public auth: AuthService,
              private ps: PersonasService,
              private cs: CatalogsService) {
  }

  ngOnInit(): void {
    this.loading = true;
    forkJoin(this.ps.getAgrupacion(), this.ps.getClasificacion())
      .subscribe(d => {
        this.AgrupacionCatalog.values = d[0];
        this.ClasificacionCatalog.values = d[1];
        this.loading = false;
      });
  }


  createClasificacion(): void {
    this.dialog.open(CatalogFormComponent, {
      data: {
        value: {},
        catalog: this.ClasificacionCatalog,
        useExternalCatalog: true,
      },
      width: '300px',
    });
  }

  editClasificacion(a: CatalogValue): void {
    this.dialog.open(CatalogFormComponent, {
      data: {
        value: JSON.parse(JSON.stringify(a)),
        catalog: this.ClasificacionCatalog,
        useExternalCatalog: true,
      },
      width: '300px',
    });
  }

  deleteClasificacion(a: CatalogValue): void {
    this.cs.deleteCatalogValue(a, null, this.ClasificacionCatalog).subscribe();
  }

  createAgrupacion(): void {
    this.dialog.open(CatalogFormComponent, {
      data: {
        value: {},
        catalog: this.AgrupacionCatalog,
        useExternalCatalog: true,
      },
      width: '300px',
    });
  }

  editAgrupacion(a: CatalogValue): void {
    this.dialog.open(CatalogFormComponent, {
      data: {
        value: JSON.parse(JSON.stringify(a)),
        catalog: this.AgrupacionCatalog,
        useExternalCatalog: true,
      },
      width: '300px',
    });
  }

  deleteAgrupacion(a: any): void {
    this.cs.deleteCatalogValue(a, null, this.AgrupacionCatalog).subscribe();
  }
}
