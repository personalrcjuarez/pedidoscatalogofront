import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonasRoutingModule } from './personas-routing.module';
import { PersonasComponent } from './personas.component';
import {SharedModule} from '../shared/shared.module';
import { PersonaFormComponent } from './components/persona-form/persona-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { PersonasCatalogsComponent } from './components/personas-catalogs/personas-catalogs.component';


@NgModule({
  declarations: [PersonasComponent, PersonaFormComponent, PersonasCatalogsComponent],
    imports: [
        CommonModule,
        PersonasRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
    ]
})
export class PersonasModule { }
