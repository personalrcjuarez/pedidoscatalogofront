import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {PersonModel} from '../models/personModel';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {PersonaFormComponent} from './components/persona-form/persona-form.component';
import {PersonasService} from '../services/personas.service';
import {CatalogValue} from '../models/catalogs';
import {AuthService} from '../services/auth.service';
import { Marca } from '../models/articles';
import { CatalogsService } from '../services/catalogs.service';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
})
export class PersonasComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['id', 'nombre', 'apellido_paterno', 'apellido_materno', 'tipo', 'createAt', 'updateAt'];
  dataSource: MatTableDataSource<PersonModel>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  errorMessage = '';
  tiposPersona = {};
  loading = false;
  marcas: Marca[];

  constructor(
    private dialog: MatDialog, 
    public ps: PersonasService, 
    public cs: CatalogsService,
    public auth: AuthService
    ) 
    {
    this.dataSource = new MatTableDataSource<PersonModel>([]);
  }

  ngOnInit(): void {

    this.loading = true;
    this.ps.getPersons().subscribe(d => {
      this.loading = false;
    }, (err) => {
      this.loading = false;
      this.errorMessage = err.error.mensajeInformativo || '';
    });
    this.ps.personas.subscribe(p => {
      this.dataSource.data = p;
    });
    this.ps.getTiposPersona().subscribe((d: CatalogValue[]) => {
      d.forEach(v => {
        this.tiposPersona[v.value] = v.text;
      });
    });

    this.cs.loadMarcas().subscribe(d => {
      this.marcas = d;      
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(p: PersonModel): void {
    if (!this.auth.isFilial() && !this.auth.isAdmin()) {
      return;
    }
    const dialogRef = this.dialog.open(PersonaFormComponent, {
      position: {
        top: '0',
        right: '0',
      },
      maxWidth: '100vw',
      height: '100vh',
      maxHeight: '100vh',
      disableClose: true,
      closeOnNavigation: true,
      panelClass: ['full-screen-modal', 'w-full', 'sm:w-9/12', 'md:w-6/12', 'xl:w-5/12'],
      data: {
        person: p,
        marcas: this.marcas,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
