import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {AppLayoutComponent} from './app-layout.component';
import {AuthGuard} from './guards/auth.guard';
import {AdminOrFilial} from './guards/admin-or-filial.service';
import { DowloadcatalogosComponent } from './productos/components/dowloadcatalogos/dowloadcatalogos.component';
import { ImportProductsComponent } from './productos/components/import-products/import-products.component';

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Pedidos por catálogo',
      breadcrumb: 'Pedidos por catálogo',
    },
    children: [
      {
        path: '',
        component: HomeComponent,
        data: {
          title: 'Pedidos por catálogo',
          breadcrumb: 'Pedidos por catálogo',
        },
      },
      {
        path: 'personas', loadChildren: () => import('./personas/personas.module').then(m => m.PersonasModule),
        data: {
          breadcrumb: 'Personas',
        },
        canLoad: [AuthGuard, AdminOrFilial],
      },
      {
        path: 'productos', loadChildren: () => import('./productos/productos.module').then(m => m.ProductosModule),
        data: {
          breadcrumb: 'Artículos',
        },
        canLoad: [AuthGuard, AdminOrFilial],
      }, 
      {
        path: 'import_productos', component: ImportProductsComponent,
        data: {
          breadcrumb: 'Importación masiva de productos',
        },
        canLoad: [AuthGuard, AdminOrFilial],
      },      
      {
        path: 'catalogos', component: DowloadcatalogosComponent,
        data: {
          breadcrumb: 'Descarga de catálogos',
        },
        canLoad: [AuthGuard, AdminOrFilial],
      },
      {
        path: 'pedidos', loadChildren: () => import('./pedidos/pedidos.module').then(m => m.PedidosModule),
        data: {
          breadcrumb: 'Pedidos',
        },
        canLoad: [AuthGuard],
      },
      {
        path: 'devoluciones', loadChildren: () => import('./pedidos/devoluciones/devoluciones.module').then(m => m.DevolucionesModule),
        data: {
          breadcrumb: 'Devoluciones',
        },
        canLoad: [AuthGuard],
      },
      {
        path: 'almacenes', loadChildren: () => import('./almacenes/almacenes.module').then(m => m.AlmacenesModule),
        data: {
          breadcrumb: 'Almacenes',
        },
        canLoad: [AuthGuard],
      },
    ]

  },
  {
    path: 'login',
    component: LoginComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
