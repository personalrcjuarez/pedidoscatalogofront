import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, of } from 'rxjs';
import { Color, Marca, Talla } from 'src/app/models/articles';
import { ReporteDevoluciones } from 'src/app/models/devolucion';
import { filters, QueryReport } from 'src/app/models/pedidos';
import { PersonModel } from 'src/app/models/personModel';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { PedidosService } from 'src/app/services/pedidos.service';
import * as XLSX from 'xlsx';
import { formatDate } from '../home.component';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-reporte-devoluciones',
  templateUrl: './reporte-devoluciones.component.html',
  styleUrls: ['./reporte-devoluciones.component.scss']
})
export class ReporteDevolucionesComponent implements OnInit {

  displayedDataDevs = [];
  displayedColumnsDevs: string[] = ['id', 'updateAt', 'estatusPed', 'moneda',
    'cliente', 'estatusDev', 'montoDev', 'producto',
    'clave', 'cantidadDev', 'color', 'talla', 'marca', 'importe', 'precio'];

  selectedTalla: Talla;
  filteredTallas: Observable<Talla[]> = of([]);
  errorMessage = '';
  nameProductDev: string = "";
  marcaInput = new FormControl();
  selectedMarca: Marca;
  persons: Map<any, PersonModel> = new Map<any, PersonModel>();
  fechaActual = new Date();
  fechaAnterior: Date;
  loading = false;
  tallas: Talla[];
  colores: Color[];
  marcas: Marca[];
  colorInput = new FormControl();
  selectedColor: Color;
  filteredColores: Observable<Color[]> = of([]);
  tallaInput = new FormControl();
  filteredMarcas: Observable<Marca[]> = of([]);

  dataSourceDevs: MatTableDataSource<any>;
  @ViewChild('paginatorDevs', { static: true }) paginatorDevs: MatPaginator;
  @ViewChild('TABLEDEVS') table2: ElementRef;
  reporteDevs: ReporteDevoluciones[] = [];
  @ViewChild(MatSort) sort: MatSort;
  monedaSeleccionada: string = 'GTQ';

  constructor(
    private snack: MatSnackBar,
    private pedidosService: PedidosService,
    private cs: CatalogsService,
  ) {
    let diasAnteriores = 10;
    this.fechaAnterior = new Date((this.fechaActual.getTime() - (diasAnteriores * 1000 * 60 * 60 * 24)));

   }

  ngOnInit(): void {
    this.loading = true;
    
    this.cs.loadCatalogs();


    this.cs.loadMarcas().subscribe(marcs => {        
      if (marcs.length > 0) {
        //carga los demás catálogos
        this.cs.availableCatalogs.subscribe( cat => {
          //Recorre y asigna las tallas
          this.tallas = [];
          cat.tallas.values.forEach(t => {

            let talla: Talla = {};              
            talla.idTalla = Number(t.value);
            talla.nombreTalla = t.text;
            
            this.tallas.push(talla);
          });

          //Recorre y asigna los colores
          this.colores = [];
          cat.colores.values.forEach(t => {

            let color: Color = {};
            color.idColor = Number(t.value);
            color.color = t.text;
            
            this.colores.push(color);
          });

        });

        this.marcas = marcs;
        this.cargarVistaReportes();
        this.loading = false;
      }
    });
  }

  //Funciones utiles
  ExportTOExcelDevs() {
    let dataToExport = this.dataSourceDevs.filteredData
      .map(x => ({
        numero_pedido: x.id,
        fecha: formatDate(x.updateAt),
        estatus_pedido: x.estatusPed,
        moneda: x.moneda,
        cliente: x.cliente,
        estatus_devolucion: x.estatusDev,
        importe_devolucion: x.montoDev,
        articulo: x.producto,
        clave: x.clave,
        cantidad_devuelta: x.cantidadDev,
        color: x.color,
        talla: x.talla,
        marca: x.marca,
        total: x.importe,
        precio: x.precio,
      }));

    let workSheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataToExport, <XLSX.Table2SheetOpts>{ sheet: 'Sheet 1' });
    let workBook: XLSX.WorkBook = XLSX.utils.book_new();

    // Adjust column width
    var wscols = [
      { wch: 7 },
      { wch: 20 },
      { wch: 20 },
      { wch: 10 },
      { wch: 5 },
      { wch: 10 },
      { wch: 10 },
      { wch: 10 },
      { wch: 20 },
      { wch: 20 },
    ];

    workSheet["!cols"] = wscols;

    XLSX.utils.book_append_sheet(workBook, workSheet, 'Sheet 1');
    XLSX.writeFile(workBook, `ReporteDevoluciones.xlsx`);
  }

  getTotalDevs() {
    if (this.reporteDevs != null) {
      let dato = this.reporteDevs.map(t => t.Importe).reduce((acc, value) => acc + value, 0);
      return dato;
    }
    return 0;
  }

  getCantidadDevs() {
    if (this.reporteDevs != null) {
      let dato = this.reporteDevs.map(t => t.CantidadDevuelta).reduce((acc, value) => acc + value, 0);
      return dato;
    }
    return 0;
  }

  ngAfterViewDevsInit(): void {
    this.dataSourceDevs.paginator = this.paginatorDevs;
    this.dataSourceDevs.sort = this.sort;
  }

  displayColorFn(color: Color): string {
    return color ? `${color.color}` : '';
  }

  selectColor(c: MatAutocompleteSelectedEvent): void {
    this.selectedColor = c.option.value;
  }

  displayTallaFn(talla: Talla): string {
    return talla ? `${talla.nombreTalla}` : '';
  }

  selectTalla(c: MatAutocompleteSelectedEvent): void {
    this.selectedTalla = c.option.value;
  }

  displayMarcaFn(marca: Marca): string {
    return marca ? `${marca.NombreMarca}` : '';
  }

  selectMarca(c: MatAutocompleteSelectedEvent): void {
    this.selectedMarca = c.option.value;
  }

  generarReporteDevolucion() {
    let filt: filters[] = [
      {
        campo: "updated_at",
        operador: ">=",
        valor: this.fechaAnterior,
      },
      {
        campo: "updated_at",
        operador: "<=",
        valor: this.fechaActual,
      }
    ];

    if (this.nameProductDev != "") {
      filt.push(this.returnAFilter("nombre_articulo", "like", '%' + this.nameProductDev + '%'));
    }

    if (this.selectedColor != null) {
      filt.push(this.returnAFilter("nombre_color", "=", this.selectedColor.color));
    }

    if (this.selectedTalla != null) {
      filt.push(this.returnAFilter("nombre_talla", "=", this.selectedTalla.nombreTalla));
    }

    if (this.selectedMarca != null) {
      filt.push(this.returnAFilter("nombre_marca", "=", this.selectedMarca.NombreMarca));
    }

    let filtros: QueryReport = {
      SearchProduct: true,
      ProductName: this.nameProductDev,
      Filters: filt,
    }
    this.loading = true;

    if (filtros.Filters.length > 0) {

    } else {
      this.loading = false;
      this.snack.open("Seleccione alguna de las opciones", '',
        {
          horizontalPosition: 'center',
          verticalPosition: 'top',
          duration: 1500,
        });
    }

    this.obtenerDatosDevs(filtros);
  }

  obtenerDatosDevs(filtros: QueryReport): void {
    this.pedidosService.getDevolucionesReporte(filtros).subscribe(dev => {
      this.reporteDevs = dev;

      dev.forEach(p => {
        p.cliente = this.persons.get(p.ClienteId);
        //p.vendedor = this.persons.get(p.vendedorId);
        //p.almacen = this.almacenes.get(p.AlmacenID);
      });

      if (dev == null) {
        this.dataSourceDevs.data = null;
        this.displayedDataDevs = null;
        this.reporteDevs = null;
        //this.getTotalImporte();
        //this.getTotalSaldo();
      } else {
        this.displayedDataDevs = dev.map(p => {
          return {
            id: p.ID,
            updateAt: p.UpdatedAt,
            estatusPed: p.NombreStatus,
            moneda: p.ClaveMoneda,
            cliente: `${p.cliente?.nombres} ${p.cliente?.apellido1} ${p.cliente?.apellido2}`,
            estatusDev: p.EstatusDevolucion,
            montoDev: p.MontoDevolucion,
            producto: p.NombreArticulo,
            clave: p.ClaveArticulo,
            cantidadDev: p.CantidadDevuelta,
            color: p.NombreColor,
            talla: p.NombreTalla,
            marca: p.NombreMarca,
            importe: p.Importe,
            precio: p.Precio,
          };
        });
        this.dataSourceDevs = new MatTableDataSource<any>([]);
        this.dataSourceDevs.data = this.displayedDataDevs;
        this.ngAfterViewDevsInit();
      }

      this.loading = false;
    }, err => {
      console.log("Error al obtener los datos de las devoluciones: ", err);
      this.dataSourceDevs = null;
      this.displayedDataDevs = null;
      this.reporteDevs = null;
      this.loading = false;
      this.snack.open(`${err.error.mensajeInformativo}`, '', {
        duration: 4000,
      });
    });
  }

  cargarVistaReportes(): void {

    this.cargaInputMarca();
    this.cargaInputColor();
    this.cargaInputTalla()
    this.loading = false;
  }

  dateRangeChange(dateRangeStart: HTMLInputElement, dateRangeEnd: HTMLInputElement) {
    this.fechaAnterior = new Date(dateRangeStart.value);
    this.fechaActual = new Date(dateRangeEnd.value);
  }

  returnAFilter(campo: string, operador: string, valor: any): filters {
    let ft: filters = {
      campo: campo,
      operador: operador,
      valor: valor,
    };
    return ft;
  }

  //Marcas

  cargaInputMarca(): void {

    this.filteredMarcas = this.marcaInput.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value && value.toLowerCase ? value.toLowerCase() : '';
          return this.marcas.filter(option => option.NombreMarca.toLowerCase().includes(filterValue));
        })
      );
  }  

  cargaInputColor(): void {

    this.filteredColores = this.colorInput.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value && value.toLowerCase ? value.toLowerCase() : '';
          return this.colores.filter(option => option.color.toLowerCase().includes(filterValue));
        })
      );
  }  

  cargaInputTalla(): void {

    this.filteredTallas = this.tallaInput.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value && value.toLowerCase ? value.toLowerCase() : '';
          return this.tallas.filter(option => option.nombreTalla.toLowerCase().includes(filterValue));
        })
      );
  }

}
