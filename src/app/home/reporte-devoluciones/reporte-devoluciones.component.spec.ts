import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteDevolucionesComponent } from './reporte-devoluciones.component';

describe('ReporteDevolucionesComponent', () => {
  let component: ReporteDevolucionesComponent;
  let fixture: ComponentFixture<ReporteDevolucionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteDevolucionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteDevolucionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
