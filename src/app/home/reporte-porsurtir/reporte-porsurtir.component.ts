import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';import { forkJoin, Observable, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Marca } from 'src/app/models/articles';
import { ReporteProductosSolicitados } from 'src/app/models/devolucion';
import { filters } from 'src/app/models/pedidos';
import { PedidosService } from 'src/app/services/pedidos.service';
import * as XLSX from 'xlsx';
import { Almacen } from 'src/app/models/almacenes';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { APIResponse } from 'src/app/models/common';

@Component({
  selector: 'app-reporte-porsurtir',
  templateUrl: './reporte-porsurtir.component.html',
  styleUrls: ['./reporte-porsurtir.component.scss']
})
export class ReportePorsurtirComponent implements OnInit {

  displayedData = [];
  displayedColumns: string[] = ['NombreAlmacen', 'Clave', 'Nombre', 'Color',
    'NombreTalla', 'NombreUnidad', 'NombreMarca', 'TotalCantidad'];

  fechaActual = new Date();
  fechaAnterior: Date;
  marcaInput = new FormControl();
  filteredMarcas: Observable<Marca[]> = of([]);
  marcas: Marca[];
  selectedMarca: Marca;
  loading = false;
  dataSource: APIResponse = null;
  reporte: ReporteProductosSolicitados[] = [];
  @ViewChild('paginator', { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  errorMessage = '';
  almacenSelected: Almacen;
  postPerPage = 5;
  pageNumber = 1;
  campoOrden = "CreatedAt";
  direccionOrden = "desc";
  almacenes: Almacen[] = [];

  constructor(
    private snack: MatSnackBar,
    private pedidosService: PedidosService,
    private cs: CatalogsService,
  ) {
    let diasAnteriores = 30;
    this.fechaAnterior = new Date((this.fechaActual.getTime() - (diasAnteriores * 1000 * 60 * 60 * 24)));
  }

  ngOnInit(): void {

    this.loading = true;
    const observable = forkJoin({
      marcas: this.cs.loadMarcas(),
      almacenes: this.cs.loadAlmacenes()
    });

    observable.subscribe({
      next: value => {
        this.almacenes = value.almacenes;
        this.marcas = value.marcas;
      },
      complete: () => {
        this.cargaInputMarca();
        this.loading = false;
      },
    });

  }

  dateRangeChange(dateRangeStart: HTMLInputElement, dateRangeEnd: HTMLInputElement) {
    this.fechaAnterior = new Date(dateRangeStart.value);
    this.fechaActual = new Date(dateRangeEnd.value);
  }

  //Marcas

  cargaInputMarca(): void {

    this.filteredMarcas = this.marcaInput.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value && value.toLowerCase ? value.toLowerCase() : '';
          return this.marcas.filter(option => option.NombreMarca.toLowerCase().includes(filterValue));
        })
      );
  }

  displayMarcaFn(marca: Marca): string {
    return marca ? `${marca.NombreMarca}` : '';
  }

  selectMarca(c: MatAutocompleteSelectedEvent): void {
    this.selectedMarca = c.option.value;
  }

  generarReporte() {
    let filt: filters[] = [
      {
        campo: "updated_at",
        operador: ">=",
        valor: this.fechaAnterior,
      },
      {
        campo: "updated_at",
        operador: "<=",
        valor: this.fechaActual,
      }
    ];

    if (this.selectedMarca != null) {
      filt.push(this.returnAFilter("id_marca", "=", this.selectedMarca.IDMarca));
    }

    if (this.almacenSelected != undefined) {
      filt.push(this.returnAFilter("almacen_id", "=", this.almacenSelected.id));
    }

    this.obtenerDatos(filt, this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden);
  }

  returnAFilter(campo: string, operador: string, valor: any): filters {
    let ft: filters = {
      campo: campo,
      operador: operador,
      valor: valor,
    };
    return ft;
  }

  obtenerDatos(filtros: filters[], limit: number, page: number, active: string, direction: string): void {
    this.pedidosService.getProductosSolicitados(filtros, limit, page, active, direction).subscribe(dev => {


      if (dev == null) {
        this.dataSource.DataMeta.items = null;
        this.displayedData = null;
        this.reporte = null;
        this.getTotalSolicitado();
      } else {
        this.dataSource = dev;
        this.reporte = dev.DataMeta.items;
        this.ngAfterViewInit();
      }

      this.loading = false;
    }, err => {
      console.log("Error al obtener los datos de las devoluciones: ", err);
      this.dataSource = null;
      this.displayedData = null;
      this.reporte = null;
      this.loading = false;
      this.snack.open(`${err.error.mensajeInformativo}`, '', {
        duration: 4000,
      });
    });
  }

  ngAfterViewInit(): void {
    /*
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    */
  }

  //Funciones utiles
  ExportTOExcel() {

    let workSheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.reporte, <XLSX.Table2SheetOpts>{ sheet: 'Sheet 1' });
    let workBook: XLSX.WorkBook = XLSX.utils.book_new();

    // Adjust column width
    var wscols = [
      { wch: 7 },
      { wch: 20 },
      { wch: 20 },
      { wch: 10 },
      { wch: 5 },
      { wch: 10 },
      { wch: 10 },
      { wch: 10 },
      { wch: 20 },
      { wch: 20 },
    ];

    workSheet["!cols"] = wscols;

    XLSX.utils.book_append_sheet(workBook, workSheet, 'Sheet 1');
    XLSX.writeFile(workBook, `ReporteProductosPorSurtir.xlsx`);

  }

  getTotalSolicitado() {
    if (this.reporte != null) {
      let dato = this.reporte.map(t => t.TotalCantidad).reduce((acc, value) => acc + value, 0);
      return dato;
    }
    return 0;
  }

  cambiaAlmacen(a: any): void {
    this.almacenSelected = a.value;
  }

  onPaginate(pageEvent: PageEvent) {

    this.postPerPage = +pageEvent.pageSize;
    this.pageNumber = +(pageEvent.pageIndex + 1);

    let filt: filters[] = [
      {
        campo: "updated_at",
        operador: ">=",
        valor: this.fechaAnterior,
      },
      {
        campo: "updated_at",
        operador: "<=",
        valor: this.fechaActual,
      }
    ];

    if (this.selectedMarca != null) {
      filt.push(this.returnAFilter("id_marca", "=", this.selectedMarca.IDMarca));
    }

    if (this.almacenSelected != undefined) {
      filt.push(this.returnAFilter("almacen_id", "=", this.almacenSelected.id));
    }

    this.obtenerDatos(filt, this.postPerPage, this.pageNumber, this.campoOrden, this.direccionOrden);
  }

}
