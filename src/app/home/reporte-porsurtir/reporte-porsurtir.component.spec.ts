import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportePorsurtirComponent } from './reporte-porsurtir.component';

describe('ReportePorsurtirComponent', () => {
  let component: ReportePorsurtirComponent;
  let fixture: ComponentFixture<ReportePorsurtirComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportePorsurtirComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportePorsurtirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
