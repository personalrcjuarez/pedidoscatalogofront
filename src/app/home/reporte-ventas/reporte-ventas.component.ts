import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatPaginator } from '@angular/material/paginator';
import { MatRadioChange } from '@angular/material/radio';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { forkJoin, Observable, of } from 'rxjs';
import { filters, PedidoFront, QueryReport, StatusPedido } from 'src/app/models/pedidos';
import { PersonModel } from 'src/app/models/personModel';
import { PedidosService } from 'src/app/services/pedidos.service';
import { formatDate } from '../home.component';
import * as XLSX from 'xlsx';
import { Marca } from 'src/app/models/articles';
import { AuthService } from 'src/app/services/auth.service';
import { map, startWith } from 'rxjs/operators';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { PersonasService } from 'src/app/services/personas.service';
import { Almacen } from 'src/app/models/almacenes';

@Component({
  selector: 'app-reporte-ventas',
  templateUrl: './reporte-ventas.component.html',
  styleUrls: ['./reporte-ventas.component.scss']
})
export class ReporteVentasComponent implements OnInit {

  monedaSeleccionada: string = 'GTQ';
  fechaActual = new Date();
  fechaAnterior: Date;
  selectedCustomer: PersonModel;
  selectedSeller: PersonModel;
  customerInput = new FormControl();
  sellerInput = new FormControl();  
  status: string[] = [];
  statusSelected: string;
  checkSelected: boolean = false;
  nameProduct: string = "";
  loading = false;
  pedidos: PedidoFront[] = [];
  almacenes: Map<any, Almacen> = new Map<any, Almacen>();
  persons: Map<any, PersonModel> = new Map<any, PersonModel>();
  dataSource: MatTableDataSource<any>;
  displayedData = [];
  @ViewChild('paginatorPeds', {static: true}) paginatorPeds: MatPaginator;
  @ViewChild('TABLE') table: ElementRef;  
  displayedColumns: string[] = ['id', 'afiliado', 'encargado', 'almacen', 'moneda', 'total', 'createAt', 'updateAt', 'status', 'saldoActual', 'estatusDevolucion', 'montoDevolucion'];
  @ViewChild(MatSort) sort: MatSort;
  errorMessage = '';
  filteredSellers: Observable<PersonModel[]> = of([]);
  filteredCustomers: Observable<PersonModel[]> = of([]);
  filteredMarcas: Observable<Marca[]> = of([]);
  clientes: PersonModel[] = [];
  vendedores: PersonModel[] = [];
  marcaInput = new FormControl();
  marcas: Marca[];
  readonly customerType = 3;
  readonly sellerType = 2;

  constructor(
    private cs: CatalogsService,
    private snack: MatSnackBar,
    public auth: AuthService,
    private pedidosService: PedidosService,
    private pes: PersonasService,
  ) { 
    this.dataSource = new MatTableDataSource<any>([]);

    let diasAnteriores = 10;
    this.fechaAnterior = new Date((this.fechaActual.getTime() - (diasAnteriores * 1000 * 60 * 60 * 24)));

  }

  ngOnInit(): void {
    this.loading = true;
    const observable = forkJoin({
      personas: this.pes.getPersons(),
      almacenes: this.cs.loadAlmacenes()
    });

    observable.subscribe({
     next: value => {
      value.almacenes.forEach(v => {
        this.almacenes.set(v.id, v);
      });
      this.clientes = value.personas;
      this.vendedores = value.personas;
      value.personas.forEach(v => {
        this.persons.set(v.id, v);
      });      
     },
     complete: () => {
      this.cargarVistaReportes();
     },
    });

    this.loading = false;

  }

  cambiaMoneda(evento: MatRadioChange): void {
    this.monedaSeleccionada = evento.value
  }

  builtFiltersPedidos(): void {

    let filt: filters[] = [
      {
        campo: "created_at",
        operador: ">=",
        valor: this.fechaAnterior,
      },
      {
        campo: "created_at",
        operador: "<=",
        valor: this.fechaActual,
      }
    ];

    if (this.selectedSeller != null) {
      filt.push(this.returnAFilter("vendedor_id", "=", this.selectedSeller.id));
    }


    if (this.selectedCustomer != null && this.customerInput.value != "") {
      filt.push(this.returnAFilter("cliente_id", "=", this.selectedCustomer.id));
    }

    if (this.statusSelected != "" && this.statusSelected != "*") {
      filt.push(this.returnAFilter("nombre_status", "=", this.statusSelected));
    }

    if (this.monedaSeleccionada != "") {
      filt.push(this.returnAFilter("clave_moneda", "=", this.monedaSeleccionada));
    }

    let filtros: QueryReport = {
      SearchProduct: this.checkSelected,
      ProductName: this.nameProduct,
      Filters: filt,
    }
    this.loading = true;
    this.obtenerDatos(filtros);
  }

  generarReportePedidos() {

    if (this.checkSelected) {
      if (this.nameProduct != "") {
        this.builtFiltersPedidos();
      } else {
        this.snack.open("Desactive la búsqueda de productos o ingrese un nombre/clave de producto", '',
          {
            horizontalPosition: 'center',
            verticalPosition: 'top',
            duration: 1500,
          });
      }
    } else {
      this.builtFiltersPedidos();
    }
  }  

  obtenerDatos(filtros: QueryReport): void {
    this.pedidosService.getPedidosReporte(filtros).subscribe(ped => {
      this.pedidos = ped;
      ped.forEach(p => {
        p.cliente = this.persons.get(p.ClienteId);
        //p.vend = this.persons.get(p.VendedorId);
        //p.almacen = this.almacenes.get(p.AlmacenId);
      });

      if (ped == null) {
        this.dataSource.data = null;
        this.displayedData = null;
        this.pedidos = null;
        this.getTotalImporte();
        this.getTotalSaldo();
      } else {
        this.displayedData = ped.map(p => {
          return {
            id: p.Id,
            cliente: `${p.cliente?.nombres} ${p.cliente?.apellido1} ${p.cliente?.apellido2}`,
            vendedor: p.NombreVendedor,
            almacen: p.NombreAlmacen,
            moneda: p.ClaveMoneda,
            total: p.TotalGeneral,
            nombreStatus: p.NombreStatus,
            saldoActual: p.SaldoActual,
            cancelado: p.NombreStatus === StatusPedido[6],
            createAt: p.CreatedAt,
            updateAt: p.UpdatedAt,
            estatusDevolucion: p.EstatusDevolucion,
            montoDevolucion: p.MontoDevolucion,
          };
        });
        this.dataSource = new MatTableDataSource<any>([]);
        this.dataSource.data = this.displayedData;
        this.ngAfterViewInit();
      }

      this.loading = false;
    }, err => {
      this.dataSource = null;
      this.displayedData = null;
      this.pedidos = null;
      this.loading = false;
      this.snack.open(`${err.error.mensajeInformativo}`, '', {
        duration: 4000,
      });
    });
  }  

  /** Gets the total cost of all transactions. */
  getTotalImporte() {
    if (this.pedidos != null) {
      let dato = this.pedidos.map(t => t.TotalGeneral).reduce((acc, value) => acc + value, 0);
      return dato;
    }
    return 0;
  }

  getTotalSaldo() {
    if (this.pedidos != null) {
      let dato = this.pedidos.map(t => t.SaldoActual).reduce((acc, value) => acc + value, 0);
      return dato;
    }
    return 0;
  }
    
  returnAFilter(campo: string, operador: string, valor: any): filters {
    let ft: filters = {
      campo: campo,
      operador: operador,
      valor: valor,
    };
    return ft;
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginatorPeds;
    this.dataSource.sort = this.sort;
  }  

  dateRangeChange(dateRangeStart: HTMLInputElement, dateRangeEnd: HTMLInputElement) {
    this.fechaAnterior = new Date(dateRangeStart.value);
    this.fechaActual = new Date(dateRangeEnd.value);
  }

  cambiaStatus(a: any): void {
    this.statusSelected = a.value;
  }

  setAll(completed: boolean) {
    this.checkSelected = completed;
  }

  displayClientFn(client: PersonModel): string {
    return client ? `${client.nombres} ${client.apellido1} ${client.apellido2}` : '';
  } 
  
  selectSeller(c: MatAutocompleteSelectedEvent): void {
    this.selectedSeller = c.option.value;
  }
    
  selectClient(c: MatAutocompleteSelectedEvent): void {
    this.selectedCustomer = c.option.value;
  }  

  ExportTOExcel() {
    let dataToExport = this.dataSource.filteredData
      .map(x => ({
        id: x.id,
        cliente: x.cliente,
        vendedor: x.vendedor,
        almacen: x.almacen,
        moneda: x.moneda,
        total: x.total,
        nombreStatus: x.nombreStatus,
        saldoActual: x.saldoActual,
        fecha_creacion: formatDate(x.createAt),
        fecha_actualizacion: formatDate(x.updateAt),
      }));

    let workSheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataToExport, <XLSX.Table2SheetOpts>{ sheet: 'Sheet 1' });
    let workBook: XLSX.WorkBook = XLSX.utils.book_new();

    // Adjust column width
    var wscols = [
      { wch: 7 },
      { wch: 20 },
      { wch: 20 },
      { wch: 10 },
      { wch: 5 },
      { wch: 10 },
      { wch: 10 },
      { wch: 10 },
      { wch: 20 },
      { wch: 20 },
    ];

    workSheet["!cols"] = wscols;

    XLSX.utils.book_append_sheet(workBook, workSheet, 'Sheet 1');
    XLSX.writeFile(workBook, `ReportePedidos.xlsx`);
  }

  cargarVistaReportes(): void {
    //Recorre todo los estatus de los pedidos para asignarlos a un array
    for (let stPed in StatusPedido) {
      if (isNaN(Number(stPed))) {
        this.status.push(stPed);
      }
    }
    //Status cerrado
    this.statusSelected = StatusPedido[4];
    //cargar la vista del input del cliente
    this.cargaInputCliente();
    this.cargaInputVendedor();

    this.builtFiltersPedidos();

    this.loading = false;
  }  



  cargaInputCliente(): void {
    this.clientes = this.clientes.filter(d => this.customerType === d.tipo && !d.bloqueado);
    const myself = this.clientes.filter(d => d.id === this.auth.user.userId);
    if (this.auth.isCustomer()) {
      this.customerInput.setValue(myself[0]);
      this.selectedCustomer = myself[0];
      this.customerInput.disable();
      this.sellerInput.disable();
    }
    //this.customerInput.setValue(this.selectedCustomer);
    this.filteredCustomers = this.customerInput.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value && value.toLowerCase ? value.toLowerCase() : '';
          return this.clientes.filter(option => option.nombres.toLowerCase().includes(filterValue));
        })
      );
  }  

  cargaInputVendedor(): void {
    this.vendedores = this.vendedores.filter(d => this.sellerType === d.tipo && !d.bloqueado);
    const myself = this.vendedores.filter(d => d.id === this.auth.user.userId);
    if (this.auth.isFilial()) {
      this.sellerInput.setValue(myself[0]);
      this.selectedSeller = myself[0];
      this.sellerInput.disable();
    }
    this.filteredSellers = this.sellerInput.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value && value.toLowerCase ? value.toLowerCase() : '';
          return this.vendedores.filter(option => option.nombres.toLowerCase().includes(filterValue));
        })
      );
  }  
  
}
