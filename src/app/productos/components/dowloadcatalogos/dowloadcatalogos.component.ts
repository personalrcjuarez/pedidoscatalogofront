import { Component, OnInit } from '@angular/core';
//import { APIURLPDF_X } from 'src/app/consts';
import { EnvService } from '../../../../env.service';
import { Marca } from 'src/app/models/articles';
import { CatalogsService } from '../../../services/catalogs.service';

declare var require: any
const FileSaver = require('file-saver');

@Component({
  selector: 'app-dowloadcatalogos',
  templateUrl: './dowloadcatalogos.component.html',
  styleUrls: ['./dowloadcatalogos.component.scss']
})
export class DowloadcatalogosComponent implements OnInit {

  panelOpenState = false;
  marcas: Marca[];

  constructor(
    private servBrand: CatalogsService, private env: EnvService
  ) 
  {
        //obtener las marcas
        this.servBrand.getMarcasLoaded.subscribe(marcs => {        
          if (marcs.length === 0) {
            this.servBrand.loadMarcas().subscribe(marcas => {
              this.marcas = marcas;
            })
          }else{
            this.marcas = marcs;
          }
        });
   }

  ngOnInit(): void {

  }

  downloadPdf(idBrand, nameFile: string) {
    FileSaver.saveAs(this.env.APIURL+"/"+idBrand+"_"+nameFile, nameFile);
  }

}
