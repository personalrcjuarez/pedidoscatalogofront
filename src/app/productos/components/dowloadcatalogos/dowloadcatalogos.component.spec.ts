import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DowloadcatalogosComponent } from './dowloadcatalogos.component';

describe('DowloadcatalogosComponent', () => {
  let component: DowloadcatalogosComponent;
  let fixture: ComponentFixture<DowloadcatalogosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DowloadcatalogosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DowloadcatalogosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
