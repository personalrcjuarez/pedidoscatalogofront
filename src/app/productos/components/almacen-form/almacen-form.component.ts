import {Component, Inject, OnInit} from '@angular/core';
import {CatalogsService} from '../../../services/catalogs.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import { Almacen } from 'src/app/models/almacenes';

@Component({
  selector: 'app-catalog-almacen',
  templateUrl: './almacen-form.component.html',
})
export class AlmacenFormComponent implements OnInit {
  v: Almacen;
  title = '';
  saving = false;

  constructor(private cs: CatalogsService,
              private snack: MatSnackBar,
              public dialogRef: MatDialogRef<AlmacenFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { value: Almacen }) {
    this.title = this.data?.value?.id ? `Editar almacen` : `Crear almacen`;
    this.v = this.data.value;
  }

  ngOnInit(): void {
  }

  save(): void {
    this.saving = true;
    let req: Observable<Almacen>;
    if (this.v.id) {
      req = this.cs.updateAlmacen(this.v, this.v.id);
    } else {
      req = this.cs.createAlmacen(this.v);
    }
    req.subscribe(d => {
      this.dialogRef.close(d);
      this.saving = false;
    }, err => {
      this.saving = false;
      this.snack.open('No se pudo guardar la información del almacen', 'X', {
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      this.dialogRef.close();
    });
  }
}
