import {Component, OnInit} from '@angular/core';
import {CatalogsService} from '../../../services/catalogs.service';
import {Catalog, Catalogs, CatalogValue} from '../../../models/catalogs';
import {MatDialog} from '@angular/material/dialog';
import {CatalogFormComponent} from '../../../shared/catalog-form/catalog-form.component';
import {Impuesto, Marca, Moneda} from '../../../models/articles';
import {AlmacenFormComponent} from '../almacen-form/almacen-form.component';
import {ImpuestoFormComponent} from '../impuesto-form/impuesto-form.component';
import {MarcaFormComponent} from '../marca-form/marca-form.component';
import {MonedaFormComponent} from '../moneda-form/moneda-form.component';
import {AuthService} from '../../../services/auth.service';
import { Almacen } from 'src/app/models/almacenes';

@Component({
  selector: 'app-catalogs',
  templateUrl: './catalogs.component.html',
  styleUrls: ['./catalogs.component.scss']
})
export class CatalogsComponent implements OnInit {

  catalogs: Catalogs;
  loading = false;
  almacenes: Almacen[] = [];
  marcas: Marca[] = [];
  impuestos: Impuesto[] = [];
  monedas: Moneda[] = [];
  editableCatalogs: Catalog[];

  constructor(private cs: CatalogsService, private dialog: MatDialog, public auth: AuthService) {
  }

  ngOnInit(): void {
    this.cs.availableCatalogs.subscribe(d => {
      this.loading = !d;
      this.catalogs = d;
      if (d) {
        this.editableCatalogs = [
          d.unidades,
          d.lineas,
          d.colores,
          //d.marcas,
          d.tallas
        ];
      }
    });
    this.cs.almacens.subscribe(d => {
      this.almacenes = d;
    });

    this.cs.loadAlmacenes().subscribe();
    this.cs.impuestos.subscribe(d => {
      this.impuestos = d;
    });

    this.cs.loadMarcas().subscribe();
    this.cs.marcas.subscribe(d => {
      this.marcas = d;
    });
    this.cs.loadMarcas().subscribe();

    this.cs.loadImpuestos().subscribe();
    this.cs.monedas.subscribe(d => {
      this.monedas = d;
    });
    this.cs.loadMonedas().subscribe();
  }

  createValue(c: Catalog): void {
    this.dialog.open(CatalogFormComponent, {
      data: {
        catalog: c,
        value: {
          value: '',
          id: null,
        },
        width: '300px',
      }
    });
  }

  deleteValue(v: CatalogValue, c: Catalog): void {
    this.cs.deleteCatalogValue(v, c.name).subscribe();
  }

  editValue(v: CatalogValue, c: Catalog): void {
    this.dialog.open(CatalogFormComponent, {
      data: {
        catalog: c,
        value: {
          value: v.value,
          text: v.text,
          abreviatura: v.abreviatura,
        }
      },
      width: '300px',
    });
  }

  createAlmacen(): void {
    this.dialog.open(AlmacenFormComponent, {
      data: {
        value: new Almacen(),
        width: '300px',
      }
    });
  }

  editAlmacen(a: Almacen): void {
    this.dialog.open(AlmacenFormComponent, {
      data: {
        value: a,
      },
      width: '300px',
    });
  }

  deleteAlmacen(a: Almacen): void {
    this.cs.deleteAlmacen(a, a.id).subscribe();
  }

  createImpuesto(): void {
    this.dialog.open(ImpuestoFormComponent, {
      data: {
        value: {},
        width: '300px',
      }
    });
  }

  editImpuesto(a: Impuesto): void {
    this.dialog.open(ImpuestoFormComponent, {
      data: {
        value: a,
      },
      width: '300px',
    });
  }

  editMoneda(a: Moneda): void {
    this.dialog.open(MonedaFormComponent, {
      data: {
        value: a,
      },
      width: '300px',
    });
  }

  deleteImpuesto(a: Impuesto): void {
    this.cs.deleteImpuesto(a, a.id).subscribe();
  }

  //Marcas

  createMarca(): void {
    this.dialog.open(MarcaFormComponent, {
      data: {
        value: {},
        width: '1000px',
      }
    });
  }

  editMarca(a: Marca): void {
    this.dialog.open(MarcaFormComponent, {
      data: {
        value: a,
      },
      width: '800px',
    });
  }

  deleteMarca(a: Marca): void {
    this.cs.deleteMarca(a, a.IDMarca).subscribe();
  }
  
}
