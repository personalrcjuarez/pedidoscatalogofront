import {Component, Inject, OnInit} from '@angular/core';
import { Impuesto} from '../../../models/articles';
import {CatalogsService} from '../../../services/catalogs.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-impuesto-form',
  templateUrl: './impuesto-form.component.html',
})
export class ImpuestoFormComponent implements OnInit {
  v: Impuesto;
  title = '';
  saving = false;

  constructor(private cs: CatalogsService,
              private snack: MatSnackBar,
              public dialogRef: MatDialogRef<ImpuestoFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { value: Impuesto }) {
    this.title = this.data?.value?.id ? `Editar impuesto` : `Crear impuesto`;
    this.v = this.data.value;
  }

  ngOnInit(): void {
  }

  save(): void {
    this.v.porcentaje = Number(this.v.porcentaje) || 0;
    this.saving = true;
    let req: Observable<Impuesto>;
    if (this.v.id) {
      req = this.cs.updateImpuesto(this.v, this.v.id);
    } else {
      req = this.cs.createImpuesto(this.v);
    }
    req.subscribe(d => {
      this.dialogRef.close(d);
      this.saving = false;
    }, err => {
      this.saving = false;
      this.snack.open('No se pudo guardar la información del impuesto', 'X', {
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      this.dialogRef.close();
    });
  }
}
