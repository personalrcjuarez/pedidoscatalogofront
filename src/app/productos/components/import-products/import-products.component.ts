import { Component, ViewChild, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Marca, Unidad } from 'src/app/models/articles';
import { CatalogsService } from 'src/app/services/catalogs.service';
import * as XLSX from 'xlsx';
import { DatosValidacion, ModalValidacionComponent } from './modal-validacion/modal-validacion.component';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
type AOA = any[][];

export interface catalogProductView {
  consecutivo: number;
  Clave: string;
  CodigoBarras: string;
  ClaveAlterna: string;
  NombreTalla: string;
  NombreColor: string;
  Precio: number;
  NombreProducto: string;
  Descripcion: string;
  marca: string;
  unidad: string
}

export interface catalogProduct {
  Clave: string;
  CodigoBarras: string;
  ClaveAlterna: string;
  NombreTalla: string;
  NombreColor: string;
  Precio: number;
  NombreProducto: string;
  Descripcion: string;
}

@Component({
  selector: 'app-import-products',
  templateUrl: './import-products.component.html',
  styleUrls: ['./import-products.component.scss']
})
export class ImportProductsComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = ['consecutivo', 'Clave', 'CodigoBarras', 'ClaveAlterna', 'NombreTalla', 'NombreColor', 'Precio', 'NombreProducto', 'Descripcion', 'marca', 'unidad'];
  informacionEncabezado: string[] = ['CLAVE-PRINCIPAL', 'CODIGO-BARRAS', 'CLAVE-ALTERNA', 'TALLA', 'COLOR', 'PRECIO', 'NOMBRE', 'DESCRIPCIÓN'];
  dataSource: MatTableDataSource<catalogProduct>;

  loading = false;

  element = false;
  marcaInput = new FormControl();
  unidadInput = new FormControl();
  selectedMarca: Marca;
  selectedUnidad: Unidad;
  filteredMarcas: Observable<Marca[]> = of([]);
  filteredUnidades: Observable<Unidad[]> = of([]);
  marcas: Marca[];
  unidadesAll: Unidad[] = [];
  users: catalogProductView[] = [];
  catalogos: catalogProduct[] = [];

  constructor(
    private cs: CatalogsService,

    private snack: MatSnackBar,
    private dialog: MatDialog,
  ) {
    //const users: catalogProduct[] = [];
  }

  ngOnInit(): void {

    this.loading = true;

    this.cs.marcas.subscribe(cat => {
      this.marcas = cat;
      this.cargaInputMarca();
    });

    this.cs.availableCatalogs.subscribe(cat => {
      cat?.unidades?.values.forEach(unidades => {
        this.unidadesAll.push({ idUnidad: Number(unidades.value), nombreUnidad: unidades.text })
      });
      this.cargaInputUnidad();
    });
    this.loading = false;

  }


  fileName: string = 'SheetJS.xlsx';
  data: any;
  headData: any // excel row header

  /* <input type="file" (change)="onFileChange($event)" multiple="false" /> */
  /* ... (within the component class definition) ... */
  onFileChange(evt: any) {
    this.loading = true;
    this.data = null;
    this.headData = null;
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1, raw: false, range: 0 }));


      this.headData = this.data[0];
      this.data = this.data.slice(1); // remove first header record

      for (let i = 0; i < this.data.length; i++) {
        let renglon = this.data[i];

        let myrow = {
          consecutivo: i + 1,
          Clave: renglon[0],
          CodigoBarras: renglon[1],
          ClaveAlterna: renglon[2],
          NombreTalla: renglon[3],
          NombreColor: renglon[4],
          Precio: renglon[5],
          NombreProducto: renglon[6],
          Descripcion: renglon[7],
          marca: this.selectedMarca?.NombreMarca,
          unidad: this.selectedUnidad?.nombreUnidad,
          myColor: this.getNameEnglishColor(renglon[4])
        };

        let real = {
          Clave: renglon[0],
          CodigoBarras: renglon[1],
          ClaveAlterna: renglon[2],
          NombreTalla: renglon[3],
          NombreColor: renglon[4],
          Precio: Number(renglon[5]),
          NombreProducto: renglon[6],
          Descripcion: renglon[7]
        };

        this.users.push(myrow);
        this.catalogos.push(real);
      }

      this.dataSource = new MatTableDataSource(this.users);
      this.ngAfterViewInit();

      this.loading = false;
    };
    reader.readAsBinaryString(target.files[0]);
  }

  getNameEnglishColor(spanishName: string): string {
    let nameEnglishColor = '';
    switch (spanishName.toLocaleUpperCase()) {
      case 'ARENA':
        nameEnglishColor = 'sand';
        break;
      case 'MEZCLILLA':
      case 'AZUL/VERDE':
      case 'AZUL MEZCLILLA':
        nameEnglishColor = 'blue';
        break;
      case 'IVORY':
        nameEnglishColor = 'pink';
        break;
      case 'VERDE':
        nameEnglishColor = 'green';
        break;
      case 'MOZTAZA':
      case 'AMARILLO':
      case 'ORO':
      case 'MOSTAZA':
        nameEnglishColor = 'yellow';
        break;
      case 'ANARANJADO':
      case 'NARANJADO':
      case 'NARANJA':
        nameEnglishColor = 'orange';
        break;
      case 'GRIS':
        nameEnglishColor = 'gray';
        break;
      case 'UVA':
        nameEnglishColor = 'violet';
        break;
      case 'MARINO':
      case 'AZUL MARINO':
        nameEnglishColor = 'blue';
        break;
      case 'MIEL':
        nameEnglishColor = 'YELLOW';
        break;
      case 'MULTICOLOR':
        nameEnglishColor = 'black';
        break;
      case 'ROSA':
      case 'FIUSHA':
      case 'GRIS/ROSA':
      case 'BUGAMBILIA':
        nameEnglishColor = 'pink';
        break;        
      case 'PLATA':
        nameEnglishColor = 'plate';
        break;
      case 'AZUL':
        nameEnglishColor = 'blue';
        break;
      case 'ROJO':
        nameEnglishColor = 'red';
        break;
      case 'CAFÉ':
      case 'CAFE':
      case 'BLANCO/CAFÉ':
      case 'CAFÉ/NUDE':
      case 'CAFÉ MAQUILLAJE':
      case 'CHOCOLATE':
      case 'BEIGE':
        nameEnglishColor = 'brown';
        break;
      case 'MAQUILLAJE':
        nameEnglishColor = 'yellow';
        break;
      case 'PALO DE ROSA':
        nameEnglishColor = 'pink';
        break;
      case 'TURQUESA':
        nameEnglishColor = 'turquoise';
        break;
      case 'VERDE OLIVO':
        nameEnglishColor = 'green';
        break;
      case 'AZUL REY':
        nameEnglishColor = 'blue';
        break;
      case 'VINO':
        nameEnglishColor = 'wine';
        break;
      case 'MORADO':
        nameEnglishColor = 'purpple';
        break;
      case 'LILA':
        nameEnglishColor = 'lilac';
        break;
      case 'VERDE MILITAR':
      case 'JADE':
        nameEnglishColor = 'green';        
        break;
      default:
        nameEnglishColor = 'black';
        break;
    }
    return nameEnglishColor;
  }

  showData() {
    return (this.element = true);
  }

  hideData() {
    return (this.element = false);
  }

  displayMarcaFn(marca: Marca): string {
    return marca ? `${marca.NombreMarca}` : '';
  }

  displayUnidadFn(unidad: Unidad): string {
    return unidad ? `${unidad.nombreUnidad}` : '';
  }

  selectMarca(c: MatAutocompleteSelectedEvent): void {
    this.selectedMarca = c.option.value;
    this.updateData();
  }

  updateData() {
    if (this.users) {
      for (let i = 0; i < this.users.length; i++) {
        this.users[i].marca = this.selectedMarca?.NombreMarca;
        this.users[i].unidad = this.selectedUnidad?.nombreUnidad;
      }
      this.dataSource = new MatTableDataSource(this.users);
      this.ngAfterViewInit();
    }
  }

  selectUnidad(c: MatAutocompleteSelectedEvent): void {
    this.selectedUnidad = c.option.value;
    this.updateData();
  }

  //Marcas

  cargaInputMarca(): void {

    this.filteredMarcas = this.marcaInput.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value && value.toLowerCase ? value.toLowerCase() : '';
          return this.marcas.filter(option => option.NombreMarca.toLowerCase().includes(filterValue));
        })
      );
  }

  cargaInputUnidad(): void {
    this.filteredUnidades = this.unidadInput.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value.nombreUnidad && value?.nombreUnidad.toLowerCase ? value.nombreUnidad.toLowerCase() : '';
          return this.unidadesAll.filter(option => option.nombreUnidad.toLowerCase().includes(filterValue));
        })
      );
  }

  checkValidation() {

    let datoValid: DatosValidacion = {
      NombreMarca: this.selectedMarca?.NombreMarca,
      NombreUnidad: this.selectedUnidad?.nombreUnidad,
      Registros: this.users,
      Encabezado: this.headData,
      informacionEncabezado: this.informacionEncabezado,

      IdMarca: this.selectedMarca.IDMarca,
      IdUnidad: this.selectedUnidad.idUnidad,
      Articulos: this.catalogos
    };

    this.openDialog(datoValid);
  }

  openDialog(datoValid: DatosValidacion): void {

    this.snack.open(`Validando información... Espere`, 'Validando', {
      duration: 5000,
      verticalPosition: 'top',
      horizontalPosition: 'center',
    });

    const dialogRef = this.dialog.open(ModalValidacionComponent, {
      maxWidth: '100vw',
      height: '85vh',
      maxHeight: '85vh',
      closeOnNavigation: true,
      data: datoValid,
    });

    dialogRef.afterClosed().subscribe((result: Set<any>) => {
      console.log("Cerrando la ventana; ", result);
    });
  }

  ngAfterViewInit() {
    if (this.dataSource) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
