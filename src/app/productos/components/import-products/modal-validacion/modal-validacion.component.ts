import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { catalogProduct } from '../import-products.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from "rxjs";
import { ProductsService } from 'src/app/services/products.service';

export interface DatosValidacion {
  NombreMarca: string
  NombreUnidad: string
  Registros: catalogProduct[];
  Encabezado: string[];
  informacionEncabezado: string[];  

  IdMarca: number,
  IdUnidad: number,
  Articulos: any[]
}

@Component({
  selector: 'app-modal-validacion',
  templateUrl: './modal-validacion.component.html',
  styleUrls: ['./modal-validacion.component.scss']
})

export class ModalValidacionComponent implements OnInit {

  observaciones = '';
  messages: any[] = [];
  messagesResponse: string[] = [];
  loading2 = false;
  hayErrores = 0;
  showMessageResponse: boolean;

  constructor(public dialogRef: MatDialogRef<ModalValidacionComponent>,
    private snack: MatSnackBar,
    private _service_prods: ProductsService,
    @Inject(MAT_DIALOG_DATA) public datoValid: DatosValidacion) {
    this.loading2 = true;
    this.messages = [];

    //Validando el encabezado
    this.validarEncabezado(datoValid);
    //Validando el nombre de la marca
    this.validarMarca(datoValid );
    //Validando el nombre de la unidad
    this.validarUnidad(datoValid );
    //Validando los registros
    this.validarInformacion(datoValid );
    this.loading2 = false;

    this.showMessageResponse = false;
  }

  ngOnInit(): void {

  }



  accept(): void {
    this.dialogRef.close(this.observaciones);
  }

  close(): void {
    this.dialogRef.close(null);
  }

  validarEncabezado(data: DatosValidacion ) {
    this.showMessageResponse = false;
    if (data.Encabezado == undefined) {
      this.messages.push({
        'from': 'Encabezado',
        'subject': 'Seleccione un archivo',
        'isValid': false
      });
      this.hayErrores++;
    } else {
      let badCols = [];

      if (data.informacionEncabezado.length != data.Encabezado.length) {
        this.messages.push({
          'from': 'Columnas',
          'subject': 'Número de columnas incorrecto: ' + data.informacionEncabezado.length + " -- " + data.Encabezado.length,
          'isValid': false
        });
        this.hayErrores++;
      } else {
        this.messages.push({
          'from': 'Columnas',
          'subject': 'Número de columnas correcto',
          'isValid': true
        });
        for (let index = 0; index < data.informacionEncabezado.length; index++) {
          if (data.informacionEncabezado[index] != data.Encabezado[index]) {
            badCols.push(data.Encabezado[index]);
          }
        }

        if (badCols.length > 0) {
          this.messages.push({
            'from': 'Datos',
            'subject': 'Los nombres de las columnas o el orden de la información no coinciden: ' + badCols,
            'isValid': false
          });
          this.hayErrores++;
        } else {
          this.messages.push({
            'from': 'Datos',
            'subject': 'Nombre y orden de columnas correcto',
            'isValid': true
          });
        }
      }
    }
  }

  validarMarca(data: DatosValidacion ) {
    if (data.NombreMarca == "selectMarca" || data.NombreMarca == undefined) {
      this.messages.push({
        'from': 'Marca',
        'subject': 'Seleccione una marca',
        'isValid': false
      });
      this.hayErrores++;
    } else {
      this.messages.push({
        'from': 'Marca',
        'subject': 'Marca seleccionada: ' + data.NombreMarca,
        'isValid': true
      });
    }
  }

  validarInformacion(data: DatosValidacion) {
    if (data.Registros == null) {
      this.messages.push({
        'from': 'Registros',
        'subject': 'Seleccione un archivo',
        'isValid': false
      });
      this.hayErrores++;
    } else {
      //validar duplicidad
      let duplicados = 0;

      data.Registros.forEach(element => {

        let filtrado = data.Registros.filter(option =>
          option.Clave == element.Clave &&
          option.CodigoBarras == element.CodigoBarras &&
          option.NombreColor == element.NombreColor &&
          option.NombreTalla == element.NombreTalla &&
          option.Descripcion == element.Descripcion
        );

        if (filtrado.length > 1) {
          duplicados++;
        }
      });

      if (duplicados > 0) {
        this.messages.push({
          'from': 'Registros',
          'subject': 'Existen ' + duplicados + ' registros duplicados',
          'isValid': false
        });
        this.hayErrores++;
      } else {
        this.messages.push({
          'from': 'Registros',
          'subject': 'Número de registros: ' + data?.Registros?.length,
          'isValid': true
        });
      }
    }
  }

  validarUnidad(data: DatosValidacion) {
    if (data.NombreUnidad == null) {
      this.messages.push({
        'from': 'Unidad',
        'subject': 'Seleccione una unidad',
        'isValid': false
      });
      this.hayErrores++;
    } else {
      this.messages.push({
        'from': 'Unidad',
        'subject': 'Unidad seleccionada: ' + data.NombreUnidad,
        'isValid': true
      });
    }
  }

  iniciarImportacion() {
    let myValues = {
      IdMarca: this.datoValid.IdMarca,
      IdUnidad: this.datoValid.IdUnidad,
      Articulos: this.datoValid.Articulos
    };

    this._service_prods.importarProductos(myValues).subscribe(response => {

      this.showMessageResponse = true;
      this.messagesResponse = response.object;      
    },
      error => {
        this.snack.open(`No se pudo guardar datos del articulo ${error.error.mensajeError || ''}`, 'X', {
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      });
  }
}
