import {Component, Inject, OnInit} from '@angular/core';
import {Moneda} from '../../../models/articles';
import {CatalogsService} from '../../../services/catalogs.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-moneda-form',
  templateUrl: './moneda-form.component.html',
})
export class MonedaFormComponent implements OnInit {
  v: Moneda;
  title = 'Editar Moneda';
  saving = false;

  constructor(private cs: CatalogsService,
              private snack: MatSnackBar,
              public dialogRef: MatDialogRef<MonedaFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { value: Moneda }) {
    this.v = this.data.value;
  }

  ngOnInit(): void {
  }

  save(): void {
    this.v.tipoCambio = Number(this.v.tipoCambio) || 0.0;
    this.saving = true;
    this.cs.updateMoneda(this.v, this.v.id).subscribe(d => {
      this.dialogRef.close(d);
      this.saving = false;
    }, err => {
      this.saving = false;
      this.snack.open('No se pudo guardar la información de la moneda', 'X', {
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      this.dialogRef.close();
    });
  }
}
