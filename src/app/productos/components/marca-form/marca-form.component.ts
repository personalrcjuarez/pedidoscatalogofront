import { Component, Inject, OnInit } from '@angular/core';
import { Marca, Archivos } from '../../../models/articles';
import { CatalogsService } from '../../../services/catalogs.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
//import { APIURLPDF_X } from 'src/app/consts';
import { EnvService } from '../../../../env.service';
import Swal from 'sweetalert2';

declare var require: any
const FileSaver = require('file-saver');

@Component({
  selector: 'app-marca-form',
  templateUrl: './marca-form.component.html',
  styleUrls: ['./marca-form.component.scss']
})

export class MarcaFormComponent implements OnInit {
  v: Marca;
  title = '';
  saving = false;

  linkDescForm = new FormControl();
  marca: Marca = new Marca();
  subscriptions: Subscription[] = [];
  filteredMarcas: Observable<Marca[]> = of([]);
  marcas: Marca[] = [];

  isNew = false;
  archivos: Archivos[] = [];

  constructor(private cs: CatalogsService,
    private env: EnvService,
    private snack: MatSnackBar,
    public dialogRef: MatDialogRef<MarcaFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { value: Marca }) {

    this.title = this.data?.value?.IDMarca ? `Editar  marca` : `Crear marca`;
    if (this.title == "Crear marca") {
      this.isNew = true;
    }
    this.v = this.data.value;
    this.marca = this.data?.value;
  }

  ngOnInit(): void {

  }

  save(): void {
    this.saving = true;

    if (this.isNew) {
      this.v.Archivos = this.archivos;

      this.cs.createMarca(this.v).subscribe(d => {
        this.dialogRef.close(d);
        this.snack.open(`Operación exitosa`, 'Creación exitosa', {
          duration: 2000,
        });
        this.saving = false;
      }, err => {
        this.saving = false;
        this.snack.open('No se pudo guardar la información de la marca', 'X', {
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        this.dialogRef.close();
      });

    } else {
      if (this.v.Archivos == null) {
        this.v.Archivos = [];
      }
      let index = 0;
      this.archivos.forEach(element => {
        this.v.Archivos.push(element);
        this.v.Archivos[index].NombrePdf = element.NombrePdf;
        this.v.Archivos[index].TamanioPdf = element.TamanioPdf;
        index++;
      });

      this.cs.updateMarca(this.v, this.v.IDMarca).subscribe(d => {
        this.v = d;
        this.marca = d;
        this.dialogRef.close(d);
        this.saving = false;

        this.snack.open(`Operación exitosa`, 'Actualización realizado', {
          duration: 4000,
        });
      }, err => {
        this.saving = false;
        this.snack.open('No se pudo guardar la información de la marca', 'X', {
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        this.dialogRef.close();
      });

    }
  }


  csvInputChange(fileInputEvent: any) {
    let archivo: Archivos = {
      Archivo: fileInputEvent.target.files[0],
      NombrePdf: fileInputEvent.target.files[0].name,
      TamanioPdf: fileInputEvent.target.files[0].size,
      
    };
    
    let found = false;
    this.archivos.forEach(element => {
      if (element.NombrePdf == archivo.NombrePdf) {
        found = true;
      }
    });

    this.marca.Archivos?.forEach(element => {      
      if (element.NombrePdf == archivo.NombrePdf) {        
        found = true;
      }
    });

    if (found) {
      this.snack.open(`Ya existe un archivo con el mismo nombre`, 'Archivo duplicado', {
        duration: 2000,
      });
      return
    }

    if (this.archivos.length > 0) {
      this.archivos.push(archivo);
    } else {
      this.archivos = [];
      this.archivos.push(archivo);
    }
  }

  downloadPdf(nameFile: string) {
    FileSaver.saveAs(this.env.APIURL + "/" + this.marca.IDMarca+"_"+nameFile, nameFile);
  }

  askConfirmToDelete(file) {

    //MOSTRAR MENSAJES DE ADVERTENCIA
    Swal.fire({
      title: "¿Está seguro de eliminar el catálogo?",
      text: "Eliminará el catálogo",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.confirmaEliminacion(file);
      }
    })
  }

  deleteFileArray(file) {
    //let foo_object // Item to remove
    this.archivos = this.archivos.filter(obj => obj !== file);
  }

  confirmaEliminacion(file) {
    let updFiles: Archivos[] = [];
    let misArchivos = this.v.Archivos;
    misArchivos.forEach(element => {
      if (element.NombrePdf != file.NombrePdf) {
        updFiles.push(element);
      }
    });

    this.cs.deleteFile(file.IdRefMarca, file.NombrePdf).subscribe(element => {
      this.v.Archivos = updFiles;
      this.snack.open('Catálogo eliminado correctamente', "Accion ejecutada", {
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
    }, err => {
      this.saving = false;
      this.snack.open('No se pudo eliminar el archivo', err.MensajeInformativo, {
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      this.dialogRef.close();

    });
  }
}