import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CatalogsService } from '../../../services/catalogs.service';
import { Catalogs } from '../../../models/catalogs';
import { Observable, of, Subscription } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Impuesto, Marca, KitComponent, Producto, Talla, Color, CatalogoTallasColores } from '../../../models/articles';
import { ProductsService } from '../../../services/products.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { FileInput } from 'ngx-material-file-input';
import { map, startWith } from 'rxjs/operators';
import { MatTable } from '@angular/material/table';
import { ModalColoresComponent } from './modal-colores/modal-colores.component';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})

export class ProductFormComponent implements OnInit, OnDestroy {

  saving = false;
  catalogs: Catalogs;
  ColorsAll: Color[] = [];

  TallasAll: Talla[] = [];
  impuestos: Impuesto[] = [];
  marcas: Marca[] = [];
  subscriptions: Subscription[] = [];
  formProduct: FormGroup;
  editable: boolean;
  isNew: boolean;
  product: Producto = new Producto();
  imageBase64: string | ArrayBuffer;
  originalImage: string;

  articulos: Producto[] = [];
  filteredArticulos: Observable<Producto[]> = of([]);
  buscadorArticulos = new FormControl();
  mapArticulos: Map<number, Producto> = new Map<number, Producto>();
  kitComponents: KitComponent[];

  tallaSeleccionada: Talla;
  tallaSelected: boolean;

  myControlTallas = new FormControl('');
  filteredOptionsTallas: Observable<Talla[]>;

  displayedColumns: string[] = ['Clave', 'Talla', 'Color', 'Quitar'];
  tallasyColores: CatalogoTallasColores[] = [];
  dataSource = [...this.tallasyColores];

  @ViewChild(MatTable) table: MatTable<CatalogoTallasColores>;

  constructor(public cs: CatalogsService,
    public ps: ProductsService,
    private snack: MatSnackBar,
    public dialogRef: MatDialogRef<ProductFormComponent>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: { product: Producto },
  ) {

    const catalogsSubscription = this.cs.availableCatalogs.subscribe(d => {
      this.catalogs = d;
      if (this.isNew) {
        this.formProduct.get('lineaId').setValue(this.catalogs.lineas.values[0]?.value);
        this.formProduct.get('unidadId').setValue(this.catalogs.unidades.values[0]?.value);
      }
    });

    this.cs.loadImpuestos().subscribe(d => {
      this.impuestos = d;
      if (this.isNew) {
        this.formProduct.get('impuestoId').setValue(this.impuestos[0].id);
      }
    });

    this.cs.loadMarcas().subscribe(d => {
      this.marcas = d;
      if (this.isNew) {
        this.formProduct.get('marcaId').setValue(this.marcas[0].IDMarca);
      }
    });

    this.subscriptions.push(catalogsSubscription);

    this.product = this.data.product ? { ...this.data.product } : new Producto();

    //this.selectedColors = this.product.coloresProductos;
    this.ColorsAll = [];
    this.TallasAll = [];

    if (this.product.idArticulo) {
      this.saving = true;
      this.isNew = false;
      this.ps.get(this.product.idArticulo).subscribe(d => {
        this.product = d;

        /* Colores */
        this.catalogs?.colores?.values.forEach(colores => {
          this.ColorsAll.push({  idColor: Number(colores.value), color: colores.text, selected : false  })
        });

        /*End Colores */

        /* Tallas */
        this.catalogs?.tallas?.values.forEach(tallas => {
          this.TallasAll.push({ idTalla: Number(tallas.value), nombreTalla: tallas.text, selected : false })
        });

        //Asignar la clave del producto a cada catalogo talla color
        this.product.TallasColoresProducto.forEach(element => {
          element.ClaveProducto = this.product.nombre;
        });

        /*End Tallas */
        this.tallasyColores = this.product.TallasColoresProducto
        this.dataSource = this.tallasyColores;
        this.saving = false;
        this.imageBase64 = this.product.imagen;
        this.originalImage = this.product.imagen;
        this.product.imagen = null;
        this.formProduct = this.initializeForm();
        this.editable = false;
        this.formProduct.disable();
        this.initializeImageListener();
      }, err => {
        console.log(err);
      });
    } else {

      /* Colores */
      this.catalogs?.colores?.values.forEach(colores => {
        this.ColorsAll.push({ idColor: Number(colores.value), color: colores.text, selected : false  });
      });

      /* Tallas */
      this.catalogs?.tallas?.values.forEach(tallas => {
        this.TallasAll.push({ idTalla: Number(tallas.value), nombreTalla: tallas.text, selected : false  });
      });

      this.isNew = true;
      this.editable = true;
      this.imageBase64 = this.product.imagen;
      this.originalImage = this.product.imagen;
      this.product.imagen = null;
      this.formProduct = this.initializeForm();
      this.initializeImageListener();
    }
  }

  agregarNuevaTalla() {
    this.tallaSeleccionada = null;
    this.tallaSelected = false;
    this.myControlTallas.setValue("");

    this.filteredOptionsTallas = this.myControlTallas.valueChanges.pipe(
      startWith(''),
      map(value => this._filterTallas(value || '')),
    );

    let selectColors = this.ColorsAll.filter(item => item.selected);
    selectColors.forEach(element => {
      element.selected = false;
    });
  }

  ngOnInit(): void {

    const productosSubscriptions = this.ps.products.subscribe((d) => {
      this.articulos = d;
      this.articulos.forEach(a => {
        this.mapArticulos.set(a.idArticulo, a);
      });
      this.kitComponents = this.product.kit && this.product.kits ? this.product.kits.map(k => {
        return {
          componenteId: k.componenteId,
          articuloId: k.articuloId,
          cantidad: k.cantidad,
          articulo: this.mapArticulos.get(k.componenteId),
        };
      }) : null;
      this.buscadorArticulos.enable();
    });

    this.subscriptions.push(productosSubscriptions);

    this.filteredArticulos = this.buscadorArticulos.valueChanges
      .pipe(
        startWith(''),
        map(value => {
          const filterValue = value && value.toLowerCase ? value.toLowerCase() : '';
          return this.articulos.filter(option => !option.kit &&
            (
              option.nombre.toLowerCase().includes(filterValue) ||
              option.clave.toLowerCase().includes(filterValue)
            )
          );
        })
      );
    this.filteredOptionsTallas = this.myControlTallas.valueChanges.pipe(
      startWith(''),
      map(value => this._filterTallas(value)),
    );
  }

  asignarTallaSeleccionada(valor: Talla) {
    this.tallaSelected = true;
    this.tallaSeleccionada = valor;
  }

  displayTallaFn(dataTalla: Talla): string | undefined {
    if (dataTalla == undefined || !dataTalla) {
      return "";
    }
    return dataTalla?.nombreTalla;
  }

  removeData(data: CatalogoTallasColores) {

    const index = this.tallasyColores.findIndex(
      (dato) => dato.IdColor === data.IdColor && dato.IdTalla === data.IdTalla
    );

    if (index >= 0) {
      this.tallasyColores.splice(index, 1);
      this.dataSource = this.tallasyColores;
      this.table.renderRows();
    }
  }

  private _filterTallas(value: any): Talla[] {
    if (value == undefined || value == "" || !value) {
      return this.TallasAll;
    } else {

      var filterValue = "";
      if (typeof value === 'string') {
        filterValue = value.toLowerCase();
      } else {
        filterValue = value.nombreTalla.toLowerCase();
      }

      let filtrado = this.TallasAll.filter(option => option.nombreTalla.toLowerCase().includes(filterValue));
      if (filtrado.length == 1) {
        this.tallaSeleccionada = filtrado[0];
      }
      return filtrado;
    }
  }

  /*
  displayTallaFn(p: TallaProducto): string {
    return p ? `${p.idTalla} - ${p.talla.nombreTalla}` : '';
  }
  */

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => {
      if (!s.closed) {
        s.unsubscribe();
      }
    });
  }

  initializeImageListener(): void {
    this.formProduct.get('imagen').valueChanges.subscribe((d: FileInput) => {
      if (!d || !d.files || !d.files.length) {
        this.imageBase64 = null;
        return;
      }
      const file = d.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageBase64 = reader.result;
      };
    });
  }

  toggleEdit(): void {
    this.editable = !this.editable;
    if (this.editable) {
      this.formProduct.enable();
    } else {
      // tslint:disable-next-line:forin
      for (const k in this.product) {
        if (k !== 'imagen') {
          this.formProduct.get(k)?.setValue(this.product[k]);
        }
      }
      this.product.imagen = this.originalImage as string;
      this.formProduct.disable();
    }
  }

  saveProduct(): void {

    if ( this.tallasyColores.length <= 0) {
      this.snack.open('No ha seleccionado la talla del producto con su respectivo color', 'Talla y color', {
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });
      return;
    }

    const p: Producto = Producto.FromJson(this.formProduct.getRawValue());
    p.TallasColoresProducto = this.tallasyColores;
    p.imagen = this.imageBase64 as string;
    p.idArticulo = this.product.idArticulo;
    const unidadIndex = this.catalogs.unidades.values.findIndex(u => u.value === p.unidadId);
    p.unidad = this.catalogs.unidades.fromCatalog(this.catalogs.unidades.values[unidadIndex]);

    const lineaIndex = this.catalogs.lineas.values.findIndex(u => u.value === p.lineaId);
    p.linea = this.catalogs.lineas.fromCatalog(this.catalogs.lineas.values[lineaIndex]);
    this.saving = true;

    if (p.kit) {
      p.kits = this.kitComponents.map(k => {
        return { cantidad: k.cantidad, articuloId: p.idArticulo, componenteId: k.componenteId };
      });
    }
    let s: Observable<Producto>;
    if (this.isNew) {
      s = this.ps.createProduct(p);
    } else {
      s = this.ps.updateProduct(p, p.idArticulo);
    }
    s.subscribe(d => {
      this.saving = false;
      this.snack.open('Información guardada correctamente', '', {
        horizontalPosition: 'right',
        verticalPosition: 'top',
        duration: 2000,
      });
      this.dialogRef.close(d);
    },
      error => {
        console.log("My error: ", error);
        this.saving = false;
        this.snack.open(`No se pudo guardar datos del articulo ${error.error.mensajeInformativo || ''}`, 'X', {
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      });
  }

  displayProductFn(p: Producto): string {
    return p ? `${p.clave} - ${p.nombre}` : '';
  }

  addArticulo(): void {
    if (!this.kitComponents) {
      this.kitComponents = [];
    }
    const v: Producto = this.buscadorArticulos.value;
    if (this.buscadorArticulos.value instanceof Producto) {
      const exists = this.kitComponents.findIndex(x => x.componenteId === v.idArticulo);
      if (exists < 0) {
        this.kitComponents.push({
          componenteId: v.idArticulo,
          articulo: v,
          articuloId: this.product.idArticulo,
          cantidad: 1,
        });
      } else {
        this.snack.open(`El articulo "${v.nombre}" ya fue agregado al kit`);
      }
      this.buscadorArticulos.setValue('');
    }
  }

  removeKitComponent(idx: number): void {
    this.kitComponents.splice(idx, 1);
  }

  initializeForm(): FormGroup {
    return new FormGroup({
      nombre: new FormControl(this.product.nombre, [Validators.required]),
      descripcion: new FormControl(this.product.descripcion, [Validators.required]),
      clave: new FormControl(this.product.clave, [Validators.required]),
      claveAlterna: new FormControl(this.product.claveAlterna, []),
      codigoBarras: new FormControl(this.product.codigoBarras, []),
      lineaId: new FormControl(this.product.lineaId, [Validators.required]),
      marcaId: new FormControl(this.product.marcaId, [Validators.required]),
      //modelo: new FormControl(this.product.modelo, [Validators.required]),
      unidadId: new FormControl(this.product.unidadId, [Validators.required]),
      //talla: new FormControl(this.product.talla, [Validators.required]),
      //colorId: new FormControl(this.product.colorId, [Validators.required]),
      precio1: new FormControl(this.product.precio1, [Validators.required]),
      costoReal: new FormControl(this.product.costoReal),
      costoUltimo: new FormControl(this.product.costoUltimo, [Validators.required]),
      costoAdicional: new FormControl(this.product.costoAdicional),
      //existencia: new FormControl(this.product.existencia, [Validators.required]),
      porcentajeDescuento: new FormControl(this.product.porcentajeDescuento),
      imagen: new FormControl(this.product.imagen, []),
      enOferta: new FormControl(this.product.enOferta, []),
      precioBajoCosto: new FormControl(this.product.precioBajoCosto, []),
      bloqueado: new FormControl(this.product.bloqueado, []),
      observaciones: new FormControl(this.product.observaciones, []),
      impuestoId: new FormControl(this.product.impuestoId, [Validators.required]),
      kit: new FormControl(this.product.kit),
    });

  }

  openDialog(): void {

    const dialogRef = this.dialog.open(ModalColoresComponent, {
      maxWidth: '100vw',
      height: '85vh',
      maxHeight: '85vh',
      closeOnNavigation: true,
      data: {
        talla: this.tallaSeleccionada,
        colores: this.ColorsAll
      },
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      let colorsSelectedToTallas: Color[];
      colorsSelectedToTallas = result.colores.filter(item => item.selected);

      colorsSelectedToTallas.forEach(element => {
        let tallaColor: CatalogoTallasColores = {
          IdArticulo: 0,
          ClaveProducto: this.formProduct.get("nombre").value,
          NombreTalla: this.tallaSeleccionada.nombreTalla,
          NombreColor: element.color,
          IdTalla: result.talla.idTalla,
          IdColor: element.idColor
        }

        const index = this.tallasyColores.findIndex(
          (dato) => dato.IdColor === tallaColor.IdColor && dato.IdTalla === tallaColor.IdTalla
        );

        if (index == -1) {
          this.tallasyColores.push(tallaColor);
        }
      });
      this.dataSource = this.tallasyColores;
      this.table.renderRows();
    });
  }

}
