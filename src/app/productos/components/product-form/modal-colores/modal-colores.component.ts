import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Color } from 'src/app/models/articles';

@Component({
  selector: 'app-modal-colores',
  templateUrl: './modal-colores.component.html',
  styleUrls: ['./modal-colores.component.scss']
})
export class ModalColoresComponent implements OnInit {

  colorControl = new FormControl();
  filteredColors: Observable<Color[]>;
  lastFilterColor: string = '';
  selectedAllColors: Color[] = [];
  nombreTalla : "";
  selectedColors: Color[] = new Array<Color>();
  
  constructor(
    public dialogRef: MatDialogRef<ModalColoresComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any 
  ) { 
    this.selectedAllColors = data.colores;
    this.nombreTalla = data.talla.nombreTalla;
  }

  ngOnInit(): void {
    this.filteredColors = this.colorControl.valueChanges.pipe(
      startWith<string | Color[]>(''),
      map(value => typeof value === 'string' ? value : this.lastFilterColor),
      map(filterColor => this.filterColors(filterColor))
    );    
  }

  filterColors(filter: string): Color[] {
    this.lastFilterColor = filter;
    if (filter) {
      return this.selectedAllColors.filter(option => {
        return option.color.toLowerCase().indexOf(filter.toLowerCase()) >= 0;
      })
    } else {
      return this.selectedAllColors.slice();
    }
  }

  stopPropagationColor(event: Event) {
    event.stopPropagation();
  }

  optionClickedColor(event: Event, color: Color) {
    event.stopPropagation();
    this.toggleSelectionColor(color);
  }

  toggleSelectionColor(dataColor: Color) {

    dataColor.selected = !dataColor.selected;
    if (dataColor.selected) {
      this.selectedColors.push(dataColor);
    } else {
      const i = this.selectedColors.findIndex(value => value.idColor === dataColor.idColor && value.color === dataColor.color);
      this.selectedColors.splice(i, 1);
    }

    
    this.colorControl.setValue(this.selectedColors);

  }

  displayFnColor(dataColors: Color[] | string): string | undefined {

    let displayValue: string;
    if (Array.isArray(dataColors)) {
      dataColors.forEach((data, index) => {
        if (index === 0) {
          displayValue = data.color;
        } else {
          displayValue += ', ' + data.color;
        }
      });
    } else {
      displayValue = dataColors;
    }
    return displayValue;
  }

  cancel(): void {
    this.dialogRef.close();
  }
}
