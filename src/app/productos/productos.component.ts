import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { Marca, Producto } from '../models/articles';
import { MatDialog } from '@angular/material/dialog';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { Subscription } from 'rxjs';
import { map, tap} from 'rxjs/operators';
import { ProductsService } from '../services/products.service';
import { CatalogsService } from '../services/catalogs.service';
import { Catalogs } from '../models/catalogs';
import { AuthService } from '../services/auth.service';
import { APIResponse, DataPaginator } from '../models/common';
@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
})
export class ProductosComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns: string[] = ['id', 'clave', /* 'tipo', */ 'nombre', 'descripcion', 'marca', /* 'modelo', 'existencia', */ 'created_at', 'updated_at', 'en_oferta'];
  //dataSource: MatTableDataSource<any>;
  dataSource: DataPaginator = null;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  private subscriptions: Subscription[] = [];
  loading = false;
  catalogs: Catalogs;
  marcas: Map<any, Marca> = new Map<any, Marca>();
  idMovimientos = [];
  datosTotales = 0;
  cantidadRegistros = 10;
  sortedData: Producto[];
  productos: Producto[];
  postPerPage = 10;
  pageNumber = 1;
  campoOrden = "nombre";
  direccionOrden = "asc";
  inputBusqueda : "";

   // MatPaginator Output
   pageEvent: PageEvent;
   
  constructor(
    private ps: ProductsService,
    private cs: CatalogsService,
    private dialog: MatDialog,
    public auth: AuthService) {
    //this.dataSource = new MatTableDataSource<any>([]);
    
  }

  ngOnInit(): void {

    this.loading = true;
    
    this.cs.availableCatalogs.subscribe(d => {
      if (d === null){
        this.cs.loadCatalogs();
      }
      this.catalogs = d;
    });

    this.marcas = this.cs.mapaMarcas;

    this.cs.getMarcasLoaded.subscribe(marc => {
      if (marc.length === 0) {
        this.cs.loadMarcas().subscribe(personsdb => {
        })
      }
      this.marcas = this.cs.mapaMarcas;
    });
    
    this.initProductSource(this.postPerPage,this.pageNumber,this.campoOrden,this.direccionOrden, this.inputBusqueda);
  }

  initProductSource(limit: number, page: number, active: string, direction :string, stringSearched: string) {
    this.ps.getProductsPaginator(limit, page, active, direction,stringSearched).pipe(
      map((productData: APIResponse) => 
      this.dataSource = productData.DataMeta
      )
    ).subscribe( dato => {
      this.dataSource.items.forEach(element => {
        element.marca = this.marcas.get(element.marcaId);
      });
      this.sortedData = dato.items.slice();
      this.productos = dato.items;
      this.loading = false;
    }
    );
  }

  sortData(sort: Sort) {
    
    const data = this.productos.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.campoOrden = sort.active;
    this.direccionOrden = sort.direction;

    this.initProductSource(this.postPerPage,this.pageNumber,this.campoOrden,this.direccionOrden,this.inputBusqueda);
  }

  ngAfterViewInit(): void {
    //this.dataSource = this.paginator;
    //this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => {
      if (!s.closed) {
        s.unsubscribe();
      }
    });
  }

  applyFilter(): void {
    this.pageNumber = 0;
    if ( this.inputBusqueda != "" ) {
      this.initProductSource(this.postPerPage,this.pageNumber,this.campoOrden,this.direccionOrden,  this.inputBusqueda);
    }
  }

  resetQuery(): void {
    this.inputBusqueda = "";
    this.initProductSource(this.postPerPage,this.pageNumber,this.campoOrden,this.direccionOrden,  this.inputBusqueda);
  }


  openDialog(p: Producto): void {
    if (!this.auth.isFilial() && !this.auth.isAdmin()) {
      return;
    }
    const dialogRef = this.dialog.open(ProductFormComponent, {
      position: {
        top: '0',
        right: '0',
      },
      maxWidth: '100vw',
      height: '100vh',
      maxHeight: '100vh',
      disableClose: true,
      closeOnNavigation: true,
      panelClass: ['full-screen-modal', 'w-full', 'lg:w-10/12', 'xl:w-8/12'],
      data: {
        product: p,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  onPaginate(pageEvent: PageEvent) {
    
    this.postPerPage = +pageEvent.pageSize;
    this.pageNumber = +(pageEvent.pageIndex + 1) ;

    this.initProductSource(this.postPerPage,this.pageNumber,this.campoOrden,this.direccionOrden,  this.inputBusqueda);
    }
}