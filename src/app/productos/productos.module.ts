import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatProgressBarModule } from '@angular/material/progress-bar';
import {MatExpansionModule} from '@angular/material/expansion';

import {ProductosRoutingModule} from './productos-routing.module';
import {ProductosComponent} from './productos.component';
import {SharedModule} from '../shared/shared.module';
import {ProductFormComponent} from './components/product-form/product-form.component';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {CatalogsComponent} from './components/catalogs/catalogs.component';

import {AlmacenFormComponent} from './components/almacen-form/almacen-form.component';
import {ImpuestoFormComponent} from './components/impuesto-form/impuesto-form.component';
import {MonedaFormComponent} from './components/moneda-form/moneda-form.component';
import { MarcaFormComponent } from './components/marca-form/marca-form.component';
import { DowloadcatalogosComponent } from './components/dowloadcatalogos/dowloadcatalogos.component';
import { ModalColoresComponent } from './components/product-form/modal-colores/modal-colores.component';
import { ImportProductsComponent } from './components/import-products/import-products.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { ModalValidacionComponent } from './components/import-products/modal-validacion/modal-validacion.component';

@NgModule({
  declarations: [ProductosComponent, ProductFormComponent, CatalogsComponent, AlmacenFormComponent, ImpuestoFormComponent, MonedaFormComponent, MarcaFormComponent, DowloadcatalogosComponent, ModalColoresComponent, ImportProductsComponent, ModalValidacionComponent],
  imports: [
    SharedModule,
    CommonModule,
    MatFormFieldModule,
    ProductosRoutingModule,
    MaterialFileInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatGridListModule,
  ]
})
export class ProductosModule {
}
