module.exports = {
    prefix: '',
    purge: {
      content: [
        './src/**/*.{html,ts,scss}',
      ]
    },
    darkMode: false, // or 'media' or 'class'
    theme: {
      extend: {},
    },
    variants: {
      extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'),require('@tailwindcss/line-clamp')],
};
